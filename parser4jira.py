#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Usage:
    parser4jira.py -o <old_value> -n <new_value> -f <metadata_json_filename> -d <database>
"""

"""
__author__      = "Tomasz Dudzisz <tdudzisz@atlassian.com>"
__author__      = "Rudy Slaiby <rslaiby@atlassian.com>"
__author__      = "Tomasz Kanafa <tkanafa@atlassian.com>"
__copyright__   = "Copyright 2018, Atlassian.com"
__credits__     = []
__license__     = "BSD"
__version__     = "1.0.5"
__status__      = "Development"
__created__     = 11/04/2018
__updated__     = 12/04/2018

__project__     = gdpr-framework
__file__        = parser.py
__description__ = 

"""

import json
import os
import sys
import traceback
from docopt import docopt
from schema import And, Schema

from library.helpers import Database, Generic, Storage

JQL_WARNING = """--   This update can affect existing JQL queries as changed entity could be used by some JQL query/filter. List of places where Jira could use JQL queries:
--     * filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Saving+your+search+as+a+filter
--     * filter subscriptions - https://confluence.atlassian.com/display/JIRACORESERVER/Working+with+search+results
--     * gadgets that use JQL filters of JQL - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Adding+and+customizing+gadgets
--     * reports - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Reporting
--     * boards - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+filters
--     * board swimlanes - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+swimlanes
--     * board quick-filters - https://confluence.atlassian.com/display/JIRASOFTWARESERVER/Configuring+Quick+Filters
--     * possibly other places that rely on saved filters or free-text JQL queries
--   If you are running JIRA Service Desk then JQL can be used in following places:
--     * Queues - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+queues+for+your+team
--     * Reports - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+service+desk+reports
--     * Automation - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Automating+your+service+desk
--     * SLAs - https://confluence.atlassian.com/display/SERVICEDESKSERVER/Setting+up+SLAs"""

def encode_table_name(table_name, database):
    if database == Database.POSTGRESQL:
        if table_name[0].isupper():
            table_name = '"{}"'.format(table_name)
    return table_name

class Opts(object):

    def __init__(self, __doc__):
        self.old_value = None
        self.new_value = None
        self.filename = None
        self.database = None

        self.valid = False

        arguments = docopt(__doc__)
        self._validate(arguments)


    def _validate(self, arguments):
        schema = Schema({
            '-o': True,
            '<old_value>': And(str, len),
            '-n': True,
            '<new_value>': And(str, len),
            '-f': True,
            '<metadata_json_filename>': And(str, len),
            '-d': True,
            '<database>': And(str, len),
        })
        args = schema.validate(arguments)

        self.old_value = args['<old_value>']
        self.new_value = args['<new_value>']
        self.filename = args['<metadata_json_filename>']
        self.database = args['<database>']

        self.valid = self.new_value and self.old_value and self.filename and self.database


    @staticmethod
    def _read_json(filename):
        if os.path.isfile(filename):
            content = open(filename, 'r').read()
            data = json.loads(content)
            return data
        print("No such file: {}".format(filename))
        sys.exit(1)


    def get_data(self):
        return Opts._read_json(self.filename)


class ColumnDecorator:

    def __init__(self, column_name, column_data):
        self.column_name = column_name
        self.column_data = column_data
        self.constraints = column_data.get('constraints', None)
        self.storage = column_data.get('storage', None)

        self.mysql_keywords = ['KEY']
        self.mssql_keywords = ['KEY', 'USER']
        self.oracle_keywords = ['USER']


    def type(self):
        return self.column_data['type']

    # column name
    def name_string(self):
        return self.column_name.split("-")[0]

    # column name encoded for database
    def name(self, database):
        name = self.name_string()

        if database == Database.POSTGRESQL:
            if name.isupper():
                return '"{}"'.format(name)
        if database == Database.MYSQL:
            if name in self.mysql_keywords:
                return "`{}`".format(name)
        if database == Database.MSSQL:
            if name in self.mssql_keywords:
                return '"{}"'.format(name)
        if database == Database.ORACLE:
            if name in self.oracle_keywords:
                return '"{}"'.format(name)
        return name


    # right hand of update clause
    def update_value_clause(self, database, old_value, new_value):

        def regex_match_handler(database, column_name, storage, old_value, new_value):

            if database == Database.POSTGRESQL:
                return "REGEXP_REPLACE({},'(^|[[:blank:]]|[[:punct:]]){}([[:blank:]]|[[:punct:]]|$)','\\1{}\\2','ig')".format(column_name, old_value, new_value)

            if database == Database.MYSQL:
                #no regexp replace for mysql, so we can search for stuff using "regexp" but are unable to reliably update it
                return "replace({},'{}','{}')".format(column_name, old_value, new_value)

            if database == Database.ORACLE:
                return "REGEXP_REPLACE({},'(^|[[:blank:]]|[[:punct:]]){}([[:blank:]]|[[:punct:]]|$)','\\1{}\\2')".format(column_name, old_value, new_value)

            if database == Database.MSSQL:
                if storage in [Storage.VERY_LONG, Storage.EXTREMELY_LONG]:
                    return "cast(replace(cast({} as nvarchar(max)), '{}', '{}') as ntext)".format(column_name, old_value, new_value)
                return "replace({}, '{}', '{}')".format(column_name, old_value, new_value)

            return "__REGEXP_PLACEHOLDER__"

        def overwrite_with_new_value_handler(database, column_name, storage, old_value, new_value):
            return "'{}'".format(new_value)

        # def none_handler(database, column_name, old_value, new_value):
        #     return "{}".format(column_name)

        ret = {
            'text': regex_match_handler,
            'jql': regex_match_handler,
            'audit_changed_value': regex_match_handler,
            'user_key': overwrite_with_new_value_handler,
        }.get(self.type(), overwrite_with_new_value_handler)(database, self.name(database), self.storage, old_value, new_value)
        return ret


    # clause for column that can be used in "where" statement
    def get_sql_select_clause(self, database, old_value, new_value):

        def constraints_decorator(sql):
            if self.constraints is not None:
                constraints_sql = ["{} {}".format(ColumnDecorator(column,{}).name(database), constraint) for column, constraint in self.constraints.items()]
                sql = "({}) AND {}".format(sql, ' AND '.join(constraints_sql))
                return sql
            else:
                return sql


        def exact_with_lower_match_handler(database, column_name, storage, old_value, new_value):

            if database == Database.MSSQL:
                return "{} = '{}' ".format(column_name, old_value)
            if database == Database.ORACLE:
                if storage == Storage.EXTREMELY_LONG:
                    return "LOWER(to_char({})) = LOWER('{}') ".format(column_name, old_value)

            return "LOWER({}) = LOWER('{}') ".format(column_name, old_value)


        def like_match_handler(database, column_name, storage, old_value, new_value):
            if database == Database.MSSQL:
                return "{} like '%{}%' ".format(column_name, old_value)

            return "LOWER({}) like LOWER('%{}%') ".format(column_name, old_value)


        def regex_match_handler(database, column_name, storage, old_value, new_value):
            if database == Database.MYSQL:
                return "LOWER({}) regexp '(^|[[:blank:]]|[[:punct:]]){}([[:blank:]]|[[:punct:]]|$)'".format(column_name, old_value)

            if database == Database.ORACLE:
                return "REGEXP_LIKE (LOWER({}),'(^|[[:blank:]]|[[:punct:]]){}([[:blank:]]|[[:punct:]]|$)')".format(column_name, old_value)

            if database == Database.MSSQL:
                return "{} like '%{}%' ".format(column_name, old_value)

            return "LOWER({}) ~* '(^|[[:blank:]]|[[:punct:]]){}([[:blank:]]|[[:punct:]]|$)'".format(column_name, old_value)

        def none_handler(database, column_name, storage, old_value, new_value):
            return "1 = 0".format(column_name, old_value)

        print("TYPE " + self.type())

        ret = {
            'user_key': exact_with_lower_match_handler,
            'email': exact_with_lower_match_handler,
            'text': regex_match_handler,
            'url': like_match_handler,
            'jql': regex_match_handler,
            'audit_changed_value': regex_match_handler,
            'audit_data': regex_match_handler,
            'username': exact_with_lower_match_handler,
            'ip':exact_with_lower_match_handler
        }.get(self.type(), none_handler)(database, self.name(database), self.storage, old_value, new_value)
        return constraints_decorator(ret)


    # clause for column that can be used in "update" statements
    def get_sql_update_clause(self, database, updateFrom, updateTo):
        value = self.update_value_clause(database, updateFrom, updateTo)
        return "{} = {}".format(self.name(database), value)

    def get_sql_update_clause_warning(self, database, updateFrom, updateTo):
        warning = []
        sql = self.update_value_clause(database, updateFrom, updateTo)
        if database == Database.MYSQL:
            if "replace(" in sql:
                warning.append("-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.")
        if self.column_data.get('jql_field', False):
            warning.append(JQL_WARNING)

        if warning:
            return "\n".join(warning)
        return None

    def has_storage(self, *types):
        if not self.storage:
            return False
        return self.storage in types

class BuildQuery(Generic):

    def __init__(self, where, db, old_value, new_value):

        super().__init__()

        self.where = where
        self.database = db
        self.old_value = old_value
        self.new_value = new_value

        self.mysql_keywords = ['key']


    # select/update/delete template - "where ..." should be appended to it
    def get_sql_action_select(self, database, table_name, columns, action, add_additional_columns):

        action_type = action['type']
        select_columns = "{}.{}".format(table_name, action['columns'])

        if database == Database.POSTGRESQL:
            select_columns = '"{}".{}'.format(table_name, action['columns'])

        additional_columns_sql = ""
        if add_additional_columns:
            additional_columns = []
            for column_name, column_data in columns.items():
                columnDecorator = ColumnDecorator(column_name, column_data)
                additional_columns.append(
                    "{} as {}".format(columnDecorator.name(self.database), "{}_before".format(columnDecorator.name_string())))
                additional_columns.append("{} as {}".format(
                    columnDecorator.update_value_clause(self.database, self.old_value, self.new_value),
                    "{}_after".format(columnDecorator.name_string())))
                # updating self dependant column
                if "update" in column_data:
                    update_column_data = column_data["update"]
                    if update_column_data['table']==table_name:
                        updateColumnDecorator = ColumnDecorator(update_column_data["column"], {
                            "type": update_column_data["column_type"],
                            "storage": update_column_data.get("storage", None)
                        })
                        additional_columns.append(
                            "{} as {}".format(updateColumnDecorator.name(self.database), "{}_before".format(updateColumnDecorator.name_string())))
                        additional_columns.append("{} as {}".format(
                            updateColumnDecorator.update_value_clause(self.database, self.old_value, self.new_value),
                            "{}_after".format(updateColumnDecorator.name_string())))

            if additional_columns:
                additional_columns_sql = ",{}".format(",".join(additional_columns))

        table_name = encode_table_name(table_name, self.database)
        sql_action = "{} {}{} from {}".format(action_type, select_columns, additional_columns_sql, table_name)

        return sql_action


    def get_select_action(self, actions):
        for action in actions:
            if action['type'] == 'select':
                return action


    def get_comment_info(self, metadata, action_type):
        if "url" not in metadata:
            return

        result = []
        result.append("-- Update via Jira URL (recommended):")
        result.append("--     {}".format(metadata["url"]))
        result.append("-- ")
        for action in metadata[self.action]:
            if action[self.type] == 'update':
                if self.steps in action:
                    result.append("--   How to access: ")
                    no = 1
                    for step in action[self.steps]:
                        result.append("--     {}. {}".format(no, step))
                        no = no + 1
                if self.api in action:
                    result.append("-- ")
                    result.append("-- Update via REST API:")
                    result.append("--     {}".format(action[self.api]))

        if action_type == 'update':
            result.append("-- ")
            result.append("-- Update via SQL:")

        result = "\n".join(result)
        return result


    def get_field_comment(self,metadata):
        result = []

        if self.where in metadata:
            for field in metadata[self.where].keys():
                result.append(field)
        return ", ".join(result)


    def get_sql_for_table_and_columns(self, database, table_name, metadata, columns, action, actions, comments):

        def get_sql_table_select_dependant(database, action, columns, result, table_name):
            for column_name, column_data in columns.items():
                if "update" in column_data:
                    update_column_data = column_data["update"]
                    if update_column_data['table']!=table_name:
                        selectAction = action.copy()
                        selectAction['columns'] = "*"
                        selectAction['type'] = 'select'
                        u = column_data["update"]
                        updateTable = u["table"]
                        updateTablePk = u["table_pk"]
                        updateColumnName = u["column"]
                        updateColumnData = {"type": u["column_type"], "storage":u.get("storage", None)}
                        fk = u["fk"]
                        # select from original table
                        whereClause = self.get_sql_where_clause({column_name: column_data})
                        fkColumnDecorator = ColumnDecorator(fk,{})
                        selectClause = "select {} from {} where {}".format(fkColumnDecorator.name(database), encode_table_name(table_name, database), whereClause)
                        selectSql = self.get_sql_action_select(database, updateTable, {updateColumnName: updateColumnData}, selectAction, True)
                        updateTablePkColumnDecorator = ColumnDecorator(updateTablePk,{})
                        depSelectClause = "{} where {} in ({});".format(selectSql, updateTablePkColumnDecorator.name(database), selectClause)
                        self.add_comment(result, " selecting dependant data")
                        result.append(depSelectClause)

        def get_sql_table_update_dependant(database, columns, result, table_name):
            # update dependant table first
            for column_name, column_data in columns.items():
                if "update" in column_data:
                    update_column_data = column_data["update"]
                    if update_column_data['table']!=table_name:
                        u = column_data["update"]
                        updateTable = u["table"]
                        updateTablePk = u["table_pk"]
                        updateColumnName = u["column"]
                        updateColumnData = {"type": u["column_type"], "storage":u.get("storage", None)}
                        fk = u["fk"]
                        fkColumnDecorator = ColumnDecorator(fk,{})
                        # select from original table
                        whereClause = self.get_sql_where_clause({column_name: column_data})
                        selectClause = "select {} from {} where {}".format(fkColumnDecorator.name(database), encode_table_name(table_name, database), whereClause)
                        # construct update dependant table
                        updateColumnDecorator = ColumnDecorator(updateColumnName, updateColumnData)
                        updateColumnClause = updateColumnDecorator.get_sql_update_clause(self.database, self.old_value, self.new_value)
                        updateTablePkColumnDecorator = ColumnDecorator(updateTablePk,{})
                        depUpdateClause = "update {} set {} where {} in ({});".format(encode_table_name(updateTable, database), updateColumnClause, updateTablePkColumnDecorator.name(database), selectClause)
                        self.add_comment(result, " updating dependant data")
                        result.append(depUpdateClause)


        result = []
        comment_info = self.get_comment_info(metadata, action['type'])
        if comment_info:
            result.append(comment_info)

        select_action = self.get_select_action(actions)

        if action['type'] == 'update':

            #self.add_break(result)
            self.add_comment(result, "+ SELECT (please review changes BEFORE)")

            get_sql_table_select_dependant(self.database, select_action, columns, result, encode_table_name(table_name, self.database))

            self.get_sql_table_select(select_action, columns, result, table_name, True)

            self.add_comment(result, "+ UPDATE (be careful)")

            get_sql_table_update_dependant(self.database, columns, result, encode_table_name(table_name, self.database))

            self.get_sql_table_update(columns, result, table_name)

        elif action['type'] == 'select':
            get_sql_table_select_dependant(self.database, select_action, columns, result, encode_table_name(table_name, self.database))
            self.get_sql_table_select(action, columns, result, table_name, False)

        result = "\n".join(result)
        return "{}\n".format(result)


    def add_comment(self, result, text):
        result.append("-- {}".format(text))


    def add_break(self, result):
        result.append(" ".format())


    def get_sql_table_select(self, action, columns, result, table_name, additional_columns):
        for column_name, column_data in columns.items():

            # result.append("-- Fields      : {}".format(column_name))
            steps = column_data.get("steps", [])
            if steps:
                self.add_comment(result, "")
                for step in column_data.get("steps", []):
                    self.add_comment(result, step)

            out_action = self.get_sql_action_select(self.database, table_name, columns, action, additional_columns)
            where = self.get_sql_where_clause({column_name: column_data})
            final = "{} where {};\n".format(out_action, where)
            result.append(final)


    def get_sql_table_update(self, columns, result, table_name):
        for column_name, column_data in columns.items():
            steps = column_data.get("steps", [])
            if steps:
                for step in steps:
                    self.add_comment(result, step)
                self.add_break(result)
                self.add_break(result)
            if column_data.get("manual", False):
                continue
            columnDecorator = ColumnDecorator(column_name, column_data)
            where = self.get_sql_where_clause({column_name: column_data})

            update_column = columnDecorator.get_sql_update_clause(self.database, self.old_value, self.new_value)
            #updating self-dependant column
            if "update" in column_data:
                update_column_data = column_data["update"]
                if update_column_data['table']==table_name:
                    updateColumnDecorator = ColumnDecorator(update_column_data["column"], {
                        "type": update_column_data["column_type"],
                        "storage": update_column_data.get("storage",None)
                    })
                    update_column = "{},{}".format(update_column, updateColumnDecorator.get_sql_update_clause(self.database, self.old_value, self.new_value))
            update_column_warning = columnDecorator.get_sql_update_clause_warning(self.database, self.old_value, self.new_value)

            table_name = encode_table_name(table_name, self.database)
            final = "update {} set {} where {};\n".format(table_name, update_column, where)
            if update_column_warning:
                final = "{}\n{}".format(update_column_warning, final)
            result.append(final)


    def get_sql_where_clause(self, where_details):
        if not where_details:
            return "1 = 1"

        sql_where_clause = ""

        for current_column, current_value in where_details.items():
            columnDecorator = ColumnDecorator(current_column, current_value)
            and_entry = columnDecorator.get_sql_select_clause(self.database, self.old_value, self.new_value)
            sql_where_clause += "and {}".format(and_entry)

        # Slice up the starting and
        sql_where_clause = self.slice_string(sql_where_clause, "and ")

        return sql_where_clause


    def slice_string(self, text, text_to_slice):
        if (len(text) > len(text_to_slice)) and (text.startswith(text_to_slice)):
            text = text[len(text_to_slice):]
        return text


class SearchData(Generic):

    def __init__(self):

        super().__init__()

        self.old_value = None
        self.new_value = None
        self.data = None

        self.user_order = False
        self.order = 1


    def execute(self):

        self.clear_output()

        for table_name, table_data in self.data.items():
            self.save_query(table_name, table_data)
            self.order += 1


    def save_query(self, table, metadata):

        origin = metadata[self.origin]
        description = metadata[self.description]
        product = metadata.get("product", None)
        versions = metadata.get("versions", None)

        if not metadata[self.active]:
            return

        actions = metadata[self.action]

        for action in actions:
            type = action[self.type]

            builder = BuildQuery(self.where, self.database, self.old_value, self.new_value)

            columns = metadata[self.where]
            for column_name, column_data in columns.items():
                # don't skip user_keys
                if action["manual"] and column_data['type'] != 'user_key':
                    continue

                self.comments = []
                self.comments.append("-- Type        : {}".format(type))
                self.comments.append("-- Origin      : {}".format(origin))
                self.comments.append("-- Description : {}".format(description))
                column_versions = column_data.get("versions", None)
                if product:
                    self.comments.append("-- Table valid only for specific product : {}".format(product))
                if versions:
                    versions_string = ["{}{}".format(product, product_version) for product, product_version in versions.items()]
                    self.comments.append("-- Table valid only for specific versions : {}".format(", ".join(versions_string)))
                if column_versions:
                    versions_string = ["{}{}".format(product, product_version) for product, product_version in column_versions.items()]
                    self.comments.append("-- Table column valid only for specific versions : {}".format(", ".join(versions_string)))

                sql = builder.get_sql_for_table_and_columns(self.database, table, metadata, {column_name: column_data}, action, actions, self.comments)

                print(sql)

                self.save_for_db(self.database, table, column_name, column_data, type, sql)


    def show_metadata(self, key, value):

        print("\n{}".format(key))
        print(self.get_value("origin", value))
        print(self.get_value("description", value))
        print(self.get_value("action", value))

        for cname, cvalue in value[self.action].items():
            print("\t\t{0:<25}: {0:<30}".format(cname, cvalue))

        print("\t{}:".format(self.where))

        for cname, cvalue in value[self.where].items():
            print("\t\t{0:<25}: {0:<30}".format(cname, cvalue))


    def parse_args(self):
        args = Opts(__doc__)

        if not args.valid:
            sys.exit(1)

        self.old_value = args.old_value
        self.new_value = args.new_value
        self.data = args.get_data()
        self.database = args.database

        base = os.path.basename(args.filename)
        self.output_path = "{}_{}".format(base.split('.')[0], "queries")


    def get_value(self, keyword, entry):
        if keyword in entry:
            return "\t{0:<33}: {0:<20}".format(keyword, entry[keyword])


if __name__ == '__main__':

    try:

        search = SearchData()

        search.parse_args()

        search.execute()

    except Exception as e:

        print("ERROR: {}".format(e))
        traceback.print_exc()

        sys.exit(1)
