-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base statistics associated with user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_0201F0_STATS_EVENT_PARAM.*,PARAM_VALUE as PARAM_VALUE_before,replace(PARAM_VALUE,'<OLD_VALUE>','<NEW_VALUE>') as PARAM_VALUE_after from AO_0201F0_STATS_EVENT_PARAM where LOWER(PARAM_VALUE) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update AO_0201F0_STATS_EVENT_PARAM set PARAM_VALUE = replace(PARAM_VALUE,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(PARAM_VALUE) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Email notifications
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_4E8AE6_NOTIF_BATCH_QUEUE.*,HTML_CONTENT as HTML_CONTENT_before,replace(HTML_CONTENT,'<OLD_VALUE>','<NEW_VALUE>') as HTML_CONTENT_after from AO_4E8AE6_NOTIF_BATCH_QUEUE where LOWER(HTML_CONTENT) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update AO_4E8AE6_NOTIF_BATCH_QUEUE set HTML_CONTENT = replace(HTML_CONTENT,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(HTML_CONTENT) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Email notifications
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_4E8AE6_NOTIF_BATCH_QUEUE.*,TEXT_CONTENT as TEXT_CONTENT_before,replace(TEXT_CONTENT,'<OLD_VALUE>','<NEW_VALUE>') as TEXT_CONTENT_after from AO_4E8AE6_NOTIF_BATCH_QUEUE where LOWER(TEXT_CONTENT) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update AO_4E8AE6_NOTIF_BATCH_QUEUE set TEXT_CONTENT = replace(TEXT_CONTENT,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(TEXT_CONTENT) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base links with personal data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_54307E_CONFLUENCEKB.*,APPLINK_NAME as APPLINK_NAME_before,replace(APPLINK_NAME,'<OLD_VALUE>','<NEW_VALUE>') as APPLINK_NAME_after from AO_54307E_CONFLUENCEKB where LOWER(APPLINK_NAME) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update AO_54307E_CONFLUENCEKB set APPLINK_NAME = replace(APPLINK_NAME,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(APPLINK_NAME) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base links with personal data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_54307E_CONFLUENCEKB.*,SPACE_NAME as SPACE_NAME_before,replace(SPACE_NAME,'<OLD_VALUE>','<NEW_VALUE>') as SPACE_NAME_after from AO_54307E_CONFLUENCEKB where LOWER(SPACE_NAME) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update AO_54307E_CONFLUENCEKB set SPACE_NAME = replace(SPACE_NAME,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(SPACE_NAME) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base links with personal data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_54307E_CONFLUENCEKB.*,SPACE_URL as SPACE_URL_before,replace(SPACE_URL,'<OLD_VALUE>','<NEW_VALUE>') as SPACE_URL_after from AO_54307E_CONFLUENCEKB where LOWER(SPACE_URL) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update AO_54307E_CONFLUENCEKB set SPACE_URL = replace(SPACE_URL,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(SPACE_URL) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : SLA audit log data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_54307E_SLAAUDITLOGDATA.*,VALUE as VALUE_before,replace(VALUE,'<OLD_VALUE>','<NEW_VALUE>') as VALUE_after from AO_54307E_SLAAUDITLOGDATA where LOWER(VALUE) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update AO_54307E_SLAAUDITLOGDATA set VALUE = replace(VALUE,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(VALUE) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_9B2E3B_EXEC_RULE_MSG_ITEM.*,RULE_MESSAGE_VALUE as RULE_MESSAGE_VALUE_before,replace(RULE_MESSAGE_VALUE,'<OLD_VALUE>','<NEW_VALUE>') as RULE_MESSAGE_VALUE_after from AO_9B2E3B_EXEC_RULE_MSG_ITEM where LOWER(RULE_MESSAGE_VALUE) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update AO_9B2E3B_EXEC_RULE_MSG_ITEM set RULE_MESSAGE_VALUE = replace(RULE_MESSAGE_VALUE,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(RULE_MESSAGE_VALUE) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_9B2E3B_IF_COND_CONF_DATA.*,CONFIG_DATA_VALUE as CONFIG_DATA_VALUE_before,replace(CONFIG_DATA_VALUE,'<OLD_VALUE>','<NEW_VALUE>') as CONFIG_DATA_VALUE_after from AO_9B2E3B_IF_COND_CONF_DATA where LOWER(CONFIG_DATA_VALUE) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update AO_9B2E3B_IF_COND_CONF_DATA set CONFIG_DATA_VALUE = replace(CONFIG_DATA_VALUE,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(CONFIG_DATA_VALUE) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_9B2E3B_RULESET_REVISION.*,DESCRIPTION as DESCRIPTION_before,replace(DESCRIPTION,'<OLD_VALUE>','<NEW_VALUE>') as DESCRIPTION_after from AO_9B2E3B_RULESET_REVISION where LOWER(DESCRIPTION) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update AO_9B2E3B_RULESET_REVISION set DESCRIPTION = replace(DESCRIPTION,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(DESCRIPTION) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_9B2E3B_RULESET_REVISION.*,NAME as NAME_before,replace(NAME,'<OLD_VALUE>','<NEW_VALUE>') as NAME_after from AO_9B2E3B_RULESET_REVISION where LOWER(NAME) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update AO_9B2E3B_RULESET_REVISION set NAME = replace(NAME,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(NAME) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_9B2E3B_THEN_ACT_CONF_DATA.*,CONFIG_DATA_VALUE as CONFIG_DATA_VALUE_before,replace(CONFIG_DATA_VALUE,'<OLD_VALUE>','<NEW_VALUE>') as CONFIG_DATA_VALUE_after from AO_9B2E3B_THEN_ACT_CONF_DATA where LOWER(CONFIG_DATA_VALUE) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update AO_9B2E3B_THEN_ACT_CONF_DATA set CONFIG_DATA_VALUE = replace(CONFIG_DATA_VALUE,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(CONFIG_DATA_VALUE) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Canned response audit trail
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_D530BB_CRAUDITACTIONDATA.*,FROM_STRING as FROM_STRING_before,replace(FROM_STRING,'<OLD_VALUE>','<NEW_VALUE>') as FROM_STRING_after from AO_D530BB_CRAUDITACTIONDATA where LOWER(FROM_STRING) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update AO_D530BB_CRAUDITACTIONDATA set FROM_STRING = replace(FROM_STRING,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(FROM_STRING) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Canned response audit trail
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_D530BB_CRAUDITACTIONDATA.*,FROM_TEXT as FROM_TEXT_before,replace(FROM_TEXT,'<OLD_VALUE>','<NEW_VALUE>') as FROM_TEXT_after from AO_D530BB_CRAUDITACTIONDATA where LOWER(FROM_TEXT) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update AO_D530BB_CRAUDITACTIONDATA set FROM_TEXT = replace(FROM_TEXT,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(FROM_TEXT) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Canned response audit trail
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_D530BB_CRAUDITACTIONDATA.*,TO_STRING as TO_STRING_before,replace(TO_STRING,'<OLD_VALUE>','<NEW_VALUE>') as TO_STRING_after from AO_D530BB_CRAUDITACTIONDATA where LOWER(TO_STRING) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update AO_D530BB_CRAUDITACTIONDATA set TO_STRING = replace(TO_STRING,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(TO_STRING) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Canned response audit trail
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_D530BB_CRAUDITACTIONDATA.*,TO_TEXT as TO_TEXT_before,replace(TO_TEXT,'<OLD_VALUE>','<NEW_VALUE>') as TO_TEXT_after from AO_D530BB_CRAUDITACTIONDATA where LOWER(TO_TEXT) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update AO_D530BB_CRAUDITACTIONDATA set TO_TEXT = replace(TO_TEXT,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(TO_TEXT) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

