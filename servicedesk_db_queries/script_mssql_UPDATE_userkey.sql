-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base statistics associated with user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_0201F0_STATS_EVENT_PARAM.*,PARAM_NAME as PARAM_NAME_before,'<NEW_VALUE>' as PARAM_NAME_after from AO_0201F0_STATS_EVENT_PARAM where PARAM_NAME = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_0201F0_STATS_EVENT_PARAM set PARAM_NAME = '<NEW_VALUE>' where PARAM_NAME = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Email channels
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_2C4E5C_MAILCHANNEL.*,CREATED_BY as CREATED_BY_before,'<NEW_VALUE>' as CREATED_BY_after from AO_2C4E5C_MAILCHANNEL where CREATED_BY = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_2C4E5C_MAILCHANNEL set CREATED_BY = '<NEW_VALUE>' where CREATED_BY = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Email channels
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_2C4E5C_MAILCHANNEL.*,MODIFIED_BY as MODIFIED_BY_before,'<NEW_VALUE>' as MODIFIED_BY_after from AO_2C4E5C_MAILCHANNEL where MODIFIED_BY = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_2C4E5C_MAILCHANNEL set MODIFIED_BY = '<NEW_VALUE>' where MODIFIED_BY = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Email channels
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_2C4E5C_MAILCONNECTION.*,EMAIL_ADDRESS as EMAIL_ADDRESS_before,'<NEW_VALUE>' as EMAIL_ADDRESS_after from AO_2C4E5C_MAILCONNECTION where EMAIL_ADDRESS = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_2C4E5C_MAILCONNECTION set EMAIL_ADDRESS = '<NEW_VALUE>' where EMAIL_ADDRESS = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Email channels
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_2C4E5C_MAILCONNECTION.*,USER_NAME as USER_NAME_before,'<NEW_VALUE>' as USER_NAME_after from AO_2C4E5C_MAILCONNECTION where USER_NAME = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_2C4E5C_MAILCONNECTION set USER_NAME = '<NEW_VALUE>' where USER_NAME = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Email channels
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_2C4E5C_MAILITEMAUDIT.*,FROM_ADDRESS as FROM_ADDRESS_before,'<NEW_VALUE>' as FROM_ADDRESS_after from AO_2C4E5C_MAILITEMAUDIT where FROM_ADDRESS = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_2C4E5C_MAILITEMAUDIT set FROM_ADDRESS = '<NEW_VALUE>' where FROM_ADDRESS = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Email channels using a certain email account
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_54307E_EMAILCHANNELSETTING.*,EMAIL_ADDRESS as EMAIL_ADDRESS_before,'<NEW_VALUE>' as EMAIL_ADDRESS_after from AO_54307E_EMAILCHANNELSETTING where EMAIL_ADDRESS = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_54307E_EMAILCHANNELSETTING set EMAIL_ADDRESS = '<NEW_VALUE>' where EMAIL_ADDRESS = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Organisation members
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_54307E_ORGANIZATION_MEMBER.*,USER_KEY as USER_KEY_before,'<NEW_VALUE>' as USER_KEY_after from AO_54307E_ORGANIZATION_MEMBER where USER_KEY = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_54307E_ORGANIZATION_MEMBER set USER_KEY = '<NEW_VALUE>' where USER_KEY = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Organisation name
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_54307E_ORGANIZATION.*,NAME as NAME_before,'<NEW_VALUE>' as NAME_after from AO_54307E_ORGANIZATION where NAME = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_54307E_ORGANIZATION set NAME = '<NEW_VALUE>' where NAME = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Organisation name
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_54307E_ORGANIZATION.*,SEARCH_NAME as SEARCH_NAME_before,'<NEW_VALUE>' as SEARCH_NAME_after from AO_54307E_ORGANIZATION where SEARCH_NAME = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_54307E_ORGANIZATION set SEARCH_NAME = '<NEW_VALUE>' where SEARCH_NAME = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Request subscriptions for user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_54307E_SUBSCRIPTION.*,USER_KEY as USER_KEY_before,'<NEW_VALUE>' as USER_KEY_after from AO_54307E_SUBSCRIPTION where USER_KEY = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_54307E_SUBSCRIPTION set USER_KEY = '<NEW_VALUE>' where USER_KEY = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Approver decisions made by user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_56464C_APPROVERDECISION.*,USER_KEY as USER_KEY_before,'<NEW_VALUE>' as USER_KEY_after from AO_56464C_APPROVERDECISION where USER_KEY = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_56464C_APPROVERDECISION set USER_KEY = '<NEW_VALUE>' where USER_KEY = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : User invitations
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_97EDAB_USERINVITATION.*,EMAIL_ADDRESS as EMAIL_ADDRESS_before,'<NEW_VALUE>' as EMAIL_ADDRESS_after from AO_97EDAB_USERINVITATION where EMAIL_ADDRESS = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_97EDAB_USERINVITATION set EMAIL_ADDRESS = '<NEW_VALUE>' where EMAIL_ADDRESS = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : User invitations
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_97EDAB_USERINVITATION.*,SENDER_USERNAME as SENDER_USERNAME_before,'<NEW_VALUE>' as SENDER_USERNAME_after from AO_97EDAB_USERINVITATION where SENDER_USERNAME = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_97EDAB_USERINVITATION set SENDER_USERNAME = '<NEW_VALUE>' where SENDER_USERNAME = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_9B2E3B_PROJECT_USER_CONTEXT.*,USER_KEY as USER_KEY_before,'<NEW_VALUE>' as USER_KEY_after from AO_9B2E3B_PROJECT_USER_CONTEXT where USER_KEY = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_9B2E3B_PROJECT_USER_CONTEXT set USER_KEY = '<NEW_VALUE>' where USER_KEY = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_9B2E3B_RULESET_REVISION.*,CREATED_BY as CREATED_BY_before,'<NEW_VALUE>' as CREATED_BY_after from AO_9B2E3B_RULESET_REVISION where CREATED_BY = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_9B2E3B_RULESET_REVISION set CREATED_BY = '<NEW_VALUE>' where CREATED_BY = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Automation, automation executor
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_9B2E3B_RULE_EXECUTION.*,EXECUTOR_USER_KEY as EXECUTOR_USER_KEY_before,'<NEW_VALUE>' as EXECUTOR_USER_KEY_after from AO_9B2E3B_RULE_EXECUTION where EXECUTOR_USER_KEY = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_9B2E3B_RULE_EXECUTION set EXECUTOR_USER_KEY = '<NEW_VALUE>' where EXECUTOR_USER_KEY = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Notification templates
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_C7F17E_PROJECT_LANG_REV.*,AUTHOR_USER_KEY as AUTHOR_USER_KEY_before,'<NEW_VALUE>' as AUTHOR_USER_KEY_after from AO_C7F17E_PROJECT_LANG_REV where AUTHOR_USER_KEY = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_C7F17E_PROJECT_LANG_REV set AUTHOR_USER_KEY = '<NEW_VALUE>' where AUTHOR_USER_KEY = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Canned responses created by an user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_D530BB_CANNEDRESPONSEAUDIT.*,USER_KEY as USER_KEY_before,'<NEW_VALUE>' as USER_KEY_after from AO_D530BB_CANNEDRESPONSEAUDIT where USER_KEY = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_D530BB_CANNEDRESPONSEAUDIT set USER_KEY = '<NEW_VALUE>' where USER_KEY = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Canned response user usage
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_D530BB_CANNEDRESPONSEUSAGE.*,USER_KEY as USER_KEY_before,'<NEW_VALUE>' as USER_KEY_after from AO_D530BB_CANNEDRESPONSEUSAGE where USER_KEY = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_D530BB_CANNEDRESPONSEUSAGE set USER_KEY = '<NEW_VALUE>' where USER_KEY = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Canned responses created/updated by an user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_D530BB_CANNEDRESPONSE.*,CREATED_USER_KEY as CREATED_USER_KEY_before,'<NEW_VALUE>' as CREATED_USER_KEY_after from AO_D530BB_CANNEDRESPONSE where CREATED_USER_KEY = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_D530BB_CANNEDRESPONSE set CREATED_USER_KEY = '<NEW_VALUE>' where CREATED_USER_KEY = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Canned responses created/updated by an user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_D530BB_CANNEDRESPONSE.*,UPDATED_USER_KEY as UPDATED_USER_KEY_before,'<NEW_VALUE>' as UPDATED_USER_KEY_after from AO_D530BB_CANNEDRESPONSE where UPDATED_USER_KEY = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_D530BB_CANNEDRESPONSE set UPDATED_USER_KEY = '<NEW_VALUE>' where UPDATED_USER_KEY = '<OLD_VALUE>' ;

