-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base statistics associated with user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_0201F0_STATS_EVENT_PARAM.* from AO_0201F0_STATS_EVENT_PARAM where LOWER(PARAM_VALUE) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Email channels
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_2C4E5C_MAILITEMAUDIT.* from AO_2C4E5C_MAILITEMAUDIT where LOWER(ISSUE_KEY) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Email channels
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_2C4E5C_MAILITEMAUDIT.* from AO_2C4E5C_MAILITEMAUDIT where LOWER(MESSAGE) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Email channels
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_2C4E5C_MAILITEMAUDIT.* from AO_2C4E5C_MAILITEMAUDIT where LOWER(SUBJECT) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Email notifications
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_4E8AE6_NOTIF_BATCH_QUEUE.* from AO_4E8AE6_NOTIF_BATCH_QUEUE where LOWER(HTML_CONTENT) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Email notifications
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_4E8AE6_NOTIF_BATCH_QUEUE.* from AO_4E8AE6_NOTIF_BATCH_QUEUE where LOWER(TEXT_CONTENT) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base labels with personal data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_CONFLUENCEKBLABELS.* from AO_54307E_CONFLUENCEKBLABELS where LOWER(LABEL) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base links with personal data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_54307E_CONFLUENCEKB.* from AO_54307E_CONFLUENCEKB where LOWER(APPLINK_NAME) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base links with personal data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_54307E_CONFLUENCEKB.* from AO_54307E_CONFLUENCEKB where LOWER(SPACE_NAME) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base links with personal data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_54307E_CONFLUENCEKB.* from AO_54307E_CONFLUENCEKB where LOWER(SPACE_URL) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : SLA Goal
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_GOAL.* from AO_54307E_GOAL where LOWER(JQL_QUERY) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Queues
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_QUEUE.* from AO_54307E_QUEUE where LOWER(JQL) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Queues
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_QUEUE.* from AO_54307E_QUEUE where LOWER(NAME) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Reports
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_REPORT.* from AO_54307E_REPORT where LOWER(NAME) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Series
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_SERIES.* from AO_54307E_SERIES where LOWER(JQL) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Series
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_SERIES.* from AO_54307E_SERIES where LOWER(SERIES_LABEL) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : SLA audit log data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_54307E_SLAAUDITLOGDATA.* from AO_54307E_SLAAUDITLOGDATA where LOWER(VALUE) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : SLA configuration
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_TIMEMETRIC.* from AO_54307E_TIMEMETRIC where LOWER(NAME) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Request type field
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_VIEWPORTFIELD.* from AO_54307E_VIEWPORTFIELD where LOWER(DESCRIPTION) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Request type field
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_VIEWPORTFIELD.* from AO_54307E_VIEWPORTFIELD where LOWER(LABEL) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Request type
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_VIEWPORTFORM.* from AO_54307E_VIEWPORTFORM where LOWER(DESCRIPTION) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Request type
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_VIEWPORTFORM.* from AO_54307E_VIEWPORTFORM where LOWER(NAME) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Portal
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_VIEWPORT.* from AO_54307E_VIEWPORT where LOWER(DESCRIPTION) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Portal
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_VIEWPORT.* from AO_54307E_VIEWPORT where LOWER(`KEY`) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Portal
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_VIEWPORT.* from AO_54307E_VIEWPORT where LOWER(NAME) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Calendars
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_7A2604_CALENDAR.* from AO_7A2604_CALENDAR where LOWER(NAME) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Calendar holiday
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_7A2604_HOLIDAY.* from AO_7A2604_HOLIDAY where LOWER(NAME) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_9B2E3B_EXEC_RULE_MSG_ITEM.* from AO_9B2E3B_EXEC_RULE_MSG_ITEM where LOWER(RULE_MESSAGE_VALUE) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_9B2E3B_IF_COND_CONF_DATA.* from AO_9B2E3B_IF_COND_CONF_DATA where LOWER(CONFIG_DATA_VALUE) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_9B2E3B_RULESET_REVISION.* from AO_9B2E3B_RULESET_REVISION where LOWER(DESCRIPTION) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_9B2E3B_RULESET_REVISION.* from AO_9B2E3B_RULESET_REVISION where LOWER(NAME) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_9B2E3B_THEN_ACT_CONF_DATA.* from AO_9B2E3B_THEN_ACT_CONF_DATA where LOWER(CONFIG_DATA_VALUE) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Notification section
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_C7F17E_LINGO_TRANSLATION.* from AO_C7F17E_LINGO_TRANSLATION where LOWER(CONTENT) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Canned responses created/updated by an user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_D530BB_CANNEDRESPONSE.* from AO_D530BB_CANNEDRESPONSE where LOWER(TEXT) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Canned responses created/updated by an user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_D530BB_CANNEDRESPONSE.* from AO_D530BB_CANNEDRESPONSE where LOWER(TITLE) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Canned response audit trail
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_D530BB_CRAUDITACTIONDATA.* from AO_D530BB_CRAUDITACTIONDATA where LOWER(FROM_STRING) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Canned response audit trail
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_D530BB_CRAUDITACTIONDATA.* from AO_D530BB_CRAUDITACTIONDATA where LOWER(FROM_TEXT) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Canned response audit trail
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_D530BB_CRAUDITACTIONDATA.* from AO_D530BB_CRAUDITACTIONDATA where LOWER(TO_STRING) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Canned response audit trail
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

select AO_D530BB_CRAUDITACTIONDATA.* from AO_D530BB_CRAUDITACTIONDATA where LOWER(TO_TEXT) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

