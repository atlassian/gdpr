-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base statistics associated with user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

select AO_0201F0_STATS_EVENT_PARAM.* from AO_0201F0_STATS_EVENT_PARAM where REGEXP_LIKE (LOWER(PARAM_VALUE),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Email channels
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_2C4E5C_MAILITEMAUDIT.* from AO_2C4E5C_MAILITEMAUDIT where REGEXP_LIKE (LOWER(ISSUE_KEY),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Email channels
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_2C4E5C_MAILITEMAUDIT.* from AO_2C4E5C_MAILITEMAUDIT where REGEXP_LIKE (LOWER(MESSAGE),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Email channels
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_2C4E5C_MAILITEMAUDIT.* from AO_2C4E5C_MAILITEMAUDIT where REGEXP_LIKE (LOWER(SUBJECT),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Email notifications
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

select AO_4E8AE6_NOTIF_BATCH_QUEUE.* from AO_4E8AE6_NOTIF_BATCH_QUEUE where REGEXP_LIKE (LOWER(HTML_CONTENT),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Email notifications
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

select AO_4E8AE6_NOTIF_BATCH_QUEUE.* from AO_4E8AE6_NOTIF_BATCH_QUEUE where REGEXP_LIKE (LOWER(TEXT_CONTENT),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base labels with personal data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_CONFLUENCEKBLABELS.* from AO_54307E_CONFLUENCEKBLABELS where REGEXP_LIKE (LOWER(LABEL),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base links with personal data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

select AO_54307E_CONFLUENCEKB.* from AO_54307E_CONFLUENCEKB where REGEXP_LIKE (LOWER(APPLINK_NAME),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base links with personal data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

select AO_54307E_CONFLUENCEKB.* from AO_54307E_CONFLUENCEKB where REGEXP_LIKE (LOWER(SPACE_NAME),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base links with personal data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

select AO_54307E_CONFLUENCEKB.* from AO_54307E_CONFLUENCEKB where REGEXP_LIKE (LOWER(SPACE_URL),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : SLA Goal
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_GOAL.* from AO_54307E_GOAL where REGEXP_LIKE (LOWER(JQL_QUERY),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Queues
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_QUEUE.* from AO_54307E_QUEUE where REGEXP_LIKE (LOWER(JQL),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Queues
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_QUEUE.* from AO_54307E_QUEUE where REGEXP_LIKE (LOWER(NAME),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Reports
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_REPORT.* from AO_54307E_REPORT where REGEXP_LIKE (LOWER(NAME),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Series
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_SERIES.* from AO_54307E_SERIES where REGEXP_LIKE (LOWER(JQL),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Series
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_SERIES.* from AO_54307E_SERIES where REGEXP_LIKE (LOWER(SERIES_LABEL),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : SLA audit log data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

select AO_54307E_SLAAUDITLOGDATA.* from AO_54307E_SLAAUDITLOGDATA where REGEXP_LIKE (LOWER(VALUE),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : SLA configuration
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_TIMEMETRIC.* from AO_54307E_TIMEMETRIC where REGEXP_LIKE (LOWER(NAME),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Request type field
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_VIEWPORTFIELD.* from AO_54307E_VIEWPORTFIELD where REGEXP_LIKE (LOWER(DESCRIPTION),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Request type field
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_VIEWPORTFIELD.* from AO_54307E_VIEWPORTFIELD where REGEXP_LIKE (LOWER(LABEL),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Request type
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_VIEWPORTFORM.* from AO_54307E_VIEWPORTFORM where REGEXP_LIKE (LOWER(DESCRIPTION),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Request type
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_VIEWPORTFORM.* from AO_54307E_VIEWPORTFORM where REGEXP_LIKE (LOWER(NAME),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Portal
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_VIEWPORT.* from AO_54307E_VIEWPORT where REGEXP_LIKE (LOWER(DESCRIPTION),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Portal
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_VIEWPORT.* from AO_54307E_VIEWPORT where REGEXP_LIKE (LOWER(KEY),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Portal
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_VIEWPORT.* from AO_54307E_VIEWPORT where REGEXP_LIKE (LOWER(NAME),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Calendars
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_7A2604_CALENDAR.* from AO_7A2604_CALENDAR where REGEXP_LIKE (LOWER(NAME),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Calendar holiday
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_7A2604_HOLIDAY.* from AO_7A2604_HOLIDAY where REGEXP_LIKE (LOWER(NAME),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

select AO_9B2E3B_EXEC_RULE_MSG_ITEM.* from AO_9B2E3B_EXEC_RULE_MSG_ITEM where REGEXP_LIKE (LOWER(RULE_MESSAGE_VALUE),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

select AO_9B2E3B_IF_COND_CONF_DATA.* from AO_9B2E3B_IF_COND_CONF_DATA where REGEXP_LIKE (LOWER(CONFIG_DATA_VALUE),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

select AO_9B2E3B_RULESET_REVISION.* from AO_9B2E3B_RULESET_REVISION where REGEXP_LIKE (LOWER(DESCRIPTION),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

select AO_9B2E3B_RULESET_REVISION.* from AO_9B2E3B_RULESET_REVISION where REGEXP_LIKE (LOWER(NAME),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

select AO_9B2E3B_THEN_ACT_CONF_DATA.* from AO_9B2E3B_THEN_ACT_CONF_DATA where REGEXP_LIKE (LOWER(CONFIG_DATA_VALUE),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Notification section
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_C7F17E_LINGO_TRANSLATION.* from AO_C7F17E_LINGO_TRANSLATION where REGEXP_LIKE (LOWER(CONTENT),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Canned responses created/updated by an user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_D530BB_CANNEDRESPONSE.* from AO_D530BB_CANNEDRESPONSE where REGEXP_LIKE (LOWER(TEXT),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Canned responses created/updated by an user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_D530BB_CANNEDRESPONSE.* from AO_D530BB_CANNEDRESPONSE where REGEXP_LIKE (LOWER(TITLE),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Canned response audit trail
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

select AO_D530BB_CRAUDITACTIONDATA.* from AO_D530BB_CRAUDITACTIONDATA where REGEXP_LIKE (LOWER(FROM_STRING),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Canned response audit trail
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

select AO_D530BB_CRAUDITACTIONDATA.* from AO_D530BB_CRAUDITACTIONDATA where REGEXP_LIKE (LOWER(FROM_TEXT),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Canned response audit trail
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

select AO_D530BB_CRAUDITACTIONDATA.* from AO_D530BB_CRAUDITACTIONDATA where REGEXP_LIKE (LOWER(TO_STRING),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Canned response audit trail
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : oracle

select AO_D530BB_CRAUDITACTIONDATA.* from AO_D530BB_CRAUDITACTIONDATA where REGEXP_LIKE (LOWER(TO_TEXT),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

