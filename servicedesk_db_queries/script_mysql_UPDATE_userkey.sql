-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base statistics associated with user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_0201F0_STATS_EVENT_PARAM.*,PARAM_NAME as PARAM_NAME_before,'<NEW_VALUE>' as PARAM_NAME_after from AO_0201F0_STATS_EVENT_PARAM where LOWER(PARAM_NAME) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_0201F0_STATS_EVENT_PARAM set PARAM_NAME = '<NEW_VALUE>' where LOWER(PARAM_NAME) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Email channels
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_2C4E5C_MAILCHANNEL.*,CREATED_BY as CREATED_BY_before,'<NEW_VALUE>' as CREATED_BY_after from AO_2C4E5C_MAILCHANNEL where LOWER(CREATED_BY) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_2C4E5C_MAILCHANNEL set CREATED_BY = '<NEW_VALUE>' where LOWER(CREATED_BY) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Email channels
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_2C4E5C_MAILCHANNEL.*,MODIFIED_BY as MODIFIED_BY_before,'<NEW_VALUE>' as MODIFIED_BY_after from AO_2C4E5C_MAILCHANNEL where LOWER(MODIFIED_BY) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_2C4E5C_MAILCHANNEL set MODIFIED_BY = '<NEW_VALUE>' where LOWER(MODIFIED_BY) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Email channels
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_2C4E5C_MAILCONNECTION.*,EMAIL_ADDRESS as EMAIL_ADDRESS_before,'<NEW_VALUE>' as EMAIL_ADDRESS_after from AO_2C4E5C_MAILCONNECTION where LOWER(EMAIL_ADDRESS) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_2C4E5C_MAILCONNECTION set EMAIL_ADDRESS = '<NEW_VALUE>' where LOWER(EMAIL_ADDRESS) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Email channels
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_2C4E5C_MAILCONNECTION.*,USER_NAME as USER_NAME_before,'<NEW_VALUE>' as USER_NAME_after from AO_2C4E5C_MAILCONNECTION where LOWER(USER_NAME) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_2C4E5C_MAILCONNECTION set USER_NAME = '<NEW_VALUE>' where LOWER(USER_NAME) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Email channels
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_2C4E5C_MAILITEMAUDIT.*,FROM_ADDRESS as FROM_ADDRESS_before,'<NEW_VALUE>' as FROM_ADDRESS_after from AO_2C4E5C_MAILITEMAUDIT where LOWER(FROM_ADDRESS) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_2C4E5C_MAILITEMAUDIT set FROM_ADDRESS = '<NEW_VALUE>' where LOWER(FROM_ADDRESS) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Email channels using a certain email account
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_54307E_EMAILCHANNELSETTING.*,EMAIL_ADDRESS as EMAIL_ADDRESS_before,'<NEW_VALUE>' as EMAIL_ADDRESS_after from AO_54307E_EMAILCHANNELSETTING where LOWER(EMAIL_ADDRESS) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_54307E_EMAILCHANNELSETTING set EMAIL_ADDRESS = '<NEW_VALUE>' where LOWER(EMAIL_ADDRESS) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Organisation members
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_54307E_ORGANIZATION_MEMBER.*,USER_KEY as USER_KEY_before,'<NEW_VALUE>' as USER_KEY_after from AO_54307E_ORGANIZATION_MEMBER where LOWER(USER_KEY) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_54307E_ORGANIZATION_MEMBER set USER_KEY = '<NEW_VALUE>' where LOWER(USER_KEY) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Organisation name
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_54307E_ORGANIZATION.*,NAME as NAME_before,'<NEW_VALUE>' as NAME_after from AO_54307E_ORGANIZATION where LOWER(NAME) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_54307E_ORGANIZATION set NAME = '<NEW_VALUE>' where LOWER(NAME) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Organisation name
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_54307E_ORGANIZATION.*,SEARCH_NAME as SEARCH_NAME_before,'<NEW_VALUE>' as SEARCH_NAME_after from AO_54307E_ORGANIZATION where LOWER(SEARCH_NAME) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_54307E_ORGANIZATION set SEARCH_NAME = '<NEW_VALUE>' where LOWER(SEARCH_NAME) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Request subscriptions for user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_54307E_SUBSCRIPTION.*,USER_KEY as USER_KEY_before,'<NEW_VALUE>' as USER_KEY_after from AO_54307E_SUBSCRIPTION where LOWER(USER_KEY) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_54307E_SUBSCRIPTION set USER_KEY = '<NEW_VALUE>' where LOWER(USER_KEY) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Approver decisions made by user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_56464C_APPROVERDECISION.*,USER_KEY as USER_KEY_before,'<NEW_VALUE>' as USER_KEY_after from AO_56464C_APPROVERDECISION where LOWER(USER_KEY) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_56464C_APPROVERDECISION set USER_KEY = '<NEW_VALUE>' where LOWER(USER_KEY) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : User invitations
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_97EDAB_USERINVITATION.*,EMAIL_ADDRESS as EMAIL_ADDRESS_before,'<NEW_VALUE>' as EMAIL_ADDRESS_after from AO_97EDAB_USERINVITATION where LOWER(EMAIL_ADDRESS) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_97EDAB_USERINVITATION set EMAIL_ADDRESS = '<NEW_VALUE>' where LOWER(EMAIL_ADDRESS) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : User invitations
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_97EDAB_USERINVITATION.*,SENDER_USERNAME as SENDER_USERNAME_before,'<NEW_VALUE>' as SENDER_USERNAME_after from AO_97EDAB_USERINVITATION where LOWER(SENDER_USERNAME) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_97EDAB_USERINVITATION set SENDER_USERNAME = '<NEW_VALUE>' where LOWER(SENDER_USERNAME) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_9B2E3B_PROJECT_USER_CONTEXT.*,USER_KEY as USER_KEY_before,'<NEW_VALUE>' as USER_KEY_after from AO_9B2E3B_PROJECT_USER_CONTEXT where LOWER(USER_KEY) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_9B2E3B_PROJECT_USER_CONTEXT set USER_KEY = '<NEW_VALUE>' where LOWER(USER_KEY) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_9B2E3B_RULESET_REVISION.*,CREATED_BY as CREATED_BY_before,'<NEW_VALUE>' as CREATED_BY_after from AO_9B2E3B_RULESET_REVISION where LOWER(CREATED_BY) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_9B2E3B_RULESET_REVISION set CREATED_BY = '<NEW_VALUE>' where LOWER(CREATED_BY) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Automation, automation executor
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_9B2E3B_RULE_EXECUTION.*,EXECUTOR_USER_KEY as EXECUTOR_USER_KEY_before,'<NEW_VALUE>' as EXECUTOR_USER_KEY_after from AO_9B2E3B_RULE_EXECUTION where LOWER(EXECUTOR_USER_KEY) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_9B2E3B_RULE_EXECUTION set EXECUTOR_USER_KEY = '<NEW_VALUE>' where LOWER(EXECUTOR_USER_KEY) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Notification templates
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_C7F17E_PROJECT_LANG_REV.*,AUTHOR_USER_KEY as AUTHOR_USER_KEY_before,'<NEW_VALUE>' as AUTHOR_USER_KEY_after from AO_C7F17E_PROJECT_LANG_REV where LOWER(AUTHOR_USER_KEY) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_C7F17E_PROJECT_LANG_REV set AUTHOR_USER_KEY = '<NEW_VALUE>' where LOWER(AUTHOR_USER_KEY) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Canned responses created by an user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_D530BB_CANNEDRESPONSEAUDIT.*,USER_KEY as USER_KEY_before,'<NEW_VALUE>' as USER_KEY_after from AO_D530BB_CANNEDRESPONSEAUDIT where LOWER(USER_KEY) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_D530BB_CANNEDRESPONSEAUDIT set USER_KEY = '<NEW_VALUE>' where LOWER(USER_KEY) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Canned response user usage
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- + SELECT (please review changes BEFORE)
select AO_D530BB_CANNEDRESPONSEUSAGE.*,USER_KEY as USER_KEY_before,'<NEW_VALUE>' as USER_KEY_after from AO_D530BB_CANNEDRESPONSEUSAGE where LOWER(USER_KEY) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_D530BB_CANNEDRESPONSEUSAGE set USER_KEY = '<NEW_VALUE>' where LOWER(USER_KEY) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Canned responses created/updated by an user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_D530BB_CANNEDRESPONSE.*,CREATED_USER_KEY as CREATED_USER_KEY_before,'<NEW_VALUE>' as CREATED_USER_KEY_after from AO_D530BB_CANNEDRESPONSE where LOWER(CREATED_USER_KEY) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_D530BB_CANNEDRESPONSE set CREATED_USER_KEY = '<NEW_VALUE>' where LOWER(CREATED_USER_KEY) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Canned responses created/updated by an user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mysql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
-- 
-- Update via SQL:
-- + SELECT (please review changes BEFORE)
select AO_D530BB_CANNEDRESPONSE.*,UPDATED_USER_KEY as UPDATED_USER_KEY_before,'<NEW_VALUE>' as UPDATED_USER_KEY_after from AO_D530BB_CANNEDRESPONSE where LOWER(UPDATED_USER_KEY) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_D530BB_CANNEDRESPONSE set UPDATED_USER_KEY = '<NEW_VALUE>' where LOWER(UPDATED_USER_KEY) = LOWER('<OLD_VALUE>') ;

