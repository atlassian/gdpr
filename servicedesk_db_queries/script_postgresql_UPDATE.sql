-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base statistics associated with user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : postgresql

-- + SELECT (please review changes BEFORE)
select "AO_0201F0_STATS_EVENT_PARAM".*,"PARAM_VALUE" as PARAM_VALUE_before,REGEXP_REPLACE("PARAM_VALUE",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as PARAM_VALUE_after from "AO_0201F0_STATS_EVENT_PARAM" where LOWER("PARAM_VALUE") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update "AO_0201F0_STATS_EVENT_PARAM" set "PARAM_VALUE" = REGEXP_REPLACE("PARAM_VALUE",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER("PARAM_VALUE") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Email notifications
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : postgresql

-- + SELECT (please review changes BEFORE)
select "AO_4E8AE6_NOTIF_BATCH_QUEUE".*,"HTML_CONTENT" as HTML_CONTENT_before,REGEXP_REPLACE("HTML_CONTENT",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as HTML_CONTENT_after from "AO_4E8AE6_NOTIF_BATCH_QUEUE" where LOWER("HTML_CONTENT") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update "AO_4E8AE6_NOTIF_BATCH_QUEUE" set "HTML_CONTENT" = REGEXP_REPLACE("HTML_CONTENT",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER("HTML_CONTENT") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Email notifications
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : postgresql

-- + SELECT (please review changes BEFORE)
select "AO_4E8AE6_NOTIF_BATCH_QUEUE".*,"TEXT_CONTENT" as TEXT_CONTENT_before,REGEXP_REPLACE("TEXT_CONTENT",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as TEXT_CONTENT_after from "AO_4E8AE6_NOTIF_BATCH_QUEUE" where LOWER("TEXT_CONTENT") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update "AO_4E8AE6_NOTIF_BATCH_QUEUE" set "TEXT_CONTENT" = REGEXP_REPLACE("TEXT_CONTENT",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER("TEXT_CONTENT") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base links with personal data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : postgresql

-- + SELECT (please review changes BEFORE)
select "AO_54307E_CONFLUENCEKB".*,"APPLINK_NAME" as APPLINK_NAME_before,REGEXP_REPLACE("APPLINK_NAME",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as APPLINK_NAME_after from "AO_54307E_CONFLUENCEKB" where LOWER("APPLINK_NAME") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update "AO_54307E_CONFLUENCEKB" set "APPLINK_NAME" = REGEXP_REPLACE("APPLINK_NAME",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER("APPLINK_NAME") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base links with personal data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : postgresql

-- + SELECT (please review changes BEFORE)
select "AO_54307E_CONFLUENCEKB".*,"SPACE_NAME" as SPACE_NAME_before,REGEXP_REPLACE("SPACE_NAME",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as SPACE_NAME_after from "AO_54307E_CONFLUENCEKB" where LOWER("SPACE_NAME") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update "AO_54307E_CONFLUENCEKB" set "SPACE_NAME" = REGEXP_REPLACE("SPACE_NAME",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER("SPACE_NAME") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base links with personal data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : postgresql

-- + SELECT (please review changes BEFORE)
select "AO_54307E_CONFLUENCEKB".*,"SPACE_URL" as SPACE_URL_before,REGEXP_REPLACE("SPACE_URL",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as SPACE_URL_after from "AO_54307E_CONFLUENCEKB" where LOWER("SPACE_URL") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update "AO_54307E_CONFLUENCEKB" set "SPACE_URL" = REGEXP_REPLACE("SPACE_URL",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER("SPACE_URL") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : SLA audit log data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : postgresql

-- + SELECT (please review changes BEFORE)
select "AO_54307E_SLAAUDITLOGDATA".*,"VALUE" as VALUE_before,REGEXP_REPLACE("VALUE",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as VALUE_after from "AO_54307E_SLAAUDITLOGDATA" where LOWER("VALUE") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update "AO_54307E_SLAAUDITLOGDATA" set "VALUE" = REGEXP_REPLACE("VALUE",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER("VALUE") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : postgresql

-- + SELECT (please review changes BEFORE)
select "AO_9B2E3B_EXEC_RULE_MSG_ITEM".*,"RULE_MESSAGE_VALUE" as RULE_MESSAGE_VALUE_before,REGEXP_REPLACE("RULE_MESSAGE_VALUE",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as RULE_MESSAGE_VALUE_after from "AO_9B2E3B_EXEC_RULE_MSG_ITEM" where LOWER("RULE_MESSAGE_VALUE") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update "AO_9B2E3B_EXEC_RULE_MSG_ITEM" set "RULE_MESSAGE_VALUE" = REGEXP_REPLACE("RULE_MESSAGE_VALUE",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER("RULE_MESSAGE_VALUE") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : postgresql

-- + SELECT (please review changes BEFORE)
select "AO_9B2E3B_IF_COND_CONF_DATA".*,"CONFIG_DATA_VALUE" as CONFIG_DATA_VALUE_before,REGEXP_REPLACE("CONFIG_DATA_VALUE",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as CONFIG_DATA_VALUE_after from "AO_9B2E3B_IF_COND_CONF_DATA" where LOWER("CONFIG_DATA_VALUE") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update "AO_9B2E3B_IF_COND_CONF_DATA" set "CONFIG_DATA_VALUE" = REGEXP_REPLACE("CONFIG_DATA_VALUE",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER("CONFIG_DATA_VALUE") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : postgresql

-- + SELECT (please review changes BEFORE)
select "AO_9B2E3B_RULESET_REVISION".*,"DESCRIPTION" as DESCRIPTION_before,REGEXP_REPLACE("DESCRIPTION",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as DESCRIPTION_after from "AO_9B2E3B_RULESET_REVISION" where LOWER("DESCRIPTION") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update "AO_9B2E3B_RULESET_REVISION" set "DESCRIPTION" = REGEXP_REPLACE("DESCRIPTION",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER("DESCRIPTION") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : postgresql

-- + SELECT (please review changes BEFORE)
select "AO_9B2E3B_RULESET_REVISION".*,"NAME" as NAME_before,REGEXP_REPLACE("NAME",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as NAME_after from "AO_9B2E3B_RULESET_REVISION" where LOWER("NAME") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update "AO_9B2E3B_RULESET_REVISION" set "NAME" = REGEXP_REPLACE("NAME",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER("NAME") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : postgresql

-- + SELECT (please review changes BEFORE)
select "AO_9B2E3B_THEN_ACT_CONF_DATA".*,"CONFIG_DATA_VALUE" as CONFIG_DATA_VALUE_before,REGEXP_REPLACE("CONFIG_DATA_VALUE",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as CONFIG_DATA_VALUE_after from "AO_9B2E3B_THEN_ACT_CONF_DATA" where LOWER("CONFIG_DATA_VALUE") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update "AO_9B2E3B_THEN_ACT_CONF_DATA" set "CONFIG_DATA_VALUE" = REGEXP_REPLACE("CONFIG_DATA_VALUE",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER("CONFIG_DATA_VALUE") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Canned response audit trail
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : postgresql

-- + SELECT (please review changes BEFORE)
select "AO_D530BB_CRAUDITACTIONDATA".*,"FROM_STRING" as FROM_STRING_before,REGEXP_REPLACE("FROM_STRING",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as FROM_STRING_after from "AO_D530BB_CRAUDITACTIONDATA" where LOWER("FROM_STRING") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update "AO_D530BB_CRAUDITACTIONDATA" set "FROM_STRING" = REGEXP_REPLACE("FROM_STRING",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER("FROM_STRING") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Canned response audit trail
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : postgresql

-- + SELECT (please review changes BEFORE)
select "AO_D530BB_CRAUDITACTIONDATA".*,"FROM_TEXT" as FROM_TEXT_before,REGEXP_REPLACE("FROM_TEXT",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as FROM_TEXT_after from "AO_D530BB_CRAUDITACTIONDATA" where LOWER("FROM_TEXT") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update "AO_D530BB_CRAUDITACTIONDATA" set "FROM_TEXT" = REGEXP_REPLACE("FROM_TEXT",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER("FROM_TEXT") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Canned response audit trail
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : postgresql

-- + SELECT (please review changes BEFORE)
select "AO_D530BB_CRAUDITACTIONDATA".*,"TO_STRING" as TO_STRING_before,REGEXP_REPLACE("TO_STRING",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as TO_STRING_after from "AO_D530BB_CRAUDITACTIONDATA" where LOWER("TO_STRING") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update "AO_D530BB_CRAUDITACTIONDATA" set "TO_STRING" = REGEXP_REPLACE("TO_STRING",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER("TO_STRING") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Canned response audit trail
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : postgresql

-- + SELECT (please review changes BEFORE)
select "AO_D530BB_CRAUDITACTIONDATA".*,"TO_TEXT" as TO_TEXT_before,REGEXP_REPLACE("TO_TEXT",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as TO_TEXT_after from "AO_D530BB_CRAUDITACTIONDATA" where LOWER("TO_TEXT") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update "AO_D530BB_CRAUDITACTIONDATA" set "TO_TEXT" = REGEXP_REPLACE("TO_TEXT",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER("TO_TEXT") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

