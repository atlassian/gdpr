-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base statistics associated with user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

select AO_0201F0_STATS_EVENT_PARAM.* from AO_0201F0_STATS_EVENT_PARAM where PARAM_VALUE like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Email channels
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_2C4E5C_MAILITEMAUDIT.* from AO_2C4E5C_MAILITEMAUDIT where ISSUE_KEY like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Email channels
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_2C4E5C_MAILITEMAUDIT.* from AO_2C4E5C_MAILITEMAUDIT where MESSAGE like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Email channels
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_2C4E5C_MAILITEMAUDIT.* from AO_2C4E5C_MAILITEMAUDIT where SUBJECT like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Email notifications
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

select AO_4E8AE6_NOTIF_BATCH_QUEUE.* from AO_4E8AE6_NOTIF_BATCH_QUEUE where HTML_CONTENT like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Email notifications
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

select AO_4E8AE6_NOTIF_BATCH_QUEUE.* from AO_4E8AE6_NOTIF_BATCH_QUEUE where TEXT_CONTENT like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base labels with personal data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_CONFLUENCEKBLABELS.* from AO_54307E_CONFLUENCEKBLABELS where LABEL like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base links with personal data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

select AO_54307E_CONFLUENCEKB.* from AO_54307E_CONFLUENCEKB where APPLINK_NAME like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base links with personal data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

select AO_54307E_CONFLUENCEKB.* from AO_54307E_CONFLUENCEKB where SPACE_NAME like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base links with personal data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

select AO_54307E_CONFLUENCEKB.* from AO_54307E_CONFLUENCEKB where SPACE_URL like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : SLA Goal
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_GOAL.* from AO_54307E_GOAL where JQL_QUERY like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Queues
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_QUEUE.* from AO_54307E_QUEUE where JQL like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Queues
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_QUEUE.* from AO_54307E_QUEUE where NAME like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Reports
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_REPORT.* from AO_54307E_REPORT where NAME like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Series
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_SERIES.* from AO_54307E_SERIES where JQL like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Series
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_SERIES.* from AO_54307E_SERIES where SERIES_LABEL like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : SLA audit log data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

select AO_54307E_SLAAUDITLOGDATA.* from AO_54307E_SLAAUDITLOGDATA where VALUE like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : SLA configuration
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_TIMEMETRIC.* from AO_54307E_TIMEMETRIC where NAME like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Request type field
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_VIEWPORTFIELD.* from AO_54307E_VIEWPORTFIELD where DESCRIPTION like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Request type field
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_VIEWPORTFIELD.* from AO_54307E_VIEWPORTFIELD where LABEL like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Request type
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_VIEWPORTFORM.* from AO_54307E_VIEWPORTFORM where DESCRIPTION like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Request type
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_VIEWPORTFORM.* from AO_54307E_VIEWPORTFORM where NAME like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Portal
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_VIEWPORT.* from AO_54307E_VIEWPORT where DESCRIPTION like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Portal
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_VIEWPORT.* from AO_54307E_VIEWPORT where "KEY" like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Portal
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_54307E_VIEWPORT.* from AO_54307E_VIEWPORT where NAME like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Calendars
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_7A2604_CALENDAR.* from AO_7A2604_CALENDAR where NAME like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Calendar holiday
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_7A2604_HOLIDAY.* from AO_7A2604_HOLIDAY where NAME like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

select AO_9B2E3B_EXEC_RULE_MSG_ITEM.* from AO_9B2E3B_EXEC_RULE_MSG_ITEM where RULE_MESSAGE_VALUE like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

select AO_9B2E3B_IF_COND_CONF_DATA.* from AO_9B2E3B_IF_COND_CONF_DATA where CONFIG_DATA_VALUE like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

select AO_9B2E3B_RULESET_REVISION.* from AO_9B2E3B_RULESET_REVISION where DESCRIPTION like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

select AO_9B2E3B_RULESET_REVISION.* from AO_9B2E3B_RULESET_REVISION where NAME like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

select AO_9B2E3B_THEN_ACT_CONF_DATA.* from AO_9B2E3B_THEN_ACT_CONF_DATA where CONFIG_DATA_VALUE like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Notification section
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_C7F17E_LINGO_TRANSLATION.* from AO_C7F17E_LINGO_TRANSLATION where CONTENT like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Canned responses created/updated by an user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_D530BB_CANNEDRESPONSE.* from AO_D530BB_CANNEDRESPONSE where TEXT like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Canned responses created/updated by an user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://JIRA_URL/
-- 
select AO_D530BB_CANNEDRESPONSE.* from AO_D530BB_CANNEDRESPONSE where TITLE like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Canned response audit trail
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

select AO_D530BB_CRAUDITACTIONDATA.* from AO_D530BB_CRAUDITACTIONDATA where FROM_STRING like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Canned response audit trail
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

select AO_D530BB_CRAUDITACTIONDATA.* from AO_D530BB_CRAUDITACTIONDATA where FROM_TEXT like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Canned response audit trail
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

select AO_D530BB_CRAUDITACTIONDATA.* from AO_D530BB_CRAUDITACTIONDATA where TO_STRING like '%<OLD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira ServiceDesk
-- Description : Canned response audit trail
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

select AO_D530BB_CRAUDITACTIONDATA.* from AO_D530BB_CRAUDITACTIONDATA where TO_TEXT like '%<OLD_VALUE>%' ;

