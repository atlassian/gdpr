-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base statistics associated with user
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_0201F0_STATS_EVENT_PARAM.*,PARAM_VALUE as PARAM_VALUE_before,replace(PARAM_VALUE, '<OLD_VALUE>', '<NEW_VALUE>') as PARAM_VALUE_after from AO_0201F0_STATS_EVENT_PARAM where PARAM_VALUE like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_0201F0_STATS_EVENT_PARAM set PARAM_VALUE = replace(PARAM_VALUE, '<OLD_VALUE>', '<NEW_VALUE>') where PARAM_VALUE like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Email notifications
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_4E8AE6_NOTIF_BATCH_QUEUE.*,HTML_CONTENT as HTML_CONTENT_before,replace(HTML_CONTENT, '<OLD_VALUE>', '<NEW_VALUE>') as HTML_CONTENT_after from AO_4E8AE6_NOTIF_BATCH_QUEUE where HTML_CONTENT like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_4E8AE6_NOTIF_BATCH_QUEUE set HTML_CONTENT = replace(HTML_CONTENT, '<OLD_VALUE>', '<NEW_VALUE>') where HTML_CONTENT like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Email notifications
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_4E8AE6_NOTIF_BATCH_QUEUE.*,TEXT_CONTENT as TEXT_CONTENT_before,replace(TEXT_CONTENT, '<OLD_VALUE>', '<NEW_VALUE>') as TEXT_CONTENT_after from AO_4E8AE6_NOTIF_BATCH_QUEUE where TEXT_CONTENT like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_4E8AE6_NOTIF_BATCH_QUEUE set TEXT_CONTENT = replace(TEXT_CONTENT, '<OLD_VALUE>', '<NEW_VALUE>') where TEXT_CONTENT like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base links with personal data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_54307E_CONFLUENCEKB.*,APPLINK_NAME as APPLINK_NAME_before,replace(APPLINK_NAME, '<OLD_VALUE>', '<NEW_VALUE>') as APPLINK_NAME_after from AO_54307E_CONFLUENCEKB where APPLINK_NAME like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_54307E_CONFLUENCEKB set APPLINK_NAME = replace(APPLINK_NAME, '<OLD_VALUE>', '<NEW_VALUE>') where APPLINK_NAME like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base links with personal data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_54307E_CONFLUENCEKB.*,SPACE_NAME as SPACE_NAME_before,replace(SPACE_NAME, '<OLD_VALUE>', '<NEW_VALUE>') as SPACE_NAME_after from AO_54307E_CONFLUENCEKB where SPACE_NAME like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_54307E_CONFLUENCEKB set SPACE_NAME = replace(SPACE_NAME, '<OLD_VALUE>', '<NEW_VALUE>') where SPACE_NAME like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Knowledge base links with personal data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_54307E_CONFLUENCEKB.*,SPACE_URL as SPACE_URL_before,replace(SPACE_URL, '<OLD_VALUE>', '<NEW_VALUE>') as SPACE_URL_after from AO_54307E_CONFLUENCEKB where SPACE_URL like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_54307E_CONFLUENCEKB set SPACE_URL = replace(SPACE_URL, '<OLD_VALUE>', '<NEW_VALUE>') where SPACE_URL like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : SLA audit log data
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_54307E_SLAAUDITLOGDATA.*,VALUE as VALUE_before,replace(VALUE, '<OLD_VALUE>', '<NEW_VALUE>') as VALUE_after from AO_54307E_SLAAUDITLOGDATA where VALUE like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_54307E_SLAAUDITLOGDATA set VALUE = replace(VALUE, '<OLD_VALUE>', '<NEW_VALUE>') where VALUE like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_9B2E3B_EXEC_RULE_MSG_ITEM.*,RULE_MESSAGE_VALUE as RULE_MESSAGE_VALUE_before,replace(RULE_MESSAGE_VALUE, '<OLD_VALUE>', '<NEW_VALUE>') as RULE_MESSAGE_VALUE_after from AO_9B2E3B_EXEC_RULE_MSG_ITEM where RULE_MESSAGE_VALUE like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_9B2E3B_EXEC_RULE_MSG_ITEM set RULE_MESSAGE_VALUE = replace(RULE_MESSAGE_VALUE, '<OLD_VALUE>', '<NEW_VALUE>') where RULE_MESSAGE_VALUE like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_9B2E3B_IF_COND_CONF_DATA.*,CONFIG_DATA_VALUE as CONFIG_DATA_VALUE_before,replace(CONFIG_DATA_VALUE, '<OLD_VALUE>', '<NEW_VALUE>') as CONFIG_DATA_VALUE_after from AO_9B2E3B_IF_COND_CONF_DATA where CONFIG_DATA_VALUE like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_9B2E3B_IF_COND_CONF_DATA set CONFIG_DATA_VALUE = replace(CONFIG_DATA_VALUE, '<OLD_VALUE>', '<NEW_VALUE>') where CONFIG_DATA_VALUE like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_9B2E3B_RULESET_REVISION.*,DESCRIPTION as DESCRIPTION_before,replace(DESCRIPTION, '<OLD_VALUE>', '<NEW_VALUE>') as DESCRIPTION_after from AO_9B2E3B_RULESET_REVISION where DESCRIPTION like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_9B2E3B_RULESET_REVISION set DESCRIPTION = replace(DESCRIPTION, '<OLD_VALUE>', '<NEW_VALUE>') where DESCRIPTION like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_9B2E3B_RULESET_REVISION.*,NAME as NAME_before,replace(NAME, '<OLD_VALUE>', '<NEW_VALUE>') as NAME_after from AO_9B2E3B_RULESET_REVISION where NAME like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_9B2E3B_RULESET_REVISION set NAME = replace(NAME, '<OLD_VALUE>', '<NEW_VALUE>') where NAME like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Automation
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_9B2E3B_THEN_ACT_CONF_DATA.*,CONFIG_DATA_VALUE as CONFIG_DATA_VALUE_before,replace(CONFIG_DATA_VALUE, '<OLD_VALUE>', '<NEW_VALUE>') as CONFIG_DATA_VALUE_after from AO_9B2E3B_THEN_ACT_CONF_DATA where CONFIG_DATA_VALUE like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_9B2E3B_THEN_ACT_CONF_DATA set CONFIG_DATA_VALUE = replace(CONFIG_DATA_VALUE, '<OLD_VALUE>', '<NEW_VALUE>') where CONFIG_DATA_VALUE like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Canned response audit trail
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_D530BB_CRAUDITACTIONDATA.*,FROM_STRING as FROM_STRING_before,replace(FROM_STRING, '<OLD_VALUE>', '<NEW_VALUE>') as FROM_STRING_after from AO_D530BB_CRAUDITACTIONDATA where FROM_STRING like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_D530BB_CRAUDITACTIONDATA set FROM_STRING = replace(FROM_STRING, '<OLD_VALUE>', '<NEW_VALUE>') where FROM_STRING like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Canned response audit trail
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_D530BB_CRAUDITACTIONDATA.*,FROM_TEXT as FROM_TEXT_before,replace(FROM_TEXT, '<OLD_VALUE>', '<NEW_VALUE>') as FROM_TEXT_after from AO_D530BB_CRAUDITACTIONDATA where FROM_TEXT like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_D530BB_CRAUDITACTIONDATA set FROM_TEXT = replace(FROM_TEXT, '<OLD_VALUE>', '<NEW_VALUE>') where FROM_TEXT like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Canned response audit trail
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_D530BB_CRAUDITACTIONDATA.*,TO_STRING as TO_STRING_before,replace(TO_STRING, '<OLD_VALUE>', '<NEW_VALUE>') as TO_STRING_after from AO_D530BB_CRAUDITACTIONDATA where TO_STRING like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_D530BB_CRAUDITACTIONDATA set TO_STRING = replace(TO_STRING, '<OLD_VALUE>', '<NEW_VALUE>') where TO_STRING like '%<OLD_VALUE>%' ;

-- Type        : update
-- Origin      : Jira ServiceDesk
-- Description : Canned response audit trail
-- Table valid only for specific product : Jira ServiceDesk
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_D530BB_CRAUDITACTIONDATA.*,TO_TEXT as TO_TEXT_before,replace(TO_TEXT, '<OLD_VALUE>', '<NEW_VALUE>') as TO_TEXT_after from AO_D530BB_CRAUDITACTIONDATA where TO_TEXT like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_D530BB_CRAUDITACTIONDATA set TO_TEXT = replace(TO_TEXT, '<OLD_VALUE>', '<NEW_VALUE>') where TO_TEXT like '%<OLD_VALUE>%' ;

