#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
__author__      = "Tomasz Dudzisz <tdudzisz@atlassian.com>"
__copyright__   = "Copyright 2018, Atlassian.com"
__credits__     = []
__license__     = "BSD"
__version__     = "1.0.1"
__status__      = "Development"
__created__     = 14/04/2018
__updated__     = 14/04/2018

__project__     = gdpr
__file__        = generator
__description__ = jira gdpr workaround status generator (as confluence table)
                  see: https://extranet.atlassian.com/display/SERVER/JIRA+Server+GDPR+Databases

"""


APP_KEY = "servicedesk"


import json, traceback, sys, os

PATH = "/Repo/gdpr"
METADATA = "{0}/metadata".format(PATH)
FILENAME = "{0}/{1}_db.json".format(METADATA, APP_KEY)


class LinkGenerator(object):

    url = "https://bitbucket.org/atlassian/gdpr/src/master"
    select = "select"
    update = "update"

    def __init__(self, key, field, ftype):
        self.table_link = """<a href="{0}/{1}_tables/{2}.table">{2}</a>""".format(LinkGenerator.url, APP_KEY, key)
        self.url = "{0}/{1}_db_queries".format(LinkGenerator.url, APP_KEY)

        self.dbpath = "{0}_db_queries".format(APP_KEY)

        self.field = field
        self.ftype = ftype
        self.key = key

        self.userkey = self._get_userkey()

        self.update_enabled = False


    def _get_userkey(self):
        if self.ftype == "user_key":
            return "_userkey"
        return ""


    def get_select(self, dbname):
        filename = self._get_filename(dbname, LinkGenerator.select)
        return self._validate_link(dbname, LinkGenerator.select, filename)


    def get_update(self, dbname):
        filename = self._get_filename(dbname, LinkGenerator.update)
        return self._validate_link(dbname, LinkGenerator.update, filename)


    def _get_filename(self, dbname, operation):
        query = "{}/{}_{}_{}{}.sql".format(dbname, operation, self.key, self.field, self.userkey)
        return query


    def _validate_link(self, dbname, operation, filename):

        path = "{}/{}/{}".format(PATH, self.dbpath, filename)

        if os.path.isfile(path):

            link = '<a href="{}/{}" class="link-{}">{}</a>'.format(self.url, filename, operation, dbname)
            return link

        else:
            entry = '<span>{}</span>'.format(dbname)
            return entry


class Generator(object):

    def __init__(self, file):
        self.file = file


    def get_doc_header(self):
        doc = """
        <html>
            <head></head>
            <style>
                body {
        margin: 0;
        padding: 20px;
        font-size: 14px;
        font-family: -apple-system,BlinkMacSystemFont,"Segoe UI","Roboto","Oxygen","Ubuntu","Fira Sans","Droid Sans","Helvetica Neue",sans-serif;
    }

    td {
        border: 1px dotted #ccc;
        padding: 5px 10px;
    }

    th {
        background: #eee;
        padding: 10px;
    }

    a {
        text-decoration: none;
        color: #0077ab;
    }
            </style>
        <body>
        """
        return doc


    def get_doc_footer(self):
        footer = """
        </body>
        </html>
        """
        return footer


    def get_table_header(self):
        header = """
    <table class="relative-table" style="font-size: 13.0px;letter-spacing: 0.0px; width: 100.0%;">
      <tbody>
        <tr>
          <th>Table Name</th>
          <th>Origin</th>
          <th style="min-width: 100px !important">Field</th>
          <th style="min-width: 50px !important">Type</th>
          <th style="min-width: 50px !important">Update via</th>
          <th style="min-width: 300px !important">SQL</th>
          <th>Notes</th>
        </tr>
        """
        return header


    def get_table_footer(self):

        return "</tbody></table>"


    def get_table_row(self, key, column_filter):

        result = []

        for field, fieldvalue in data[key]['where'].items():

            if not column_filter(fieldvalue):
                continue

            tr = self._get_table_row(field, fieldvalue, data, key)
            result.append(tr)

        return result


    def _get_table_row(self, field_html, fieldvalue, data, key):

        origin = self._get_origin(data, key)
        via = self.get_via(data[key])

        ftype = fieldvalue['type']
        link = LinkGenerator(key, field_html, ftype)

        field_html, sql_html = self._get_sql_cell(field_html, data[key], link)

        steps = self._get_steps(data[key])

        tr = """
            <tr>
              <td>{0}</td>
              <td>{1}</td>
              <td>{2}</td>
              <td>{3}</td>
              <td>{4}</td>
              <td>{5}</td>
              <td>{6}</td>
            </tr>
        """.format(link.table_link, origin, field_html, ftype, via, sql_html, steps)

        return tr


    def _get_steps(self, entry):

        result = []

        if "action" in entry:
            for action in entry["action"]:
                if action["type"] == "update":
                    if "steps" in action:
                        for step in action["steps"]:
                            result.append("<li>{}</li>".format(step))

        if result:
            steps = "<ol>{}</ol>".format("".join(result))
            section = """
                <div class="content-wrapper">
                <ac:structured-macro ac:macro-id="ca60d51e-9a13-4f82-b45a-dfc3fb386050" ac:name="expand" ac:schema-version="1">
                <ac:parameter ac:name="title">Open</ac:parameter>
                <ac:rich-text-body>{}</ac:rich-text-body>
                </ac:structured-macro>
                </div>""".format(steps)
            return section

        return ""


    def _get_label(self, color, text, subtle = True):

        subtle = "{}".format(subtle).lower()

        label = """
              <ac:structured-macro ac:macro-id="f21cab29-bcfc-48e9-922c-95caff986c50" ac:name="status" ac:schema-version="1">
                <ac:parameter ac:name="subtle">{0}</ac:parameter>
                <ac:parameter ac:name="colour">{1}</ac:parameter>
                <ac:parameter ac:name="title">{2}</ac:parameter>
              </ac:structured-macro>
              """.format(subtle, color, text)
        return label


    def _get_sql_cell(self, field, entry, link):

        field = "<strong>{}</strong>".format(field)

        selects = self._get_selects(link)
        updates = self._get_updates(link)

        # if not self.is_manual(entry):
        #     updates = self._get_updates(link)
        # else:
        #     updates = ""

        sqls = """
        <div class="content-wrapper">
            <ac:structured-macro ac:macro-id="ca60d51e-9a13-4f82-b45a-dfc3fb386050" ac:name="expand" ac:schema-version="1">
            <ac:parameter ac:name="title">Open</ac:parameter>
            <ac:rich-text-body>
                {0}
                {1}
            </ac:rich-text-body>
            </ac:structured-macro>
        </div>""".format(selects, updates)

        return field, sqls


    def _get_updates(self, link):

        label_red = self._get_label('Red', 'update')

        update_oracle_link = link.get_update('oracle')
        update_mysql_link = link.get_update('mysql')
        update_postgresql_link = link.get_update('postgresql')
        update_mssql_link = link.get_update('mssql')

        updates = """
        <p style="color: #aaa;">
            {0}
            {1} &bull;
            {2} &bull;
            {3} &bull;
            {4}
            </p>""".format(label_red, update_oracle_link, update_mysql_link, update_postgresql_link, update_mssql_link)
        return updates


    def _get_selects(self, link):

        label_green = self._get_label('Green', 'select')

        select_oracle_link = link.get_select('oracle')
        select_mysql_link = link.get_select('mysql')
        select_postgresql_link = link.get_select('postgresql')
        select_mssql_link = link.get_select('mssql')

        selects = """
        <p class="auto-cursor-target" style="color: #aaa;">
            {0}
            {1} &bull;
            {2} &bull;
            {3} &bull;
            {4}
          </p>""".format(label_green, select_oracle_link, select_mysql_link, select_postgresql_link, select_mssql_link)
        return selects


    def generate_document(self, data, column_filter):

        #print(self.get_doc_header())
        print(self.get_table_header())

        for key in data:

            row = self.get_table_row(key, column_filter)
            row = "\n".join(row)

            print(row)

        print(self.get_table_footer())

        #print(self.get_doc_footer())


    def get_data(self):
        as_json = json.load(open(self.file, 'r'))
        if as_json:
            return as_json

        raise Exception("Invalid JSON")


    def _get_origin(self, c, key):
        origin = c[key]['origin'].title()
        return origin


    # def is_manual(self, data):
    #     if "action" in data:
    #         for action in data["action"]:
    #             if action["type"] == "update":
    #                 if "manual" in action:
    #                     return action["manual"]
    #     return False


    def get_via(self, data):
        result = []

        if "url" in data:
            label = self._get_label('Green', 'User Interface', False)
            result.append(label)

        if "action" in data:
            for action in data["action"]:
                if action["type"] == "update":
                    if "api" in action:
                        api = '<a href="{}">REST API method</a>'.format(action["api"])
                        result.append(api)

        if result:
            return "<br/>".join(result)
        else:
            label = self._get_label('Red','SQL Update', False)
            return label


    # def get_fields(self, c, key):
    #     fields = []
    #     for field, fieldvalue in c[key]['where'].items():
    #         fields.append("<li><strong>{}</strong>: {}</li>".format(field, fieldvalue['type']))
    #     fields = "<ul>{}</ul>".format("".join(fields))
    #     return fields


if __name__ == '__main__':

    try:

        def column_filter_user_key(column_data):
            return column_data['type']=='user_key'

        def column_filter_not_user_key(column_data):
            return not column_filter_user_key(column_data)

        def column_filter_all(column_data):
            return True

        g = Generator(FILENAME)
        data = g.get_data()
        g.generate_document(data, column_filter_all)

    except Exception as e:

        print("ERROR: {}".format(e))
        traceback.print_stack()

        print(repr(traceback.extract_stack()))
        print(repr(traceback.format_stack()))

        sys.exit(1)
