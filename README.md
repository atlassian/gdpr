[TOC]

# Prerequisites

In order to run the script, you will need the following:

* Python 3
* DocOpt library
```bash
pip install docopt
```
* Schema Library
```bash
pip install schema
```

# Confluence README

## Metadata file 
##### Information about tables, fields and types:
```
metadata/confluence_db.json
```

## Scripts

##### To generate sql scripts:
```bash
python3 parser4confluence.py -u '<USERNAME>' -f metadata/confluence_db.json -d oracle
python3 parser4confluence.py -u '<USERNAME>' -f metadata/confluence_db.json -d mysql
python3 parser4confluence.py -u '<USERNAME>' -f metadata/confluence_db.json -d mssql
python3 parser4confluence.py -u '<USERNAME>' -f metadata/confluence_db.json -d postgresql
```

## SQL files
For each database it should create files named:
```bash
order_action_tablename.sql
```

For example:
```bash
01_delete_OS_PROPERTYENTRY.sql
02_delete_BODYCONTENT.sql
03_delete_CONTENTPROPERTIES.sql
04_delete_IMAGEDETAILS.sql
05_delete_CONTENT.sql
06_delete_NOTIFICATIONS.sql
07_delete_CONTENT.sql
08_delete_CONTENT.sql
09_delete_LIKES.sql
10_delete_CONTENT.sql
11_delete_cwd_membership.sql
12_delete_cwd_user_attribute.sql
13_delete_cwd_user.sql
14_insert_journalentry.sql
15_update_user_mapping.sql
```

# Jira Software README

## Metadata file 
##### Information about tables, fields and types:
```
metadata/jira_db.json
```

## SQL files

##### To generate SQL files:
```bash
python3 parser4jira.py -o '<CURRENT_PD_VALUE>' -n '<NEW_PD_VALUE>' -f metadata/jira_db.json -d {database}
```

##### Result:
```bash
jira_db_queries/{database}/*sql
```

##### To generate combined SQL files:
```bash
cd jira_db_queries
sh generate_sql_file.sh {database}
```

##### For each database it should create files named:
###### "select" only:
```bash
jira_db_queries/script_{database}_select.sql
jira_db_queries/script_{database}_select_userkey.sql
```
###### "select" + "update":
```bash
jira_db_queries/script_{database}_UPDATE.sql
jira_db_queries/script_{database}_UPDATE_userkey.sql
```

# Jira ServiceDesk README

## Metadata file 
##### Information about tables, fields and types:
```
metadata/servicedesk_db.json
```

## SQL files

##### To generate SQL files:
```bash
python3 parser4jira.py -o '<CURRENT_PD_VALUE>' -n '<NEW_PD_VALUE>' -f metadata/servicedesk_db.json -d {database}
```

##### Result:
```bash
servicedesk_db_queries/{database}/*sql
```

##### To generate combined SQL files:
```bash
cd servicedesk_db_queries
sh generate_sql_file.sh {database}
```

##### For each database it should create files named:
###### "select" only:
```bash
servicedesk_db_queries/script_{database}_select.sql
servicedesk_db_queries/script_{database}_select_userkey.sql
```
###### "select" + "update":
```bash
servicedesk_db_queries/script_{database}_UPDATE.sql
servicedesk_db_queries/script_{database}_UPDATE_userkey.sql
```

# Jira Portfolio README

## Metadata file 
##### Information about tables, fields and types:
```
metadata/portfolio_db.json
```

## SQL files

##### To generate SQL files:
```bash
python3 parser4jira.py -o '<CURRENT_PD_VALUE>' -n '<NEW_PD_VALUE>' -f metadata/portfolio_db.json -d {database}
```

##### Result:
```bash
portfolio_db_queries/{database}/*sql
```

##### To generate combined SQL files:
```bash
cd portfolio_db_queries
sh generate_sql_file.sh {database}
```

##### For each database it should create files named:
###### "select" only:
```bash
portfolio_db_queries/script_{database}_select.sql
portfolio_db_queries/script_{database}_select_userkey.sql
```
###### "select" + "update":
```bash
portfolio_db_queries/script_{database}_UPDATE.sql
portfolio_db_queries/script_{database}_UPDATE_userkey.sql
```


# Bamboo README

## Metadata file 
##### Information about tables, fields and types:
```
metadata/bamboo_db.json
```

## SQL files

##### To generate SQL files:
```bash
python3 parser4bamboo.py -u '<CURRENT_PD_VALUE>' -n '<NEW_PD_VALUE>' -f metadata/bamboo_db.json -d {database}
```

##### Result:
```bash
bamboo_db_queries/{database}/*sql
```

##### To generate combined SQL files:
```bash
cd bamboo_db_queries
sh generate_sql_file.sh {database}
```

##### For each database it should create files named:
###### "select" only:
```bash
bamboo_db_queries/script_{database}_select.sql
bamboo_db_queries/script_{database}_select_username.sql
```
###### "select" + "update":
```bash
bamboo_db_queries/script_{database}_update.sql
bamboo_db_queries/script_{database}_update_username.sql
```
