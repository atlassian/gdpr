-- Type       : insert
-- Origin     : Journal Entry
-- Description: Inserting Journal Entry rows to trigger incremental index
INSERT INTO journalentry
                              SELECT SEQ_JOURNAL_ENTRY_ID.nextval                                            AS entry_id
                                ,    'main_index'                                                            AS journal_name
                                ,    CURRENT_TIMESTAMP - 2                                                   AS creationdate
                                ,    'DELETE_DOCUMENT'                                                       AS type
                                ,    concat('com.atlassian.confluence.user.PersonalInformation-', CONTENTID) AS message
                              FROM (
                                SELECT CONTENTID
                                FROM CONTENT
                                WHERE CONTENTTYPE = 'USERINFO' AND CONTENT.USERNAME IN (
                                  SELECT user_key
                                  FROM user_mapping
                                  WHERE username = '__username__'));

INSERT INTO journalentry
                              SELECT SEQ_JOURNAL_ENTRY_ID.nextval                                            AS entry_id
                                ,    'main_index'                                                            AS journal_name
                                ,    CURRENT_TIMESTAMP - 2                                                   AS creationdate
                                ,    'DELETE_CHANGE_DOCUMENTS'                                               AS type
                                ,    concat('com.atlassian.confluence.user.PersonalInformation-', CONTENTID) AS message
                              FROM (
                                SELECT CONTENTID
                                FROM CONTENT
                                WHERE CONTENTTYPE = 'USERINFO' AND CONTENT.USERNAME IN (
                                  SELECT user_key
                                  FROM user_mapping
                                  WHERE username = '__username__'));

INSERT INTO journalentry          
                        SELECT SEQ_JOURNAL_ENTRY_ID.nextval AS entry_id
                        ,    'main_index'                 AS journal_name
                        ,    CURRENT_TIMESTAMP - 2        AS creationdate
                        ,    type_name                    AS type
                        ,    CASE
                            WHEN (CONTENTTYPE = 'PAGE')
                                THEN concat('com.atlassian.confluence.pages.Page-', CONTENTID)
                            WHEN (CONTENTTYPE = 'BLOGPOST')
                                THEN concat('com.atlassian.confluence.pages.BlogPost-', CONTENTID)
                            WHEN (CONTENTTYPE = 'COMMENT')
                                THEN concat('com.atlassian.confluence.pages.Comment-', CONTENTID)
                            END                          AS message
                        FROM (
                        SELECT CONTENT.CONTENTID
                            , CONTENT.CONTENTTYPE
                        FROM CONTENT
                            JOIN BODYCONTENT
                            ON CONTENT.CONTENTID = BODYCONTENT.CONTENTID
                        WHERE CONTENTTYPE IN ('PAGE', 'BLOGPOST', 'COMMENT')
                                AND CONTENT.PREVVER IS NULL
                                AND CONTENT.CONTENT_STATUS = 'current'
                                AND BODYCONTENT.BODY LIKE '%<ri:user ri:userkey="' || (
                            SELECT user_key
                            FROM user_mapping
                            WHERE user_mapping.username = '__username__') || '"%'
                        )
                        JOIN (SELECT 'ADD_CHANGE_DOCUMENT' type_name FROM dual
                                UNION
                                SELECT 'UPDATE_DOCUMENT' type_name FROM dual) tmp_type
                            ON (1 = 1);