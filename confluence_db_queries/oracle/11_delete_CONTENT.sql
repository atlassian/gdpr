-- Type       : delete
-- Origin     : User Personal Information
-- Description: Deleting User Personal Information
delete from CONTENT where CONTENTTYPE = 'USERINFO' and USERNAME IN (select user_key from user_mapping where username = '__username__' );