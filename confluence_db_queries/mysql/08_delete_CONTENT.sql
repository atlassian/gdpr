-- Type       : delete
-- Origin     : User Profile Info History
-- Description: Deleting User Profile Info History
delete from CONTENT where PREVVER IN (SELECT * FROM (select CONTENTID from CONTENT where CONTENTTYPE = 'USERINFO' and CONTENT.USERNAME IN (select user_key from user_mapping where username = '__username__' )) AS reference) ORDER BY VERSION ASC;