-- Type       : delete
-- Origin     : User Profile Info
-- Description: Deleting User Profile Info
delete from IMAGEDETAILS where ATTACHMENTID IN (select CONTENTID from CONTENT where PAGEID IN (select CONTENTID from CONTENT where CONTENTTYPE = 'USERINFO' and CONTENT.USERNAME IN (select user_key from user_mapping where username = '__username__' )));