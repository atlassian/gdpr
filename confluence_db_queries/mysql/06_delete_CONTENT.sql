-- Type       : delete
-- Origin     : User Profile Info
-- Description: Deleting User Profile Info
delete from CONTENT where PAGEID IN (SELECT * FROM (select CONTENTID from CONTENT where CONTENTTYPE = 'USERINFO' and CONTENT.USERNAME IN (select user_key from user_mapping where username = '__username__' )) AS reference) ORDER BY VERSION ASC;