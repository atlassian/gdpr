-- Type       : delete
-- Origin     : User Info
-- Description: Delete User Info
delete from OS_PROPERTYENTRY where entity_name = concat('USERPROPS-', (select user_key from user_mapping where username = '__username__' ));