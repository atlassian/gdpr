-- Type       : delete
-- Origin     : Likes
-- Description: Delete all likes by the user
delete from LIKES where USERNAME IN (select user_key from user_mapping where username = '__username__' );