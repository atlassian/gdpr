-- Type       : delete
-- Origin     : User Membership
-- Description: Deleting User Membership
delete from cwd_membership where child_user_id IN (select id from cwd_user where user_name = '__username__' );