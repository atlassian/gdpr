#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Queries Jira instance for projects with specified lead username.
Also allows updating projects from one lead username to another username.

Usage:
    project_lead.py -u <username> -p <password> -m <mode> --url <url> --old <oldlead> --new <newlead>

Options:
    -m  Mode that the script is working in. Allowed values: select, update.
    -old Old lead username
    -new New lead username
"""

"""
__author__      = "Tomasz Kanafa <tkanafa@atlassian.com>"
__copyright__   = "Copyright 2018, Atlassian.com"
__credits__     = []
__license__     = "BSD"
__version__     = "1.0.0"
__status__      = "Development"
__created__     = 13/04/2018
__updated__     = 13/04/2018

__project__     = gdpr-framework
__file__        = project_lead.py
__description__ = 

"""

import requests
import sys
import traceback
from docopt import docopt
from schema import Schema, And


class Opts(object):

    def __init__(self, doc):
        self.username = None
        self.password = None
        self.mode = None
        self.baseUrl = None
        self.leadFrom = None
        self.leadTo = None

        self.valid = False

        arguments = docopt(__doc__)
        self._validate(arguments)


    def _validate(self, arguments):
        schema = Schema({
            '-u': True,
            '<username>': And(str, len),
            '-p': True,
            '<password>': And(str, len),
            '-m': True,
            '<mode>': lambda x: x in ["select", "update"],
            '--url': True,
            '<url>': And(str, len),
            '--old': True,
            '<oldlead>': And(str, len),
            '--new': True,
            '<newlead>': And(str, len)
        })

        args = schema.validate(arguments)

        self.username = args['<username>']
        self.password = args['<password>']
        self.mode = args['<mode>']
        self.baseUrl = args['<url>']
        self.leadFrom = args['<oldlead>']
        self.leadTo = args['<newlead>']

        self.valid = self.username and self.password and self.mode and self.baseUrl and self.leadFrom and self.leadTo


class ProjectLead(object):

    def __init__(self):

        args = Opts(__doc__)

        if not args.valid:
            sys.exit(1)

        self.args = args

        self.auth = (self.args.username, self.args.password)

        self.projectsUrl = '{}/rest/api/2/project'.format(self.args.baseUrl)
        self.projectUpdateUrl = '{}/rest/api/2/project/{}'.format(self.args.baseUrl)


    def get_rest_data(self, url):
        data = requests.get(url, auth = self.auth)
        if data.status_code != 200:
            print("Invalid status code {}".format(data.status_code))
            sys.exit(1)


    def put_rest_data(self, url, data):
        updateData = requests.put(url, json = data, auth = self.auth)
        if updateData.status_code != 200:
            print("Invalid status code {}, {}".format(updateData.status_code, updateData.text))


    def mode_select(self, projectKeys):
        print("Projects with lead {}: {}".format(self.args.leadFrom, projectKeys))


    def mode_update(self, projectKeys):
        for projectKey in projectKeys:
            print("updating project {} with lead {}".format(projectKey, self.args.leadTo))

            json = {'lead': self.args.leadTo}
            self.put_rest_data(self.projectUpdateUrl.format(projectKey), json)


    def execute(self):
        print("fetching all projects with lead {}".format(self.args.leadFrom))

        projectsData = self.get_rest_data(self.projectsUrl)

        projectKeys = [i["key"] for i in projectsData.json() if "lead" in i and i["lead"]["name"] == self.args.leadFrom]

        if self.args.mode == 'select':
            self.mode_select(projectKeys)
            return

        if self.args.mode == 'update':
            self.mode_update(projectKeys)
            return

        print("Unknown mode {}".format(self.args.mode))
        sys.exit(1)


if __name__ == '__main__':

    try:

        project = ProjectLead()
        project.execute()

    except Exception as e:

        print("ERROR: {}".format(e))
        traceback.print_stack()

        print(repr(traceback.extract_stack()))
        print(repr(traceback.format_stack()))

        sys.exit(1)
