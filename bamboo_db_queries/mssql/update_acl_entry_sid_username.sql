-- Type        : update
-- Origin      : bamboo
-- Description : user permissions
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select acl_entry.*,sid as sid_before,'<NEW_VALUE>' as sid_after from acl_entry where (sid = '<OLD_VALUE>' ) AND type  = 'PRINCIPAL';

-- + UPDATE (be careful)
update acl_entry set sid = '<NEW_VALUE>' where (sid = '<OLD_VALUE>' ) AND type  = 'PRINCIPAL';

