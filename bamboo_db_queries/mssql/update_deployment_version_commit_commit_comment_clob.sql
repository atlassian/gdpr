-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version commit
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_version_commit.*,commit_comment_clob as commit_comment_clob_before,replace(commit_comment_clob, '<OLD_VALUE>', '<NEW_VALUE>') as commit_comment_clob_after from deployment_version_commit where commit_comment_clob like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_version_commit set commit_comment_clob = replace(commit_comment_clob, '<OLD_VALUE>', '<NEW_VALUE>') where commit_comment_clob like '%<OLD_VALUE>%' ;

