-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo shared credentials
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select credentials.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from credentials where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update credentials set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;

