-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo project
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select project.*,description as description_before,replace(description, '<OLD_VALUE>', '<NEW_VALUE>') as description_after from project where description like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update project set description = replace(description, '<OLD_VALUE>', '<NEW_VALUE>') where description like '%<OLD_VALUE>%' ;

