-- Type        : select
-- Origin      : bamboo
-- Description : Repository commits comments - imported from vcs repository
-- Database    : mssql

select user_commit.* from user_commit where commit_comment_clob like '%<OLD_VALUE>%' ;

