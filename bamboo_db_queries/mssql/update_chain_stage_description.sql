-- Type        : update
-- Origin      : bamboo
-- Description : Plan stage definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select chain_stage.*,description as description_before,replace(description, '<OLD_VALUE>', '<NEW_VALUE>') as description_after from chain_stage where description like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update chain_stage set description = replace(description, '<OLD_VALUE>', '<NEW_VALUE>') where description like '%<OLD_VALUE>%' ;

