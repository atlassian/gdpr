-- Type        : update
-- Origin      : bamboo
-- Description : reset password tokens requested by user
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select password_reset_token.*,username as username_before,'<NEW_VALUE>' as username_after from password_reset_token where username = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update password_reset_token set username = '<NEW_VALUE>' where username = '<OLD_VALUE>' ;

