-- Type        : update
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select quick_filter_rules.*,configuration as configuration_before,replace(configuration, '<OLD_VALUE>', '<NEW_VALUE>') as configuration_after from quick_filter_rules where configuration like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update quick_filter_rules set configuration = replace(configuration, '<OLD_VALUE>', '<NEW_VALUE>') where configuration like '%<OLD_VALUE>%' ;

