-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd synchronisation tokens
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_token.*,entity_name as entity_name_before,'<NEW_VALUE>' as entity_name_after from cwd_token where entity_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_token set entity_name = '<NEW_VALUE>' where entity_name = '<OLD_VALUE>' ;

