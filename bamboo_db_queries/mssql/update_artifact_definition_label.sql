-- Type        : update
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select artifact_definition.*,label as label_before,replace(label, '<OLD_VALUE>', '<NEW_VALUE>') as label_after from artifact_definition where label like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update artifact_definition set label = replace(label, '<OLD_VALUE>', '<NEW_VALUE>') where label like '%<OLD_VALUE>%' ;

