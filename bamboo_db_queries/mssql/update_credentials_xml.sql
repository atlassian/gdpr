-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo shared credentials
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select credentials.*,xml as xml_before,replace(xml, '<OLD_VALUE>', '<NEW_VALUE>') as xml_after from credentials where xml like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update credentials set xml = replace(xml, '<OLD_VALUE>', '<NEW_VALUE>') where xml like '%<OLD_VALUE>%' ;

