-- Type        : update
-- Origin      : bamboo
-- Description : Repository author
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select author.*,author_name as author_name_before,replace(author_name, '<OLD_VALUE>', '<NEW_VALUE>') as author_name_after from author where author_name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update author set author_name = replace(author_name, '<OLD_VALUE>', '<NEW_VALUE>') where author_name like '%<OLD_VALUE>%' ;

