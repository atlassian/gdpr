-- Type        : update
-- Origin      : bamboo
-- Description : Deployment variable substitution
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_variable_substitution.*,variable_key as variable_key_before,replace(variable_key, '<OLD_VALUE>', '<NEW_VALUE>') as variable_key_after from deployment_variable_substitution where variable_key like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_variable_substitution set variable_key = replace(variable_key, '<OLD_VALUE>', '<NEW_VALUE>') where variable_key like '%<OLD_VALUE>%' ;

