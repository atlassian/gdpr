-- Type        : update
-- Origin      : bamboo
-- Description : Deployment project definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_project.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from deployment_project where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_project set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;

