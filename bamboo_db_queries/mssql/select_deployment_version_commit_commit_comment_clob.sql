-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version commit
-- Database    : mssql

select deployment_version_commit.* from deployment_version_commit where commit_comment_clob like '%<OLD_VALUE>%' ;

