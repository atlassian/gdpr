-- Type        : update
-- Origin      : bamboo
-- Description : Elastic image data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select elastic_image.*,description as description_before,replace(description, '<OLD_VALUE>', '<NEW_VALUE>') as description_after from elastic_image where description like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update elastic_image set description = replace(description, '<OLD_VALUE>', '<NEW_VALUE>') where description like '%<OLD_VALUE>%' ;

