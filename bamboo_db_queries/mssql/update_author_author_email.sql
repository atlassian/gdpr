-- Type        : update
-- Origin      : bamboo
-- Description : Repository author
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select author.*,author_email as author_email_before,'<NEW_VALUE>' as author_email_after from author where author_email = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update author set author_email = '<NEW_VALUE>' where author_email = '<OLD_VALUE>' ;

