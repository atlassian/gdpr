-- Type        : update
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select variable_substitution.*,variable_value as variable_value_before,replace(variable_value, '<OLD_VALUE>', '<NEW_VALUE>') as variable_value_after from variable_substitution where variable_value like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update variable_substitution set variable_value = replace(variable_value, '<OLD_VALUE>', '<NEW_VALUE>') where variable_value like '%<OLD_VALUE>%' ;

