-- Type        : update
-- Origin      : bamboo
-- Description : Repository commits comments - imported from vcs repository
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select user_commit.*,commit_comment_clob as commit_comment_clob_before,replace(commit_comment_clob, '<OLD_VALUE>', '<NEW_VALUE>') as commit_comment_clob_after from user_commit where commit_comment_clob like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update user_commit set commit_comment_clob = replace(commit_comment_clob, '<OLD_VALUE>', '<NEW_VALUE>') where commit_comment_clob like '%<OLD_VALUE>%' ;

