-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment configuration
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_env_config.*,plugin_config as plugin_config_before,replace(plugin_config, '<OLD_VALUE>', '<NEW_VALUE>') as plugin_config_after from deployment_env_config where plugin_config like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_env_config set plugin_config = replace(plugin_config, '<OLD_VALUE>', '<NEW_VALUE>') where plugin_config like '%<OLD_VALUE>%' ;

