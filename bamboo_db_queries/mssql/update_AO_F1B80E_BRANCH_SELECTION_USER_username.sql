-- Type        : update
-- Origin      : bamboo plugin
-- Description : ???
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select AO_F1B80E_BRANCH_SELECTION.*,"USER" as USER_before,'<NEW_VALUE>' as USER_after from AO_F1B80E_BRANCH_SELECTION where "USER" = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_F1B80E_BRANCH_SELECTION set "USER" = '<NEW_VALUE>' where "USER" = '<OLD_VALUE>' ;

