-- Type        : select
-- Origin      : bamboo
-- Description : Directory pattern of result artifacts subscription
-- Database    : mssql

select brs_consumed_subscription.* from brs_consumed_subscription where dst_directory like '%<OLD_VALUE>%' ;

