-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version naming
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_version_naming.*,next_version_name as next_version_name_before,replace(next_version_name, '<OLD_VALUE>', '<NEW_VALUE>') as next_version_name_after from deployment_version_naming where next_version_name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_version_naming set next_version_name = replace(next_version_name, '<OLD_VALUE>', '<NEW_VALUE>') where next_version_name like '%<OLD_VALUE>%' ;

