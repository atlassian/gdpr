-- Type        : update
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select label.*,namespace as namespace_before,'<NEW_VALUE>' as namespace_after from label where namespace = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update label set namespace = '<NEW_VALUE>' where namespace = '<OLD_VALUE>' ;

