-- Type        : update
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select imserver.*,host as host_before,replace(host, '<OLD_VALUE>', '<NEW_VALUE>') as host_after from imserver where host like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update imserver set host = replace(host, '<OLD_VALUE>', '<NEW_VALUE>') where host like '%<OLD_VALUE>%' ;

