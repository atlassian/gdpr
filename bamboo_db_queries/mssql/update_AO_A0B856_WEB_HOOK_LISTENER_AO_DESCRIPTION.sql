-- Type        : update
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select AO_A0B856_WEB_HOOK_LISTENER_AO.*,DESCRIPTION as DESCRIPTION_before,replace(DESCRIPTION, '<OLD_VALUE>', '<NEW_VALUE>') as DESCRIPTION_after from AO_A0B856_WEB_HOOK_LISTENER_AO where DESCRIPTION like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update AO_A0B856_WEB_HOOK_LISTENER_AO set DESCRIPTION = replace(DESCRIPTION, '<OLD_VALUE>', '<NEW_VALUE>') where DESCRIPTION like '%<OLD_VALUE>%' ;

