-- Type        : select
-- Origin      : bamboo
-- Description : user permissions - ownership
-- Database    : mssql

select acl_object_identity.* from acl_object_identity where (owner_sid = '<OLD_VALUE>' ) AND owner_type  = 'PRINCIPAL';

