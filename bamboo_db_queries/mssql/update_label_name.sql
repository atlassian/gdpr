-- Type        : update
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select label.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from label where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update label set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;

