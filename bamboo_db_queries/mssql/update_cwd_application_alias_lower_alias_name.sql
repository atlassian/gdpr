-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_application_alias.*,lower_alias_name as lower_alias_name_before,replace(lower_alias_name, '<OLD_VALUE>', '<NEW_VALUE>') as lower_alias_name_after from cwd_application_alias where lower_alias_name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update cwd_application_alias set lower_alias_name = replace(lower_alias_name, '<OLD_VALUE>', '<NEW_VALUE>') where lower_alias_name like '%<OLD_VALUE>%' ;

