-- Type        : update
-- Origin      : bamboo
-- Description : Artefact data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select artifact.*,label as label_before,replace(label, '<OLD_VALUE>', '<NEW_VALUE>') as label_after from artifact where label like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update artifact set label = replace(label, '<OLD_VALUE>', '<NEW_VALUE>') where label like '%<OLD_VALUE>%' ;

