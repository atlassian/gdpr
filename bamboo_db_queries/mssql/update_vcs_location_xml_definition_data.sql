-- Type        : update
-- Origin      : bamboo
-- Description : Configuration of vcs repository
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select vcs_location.*,xml_definition_data as xml_definition_data_before,replace(xml_definition_data, '<OLD_VALUE>', '<NEW_VALUE>') as xml_definition_data_after from vcs_location where xml_definition_data like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update vcs_location set xml_definition_data = replace(xml_definition_data, '<OLD_VALUE>', '<NEW_VALUE>') where xml_definition_data like '%<OLD_VALUE>%' ;

