-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd directories
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_directory.*,description as description_before,replace(description, '<OLD_VALUE>', '<NEW_VALUE>') as description_after from cwd_directory where description like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update cwd_directory set description = replace(description, '<OLD_VALUE>', '<NEW_VALUE>') where description like '%<OLD_VALUE>%' ;

