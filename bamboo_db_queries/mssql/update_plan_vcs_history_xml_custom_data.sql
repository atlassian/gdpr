-- Type        : update
-- Origin      : bamboo
-- Description : Merge result
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select plan_vcs_history.*,xml_custom_data as xml_custom_data_before,replace(xml_custom_data, '<OLD_VALUE>', '<NEW_VALUE>') as xml_custom_data_after from plan_vcs_history where xml_custom_data like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update plan_vcs_history set xml_custom_data = replace(xml_custom_data, '<OLD_VALUE>', '<NEW_VALUE>') where xml_custom_data like '%<OLD_VALUE>%' ;

