-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_user.*,email_address as email_address_before,'<NEW_VALUE>' as email_address_after from cwd_user where email_address = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_user set email_address = '<NEW_VALUE>' where email_address = '<OLD_VALUE>' ;

