-- Type        : update
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_version.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from deployment_version where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update deployment_version set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;

