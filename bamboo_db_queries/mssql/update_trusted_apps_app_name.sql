-- Type        : update
-- Origin      : bamboo
-- Description : Name of the trusted app
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select trusted_apps.*,app_name as app_name_before,replace(app_name, '<OLD_VALUE>', '<NEW_VALUE>') as app_name_after from trusted_apps where app_name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update trusted_apps set app_name = replace(app_name, '<OLD_VALUE>', '<NEW_VALUE>') where app_name like '%<OLD_VALUE>%' ;

