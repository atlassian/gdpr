-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_user.*,lower_first_name as lower_first_name_before,'<NEW_VALUE>' as lower_first_name_after from cwd_user where lower_first_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_user set lower_first_name = '<NEW_VALUE>' where lower_first_name = '<OLD_VALUE>' ;

