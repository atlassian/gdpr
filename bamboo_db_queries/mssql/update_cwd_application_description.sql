-- Type        : update
-- Origin      : bamboo
-- Description : User groups mapping
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_application.*,description as description_before,replace(description, '<OLD_VALUE>', '<NEW_VALUE>') as description_after from cwd_application where description like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update cwd_application set description = replace(description, '<OLD_VALUE>', '<NEW_VALUE>') where description like '%<OLD_VALUE>%' ;

