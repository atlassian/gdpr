-- Type        : update
-- Origin      : bamboo
-- Description : Job requirements
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select requirement.*,match_value as match_value_before,replace(match_value, '<OLD_VALUE>', '<NEW_VALUE>') as match_value_after from requirement where match_value like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update requirement set match_value = replace(match_value, '<OLD_VALUE>', '<NEW_VALUE>') where match_value like '%<OLD_VALUE>%' ;

