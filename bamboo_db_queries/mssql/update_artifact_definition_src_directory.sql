-- Type        : update
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select artifact_definition.*,src_directory as src_directory_before,replace(src_directory, '<OLD_VALUE>', '<NEW_VALUE>') as src_directory_after from artifact_definition where src_directory like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update artifact_definition set src_directory = replace(src_directory, '<OLD_VALUE>', '<NEW_VALUE>') where src_directory like '%<OLD_VALUE>%' ;

