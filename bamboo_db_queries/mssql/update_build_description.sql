-- Type        : update
-- Origin      : bamboo
-- Description : Plan / Job data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select build.*,description as description_before,replace(description, '<OLD_VALUE>', '<NEW_VALUE>') as description_after from build where description like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update build set description = replace(description, '<OLD_VALUE>', '<NEW_VALUE>') where description like '%<OLD_VALUE>%' ;

