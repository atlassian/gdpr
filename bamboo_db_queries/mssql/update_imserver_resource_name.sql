-- Type        : update
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select imserver.*,resource_name as resource_name_before,replace(resource_name, '<OLD_VALUE>', '<NEW_VALUE>') as resource_name_after from imserver where resource_name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update imserver set resource_name = replace(resource_name, '<OLD_VALUE>', '<NEW_VALUE>') where resource_name like '%<OLD_VALUE>%' ;

