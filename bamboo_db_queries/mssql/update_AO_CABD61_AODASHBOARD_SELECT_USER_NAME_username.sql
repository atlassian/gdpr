-- Type        : update
-- Origin      : bamboo plugin
-- Description : ???
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select AO_CABD61_AODASHBOARD_SELECT.*,USER_NAME as USER_NAME_before,'<NEW_VALUE>' as USER_NAME_after from AO_CABD61_AODASHBOARD_SELECT where USER_NAME = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_CABD61_AODASHBOARD_SELECT set USER_NAME = '<NEW_VALUE>' where USER_NAME = '<OLD_VALUE>' ;

