-- Type        : update
-- Origin      : bamboo
-- Description : creator of result label
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select buildresultsummary_label.*,user_name as user_name_before,'<NEW_VALUE>' as user_name_after from buildresultsummary_label where user_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update buildresultsummary_label set user_name = '<NEW_VALUE>' where user_name = '<OLD_VALUE>' ;

