-- Type        : update
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select buildresultsummary_customdata.*,custom_info_value as custom_info_value_before,replace(custom_info_value, '<OLD_VALUE>', '<NEW_VALUE>') as custom_info_value_after from buildresultsummary_customdata where custom_info_value like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update buildresultsummary_customdata set custom_info_value = replace(custom_info_value, '<OLD_VALUE>', '<NEW_VALUE>') where custom_info_value like '%<OLD_VALUE>%' ;

