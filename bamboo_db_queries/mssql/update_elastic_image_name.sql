-- Type        : update
-- Origin      : bamboo
-- Description : Elastic image data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select elastic_image.*,name as name_before,replace(name, '<OLD_VALUE>', '<NEW_VALUE>') as name_after from elastic_image where name like '%<OLD_VALUE>%' ;

-- + UPDATE (be careful)
update elastic_image set name = replace(name, '<OLD_VALUE>', '<NEW_VALUE>') where name like '%<OLD_VALUE>%' ;

