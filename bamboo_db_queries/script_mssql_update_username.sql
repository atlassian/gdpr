-- Type        : update
-- Origin      : support healthcheck plugin
-- Description : record of dismissed messages
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select AO_2F1435_READ_NOTIFICATIONS.*,USER_KEY as USER_KEY_before,'<NEW_VALUE>' as USER_KEY_after from AO_2F1435_READ_NOTIFICATIONS where USER_KEY = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_2F1435_READ_NOTIFICATIONS set USER_KEY = '<NEW_VALUE>' where USER_KEY = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bundled Bamboo plugin
-- Description : no description
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select AO_7A45FB_AOTRACKING_USER.*,USERNAME as USERNAME_before,'<NEW_VALUE>' as USERNAME_after from AO_7A45FB_AOTRACKING_USER where USERNAME = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_7A45FB_AOTRACKING_USER set USERNAME = '<NEW_VALUE>' where USERNAME = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bundled Bamboo plugin
-- Description : no description
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select AO_7A45FB_AOTRACKING_USER.*,USER_WHO_UPDATED as USER_WHO_UPDATED_before,'<NEW_VALUE>' as USER_WHO_UPDATED_after from AO_7A45FB_AOTRACKING_USER where USER_WHO_UPDATED = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_7A45FB_AOTRACKING_USER set USER_WHO_UPDATED = '<NEW_VALUE>' where USER_WHO_UPDATED = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo plugin
-- Description : ???
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select AO_C7F71E_OAUTH_SVC_PROV_TKNS.*,USER_NAME as USER_NAME_before,'<NEW_VALUE>' as USER_NAME_after from AO_C7F71E_OAUTH_SVC_PROV_TKNS where USER_NAME = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_C7F71E_OAUTH_SVC_PROV_TKNS set USER_NAME = '<NEW_VALUE>' where USER_NAME = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo plugin
-- Description : ???
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select AO_CABD61_AODASHBOARD_SELECT.*,USER_NAME as USER_NAME_before,'<NEW_VALUE>' as USER_NAME_after from AO_CABD61_AODASHBOARD_SELECT where USER_NAME = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_CABD61_AODASHBOARD_SELECT set USER_NAME = '<NEW_VALUE>' where USER_NAME = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo plugin
-- Description : ???
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select AO_F1B80E_BRANCH_SELECTION.*,"USER" as USER_before,'<NEW_VALUE>' as USER_after from AO_F1B80E_BRANCH_SELECTION where "USER" = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_F1B80E_BRANCH_SELECTION set "USER" = '<NEW_VALUE>' where "USER" = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : user permissions
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select acl_entry.*,sid as sid_before,'<NEW_VALUE>' as sid_after from acl_entry where (sid = '<OLD_VALUE>' ) AND type  = 'PRINCIPAL';

-- + UPDATE (be careful)
update acl_entry set sid = '<NEW_VALUE>' where (sid = '<OLD_VALUE>' ) AND type  = 'PRINCIPAL';

-- Type        : update
-- Origin      : bamboo
-- Description : user permissions - ownership
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select acl_object_identity.*,owner_sid as owner_sid_before,'<NEW_VALUE>' as owner_sid_after from acl_object_identity where (owner_sid = '<OLD_VALUE>' ) AND owner_type  = 'PRINCIPAL';

-- + UPDATE (be careful)
update acl_object_identity set owner_sid = '<NEW_VALUE>' where (owner_sid = '<OLD_VALUE>' ) AND owner_type  = 'PRINCIPAL';

-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select audit_log.*,user_name as user_name_before,'<NEW_VALUE>' as user_name_after from audit_log where user_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update audit_log set user_name = '<NEW_VALUE>' where user_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : last user login, number of failed logins per user
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select auth_attempt_info.*,user_name as user_name_before,'<NEW_VALUE>' as user_name_after from auth_attempt_info where user_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update auth_attempt_info set user_name = '<NEW_VALUE>' where user_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Repository author
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select author.*,linked_user_name as linked_user_name_before,'<NEW_VALUE>' as linked_user_name_after from author where linked_user_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update author set linked_user_name = '<NEW_VALUE>' where linked_user_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : creator of result label
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select buildresultsummary_label.*,user_name as user_name_before,'<NEW_VALUE>' as user_name_after from buildresultsummary_label where user_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update buildresultsummary_label set user_name = '<NEW_VALUE>' where user_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Default group name
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_app_dir_default_groups.*,group_name as group_name_before,'<NEW_VALUE>' as group_name_after from cwd_app_dir_default_groups where group_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_app_dir_default_groups set group_name = '<NEW_VALUE>' where group_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : User groups mapping
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_app_dir_group_mapping.*,group_name as group_name_before,'<NEW_VALUE>' as group_name_after from cwd_app_dir_group_mapping where group_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_app_dir_group_mapping set group_name = '<NEW_VALUE>' where group_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_application_alias.*,lower_user_name as lower_user_name_before,'<NEW_VALUE>' as lower_user_name_after from cwd_application_alias where lower_user_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_application_alias set lower_user_name = '<NEW_VALUE>' where lower_user_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_application_alias.*,user_name as user_name_before,'<NEW_VALUE>' as user_name_after from cwd_application_alias where user_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_application_alias set user_name = '<NEW_VALUE>' where user_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd user tokens
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_expirable_user_token.*,user_name as user_name_before,'<NEW_VALUE>' as user_name_after from cwd_expirable_user_token where user_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_expirable_user_token set user_name = '<NEW_VALUE>' where user_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd granted permissions
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_granted_perm.*,group_name as group_name_before,'<NEW_VALUE>' as group_name_after from cwd_granted_perm where group_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_granted_perm set group_name = '<NEW_VALUE>' where group_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd groups
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_group.*,group_name as group_name_before,'<NEW_VALUE>' as group_name_after from cwd_group where group_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_group set group_name = '<NEW_VALUE>' where group_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd groups
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_group.*,lower_group_name as lower_group_name_before,'<NEW_VALUE>' as lower_group_name_after from cwd_group where lower_group_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_group set lower_group_name = '<NEW_VALUE>' where lower_group_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd user groups membership
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_membership.*,child_name as child_name_before,'<NEW_VALUE>' as child_name_after from cwd_membership where child_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_membership set child_name = '<NEW_VALUE>' where child_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd user groups membership
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_membership.*,lower_child_name as lower_child_name_before,'<NEW_VALUE>' as lower_child_name_after from cwd_membership where lower_child_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_membership set lower_child_name = '<NEW_VALUE>' where lower_child_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd user groups membership
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_membership.*,lower_parent_name as lower_parent_name_before,'<NEW_VALUE>' as lower_parent_name_after from cwd_membership where lower_parent_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_membership set lower_parent_name = '<NEW_VALUE>' where lower_parent_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd user groups membership
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_membership.*,parent_name as parent_name_before,'<NEW_VALUE>' as parent_name_after from cwd_membership where parent_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_membership set parent_name = '<NEW_VALUE>' where parent_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd synchronisation tokens
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_token.*,entity_name as entity_name_before,'<NEW_VALUE>' as entity_name_after from cwd_token where entity_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_token set entity_name = '<NEW_VALUE>' where entity_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd tombstone
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_tombstone.*,entity_name as entity_name_before,'<NEW_VALUE>' as entity_name_after from cwd_tombstone where entity_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_tombstone set entity_name = '<NEW_VALUE>' where entity_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd tombstone
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_tombstone.*,parent as parent_before,'<NEW_VALUE>' as parent_after from cwd_tombstone where parent = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_tombstone set parent = '<NEW_VALUE>' where parent = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_user.*,display_name as display_name_before,'<NEW_VALUE>' as display_name_after from cwd_user where display_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_user set display_name = '<NEW_VALUE>' where display_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_user.*,first_name as first_name_before,'<NEW_VALUE>' as first_name_after from cwd_user where first_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_user set first_name = '<NEW_VALUE>' where first_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_user.*,last_name as last_name_before,'<NEW_VALUE>' as last_name_after from cwd_user where last_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_user set last_name = '<NEW_VALUE>' where last_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_user.*,lower_display_name as lower_display_name_before,'<NEW_VALUE>' as lower_display_name_after from cwd_user where lower_display_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_user set lower_display_name = '<NEW_VALUE>' where lower_display_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_user.*,lower_first_name as lower_first_name_before,'<NEW_VALUE>' as lower_first_name_after from cwd_user where lower_first_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_user set lower_first_name = '<NEW_VALUE>' where lower_first_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_user.*,lower_last_name as lower_last_name_before,'<NEW_VALUE>' as lower_last_name_after from cwd_user where lower_last_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_user set lower_last_name = '<NEW_VALUE>' where lower_last_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_user.*,lower_user_name as lower_user_name_before,'<NEW_VALUE>' as lower_user_name_after from cwd_user where lower_user_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_user set lower_user_name = '<NEW_VALUE>' where lower_user_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select cwd_user.*,user_name as user_name_before,'<NEW_VALUE>' as user_name_after from cwd_user where user_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update cwd_user set user_name = '<NEW_VALUE>' where user_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_version.*,creator_username as creator_username_before,'<NEW_VALUE>' as creator_username_after from deployment_version where creator_username = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update deployment_version set creator_username = '<NEW_VALUE>' where creator_username = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : deployment version approver
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select deployment_version_status.*,user_name as user_name_before,'<NEW_VALUE>' as user_name_after from deployment_version_status where user_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update deployment_version_status set user_name = '<NEW_VALUE>' where user_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : External users name
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select external_entities.*,name as name_before,'<NEW_VALUE>' as name_after from external_entities where name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update external_entities set name = '<NEW_VALUE>' where name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select imserver.*,username as username_before,'<NEW_VALUE>' as username_after from imserver where username = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update imserver set username = '<NEW_VALUE>' where username = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select label.*,namespace as namespace_before,'<NEW_VALUE>' as namespace_after from label where namespace = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update label set namespace = '<NEW_VALUE>' where namespace = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo notifications send to user or other recipients
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select notifications.*,recipient as recipient_before,'<NEW_VALUE>' as recipient_after from notifications where (recipient = '<OLD_VALUE>' ) AND recipient_type  = 'com.atlassian.bamboo.plugin.system.notifications:recipient.user';

-- + UPDATE (be careful)
update notifications set recipient = '<NEW_VALUE>' where (recipient = '<OLD_VALUE>' ) AND recipient_type  = 'com.atlassian.bamboo.plugin.system.notifications:recipient.user';

-- Type        : update
-- Origin      : bamboo
-- Description : reset password tokens requested by user
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select password_reset_token.*,username as username_before,'<NEW_VALUE>' as username_after from password_reset_token where username = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update password_reset_token set username = '<NEW_VALUE>' where username = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : used to auto-login user based on cookie
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select rememberme_token.*,username as username_before,'<NEW_VALUE>' as username_after from rememberme_token where username = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update rememberme_token set username = '<NEW_VALUE>' where username = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : user that has quarantined test case
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select test_case.*,quarantining_username as quarantining_username_before,'<NEW_VALUE>' as quarantining_username_after from test_case where quarantining_username = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update test_case set quarantining_username = '<NEW_VALUE>' where quarantining_username = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : author of user comments
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select user_comment.*,user_name as user_name_before,'<NEW_VALUE>' as user_name_after from user_comment where user_name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update user_comment set user_name = '<NEW_VALUE>' where user_name = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo internal users directory
-- Database    : mssql

 
-- + SELECT (please review changes BEFORE)
select users.*,name as name_before,'<NEW_VALUE>' as name_after from users where name = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update users set name = '<NEW_VALUE>' where name = '<OLD_VALUE>' ;

