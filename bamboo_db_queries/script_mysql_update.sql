-- Type        : update
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select AO_A0B856_WEB_HOOK_LISTENER_AO.*,DESCRIPTION as DESCRIPTION_before,replace(DESCRIPTION,'<OLD_VALUE>','<NEW_VALUE>') as DESCRIPTION_after from AO_A0B856_WEB_HOOK_LISTENER_AO where LOWER(DESCRIPTION) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update AO_A0B856_WEB_HOOK_LISTENER_AO set DESCRIPTION = replace(DESCRIPTION,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(DESCRIPTION) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select AO_A0B856_WEB_HOOK_LISTENER_AO.*,NAME as NAME_before,replace(NAME,'<OLD_VALUE>','<NEW_VALUE>') as NAME_after from AO_A0B856_WEB_HOOK_LISTENER_AO where LOWER(NAME) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update AO_A0B856_WEB_HOOK_LISTENER_AO set NAME = replace(NAME,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(NAME) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select AO_A0B856_WEB_HOOK_LISTENER_AO.*,URL as URL_before,'<NEW_VALUE>' as URL_after from AO_A0B856_WEB_HOOK_LISTENER_AO where LOWER(URL) like LOWER('%<OLD_VALUE>%') ;

-- + UPDATE (be careful)
update AO_A0B856_WEB_HOOK_LISTENER_AO set URL = '<NEW_VALUE>' where LOWER(URL) like LOWER('%<OLD_VALUE>%') ;

-- Type        : update
-- Origin      : bamboo plugin
-- Description : ???
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select AO_C7F71E_OAUTH_SVC_PROV_TKNS.*,VALUE as VALUE_before,replace(VALUE,'<OLD_VALUE>','<NEW_VALUE>') as VALUE_after from AO_C7F71E_OAUTH_SVC_PROV_TKNS where LOWER(VALUE) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update AO_C7F71E_OAUTH_SVC_PROV_TKNS set VALUE = replace(VALUE,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(VALUE) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select artifact_definition.*,copy_pattern as copy_pattern_before,replace(copy_pattern,'<OLD_VALUE>','<NEW_VALUE>') as copy_pattern_after from artifact_definition where LOWER(copy_pattern) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update artifact_definition set copy_pattern = replace(copy_pattern,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(copy_pattern) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select artifact_definition.*,label as label_before,replace(label,'<OLD_VALUE>','<NEW_VALUE>') as label_after from artifact_definition where LOWER(label) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update artifact_definition set label = replace(label,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(label) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select artifact_definition.*,src_directory as src_directory_before,replace(src_directory,'<OLD_VALUE>','<NEW_VALUE>') as src_directory_after from artifact_definition where LOWER(src_directory) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update artifact_definition set src_directory = replace(src_directory,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(src_directory) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Artefact data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select artifact.*,label as label_before,replace(label,'<OLD_VALUE>','<NEW_VALUE>') as label_after from artifact where LOWER(label) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update artifact set label = replace(label,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(label) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Artifact subscription
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select artifact_subscription.*,dst_directory as dst_directory_before,replace(dst_directory,'<OLD_VALUE>','<NEW_VALUE>') as dst_directory_after from artifact_subscription where LOWER(dst_directory) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update artifact_subscription set dst_directory = replace(dst_directory,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(dst_directory) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select audit_log.*,msg as msg_before,replace(msg,'<OLD_VALUE>','<NEW_VALUE>') as msg_after from audit_log where LOWER(msg) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update audit_log set msg = replace(msg,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(msg) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select audit_log.*,new_value as new_value_before,replace(new_value,'<OLD_VALUE>','<NEW_VALUE>') as new_value_after from audit_log where LOWER(new_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update audit_log set new_value = replace(new_value,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(new_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select audit_log.*,old_value as old_value_before,replace(old_value,'<OLD_VALUE>','<NEW_VALUE>') as old_value_after from audit_log where LOWER(old_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update audit_log set old_value = replace(old_value,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(old_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select audit_log.*,task_header as task_header_before,replace(task_header,'<OLD_VALUE>','<NEW_VALUE>') as task_header_after from audit_log where LOWER(task_header) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update audit_log set task_header = replace(task_header,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(task_header) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Repository author
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select author.*,author_email as author_email_before,'<NEW_VALUE>' as author_email_after from author where LOWER(author_email) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update author set author_email = '<NEW_VALUE>' where LOWER(author_email) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : bamboo
-- Description : Repository author
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select author.*,author_name as author_name_before,replace(author_name,'<OLD_VALUE>','<NEW_VALUE>') as author_name_after from author where LOWER(author_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update author set author_name = replace(author_name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(author_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : set of various values used by system; may contain username
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select bandana.*,serialized_data as serialized_data_before,replace(serialized_data,'<OLD_VALUE>','<NEW_VALUE>') as serialized_data_after from bandana where LOWER(serialized_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update bandana set serialized_data = replace(serialized_data,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(serialized_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Directory pattern of result artifacts subscription
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select brs_consumed_subscription.*,dst_directory as dst_directory_before,replace(dst_directory,'<OLD_VALUE>','<NEW_VALUE>') as dst_directory_after from brs_consumed_subscription where LOWER(dst_directory) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update brs_consumed_subscription set dst_directory = replace(dst_directory,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(dst_directory) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Job configuration
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select build_definition.*,xml_definition_data as xml_definition_data_before,replace(xml_definition_data,'<OLD_VALUE>','<NEW_VALUE>') as xml_definition_data_after from build_definition where LOWER(xml_definition_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update build_definition set xml_definition_data = replace(xml_definition_data,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(xml_definition_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Plan / Job data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select build.*,description as description_before,replace(description,'<OLD_VALUE>','<NEW_VALUE>') as description_after from build where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update build set description = replace(description,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Plan / Job data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select build.*,title as title_before,replace(title,'<OLD_VALUE>','<NEW_VALUE>') as title_after from build where LOWER(title) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update build set title = replace(title,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(title) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select buildresultsummary_customdata.*,custom_info_value as custom_info_value_before,replace(custom_info_value,'<OLD_VALUE>','<NEW_VALUE>') as custom_info_value_after from buildresultsummary_customdata where LOWER(custom_info_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update buildresultsummary_customdata set custom_info_value = replace(custom_info_value,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(custom_info_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Agent capabilities
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select capability.*,value as value_before,replace(value,'<OLD_VALUE>','<NEW_VALUE>') as value_after from capability where LOWER(value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update capability set value = replace(value,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Plan stage definition
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select chain_stage.*,description as description_before,replace(description,'<OLD_VALUE>','<NEW_VALUE>') as description_after from chain_stage where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update chain_stage set description = replace(description,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Plan stage definition
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select chain_stage.*,name as name_before,replace(name,'<OLD_VALUE>','<NEW_VALUE>') as name_after from chain_stage where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update chain_stage set name = replace(name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select chain_stage_result.*,description as description_before,replace(description,'<OLD_VALUE>','<NEW_VALUE>') as description_after from chain_stage_result where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update chain_stage_result set description = replace(description,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select chain_stage_result.*,name as name_before,replace(name,'<OLD_VALUE>','<NEW_VALUE>') as name_after from chain_stage_result where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update chain_stage_result set name = replace(name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo shared credentials
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select credentials.*,name as name_before,replace(name,'<OLD_VALUE>','<NEW_VALUE>') as name_after from credentials where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update credentials set name = replace(name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo shared credentials
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select credentials.*,xml as xml_before,replace(xml,'<OLD_VALUE>','<NEW_VALUE>') as xml_after from credentials where LOWER(xml) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update credentials set xml = replace(xml,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(xml) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select cwd_application_alias.*,alias_name as alias_name_before,replace(alias_name,'<OLD_VALUE>','<NEW_VALUE>') as alias_name_after from cwd_application_alias where LOWER(alias_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update cwd_application_alias set alias_name = replace(alias_name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(alias_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select cwd_application_alias.*,lower_alias_name as lower_alias_name_before,replace(lower_alias_name,'<OLD_VALUE>','<NEW_VALUE>') as lower_alias_name_after from cwd_application_alias where LOWER(lower_alias_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update cwd_application_alias set lower_alias_name = replace(lower_alias_name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(lower_alias_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : User groups mapping
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select cwd_application.*,description as description_before,replace(description,'<OLD_VALUE>','<NEW_VALUE>') as description_after from cwd_application where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update cwd_application set description = replace(description,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd directories
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select cwd_directory.*,description as description_before,replace(description,'<OLD_VALUE>','<NEW_VALUE>') as description_after from cwd_directory where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update cwd_directory set description = replace(description,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd user tokens
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select cwd_expirable_user_token.*,email_address as email_address_before,'<NEW_VALUE>' as email_address_after from cwd_expirable_user_token where LOWER(email_address) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update cwd_expirable_user_token set email_address = '<NEW_VALUE>' where LOWER(email_address) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd groups
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select cwd_group.*,description as description_before,replace(description,'<OLD_VALUE>','<NEW_VALUE>') as description_after from cwd_group where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update cwd_group set description = replace(description,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select cwd_user.*,email_address as email_address_before,'<NEW_VALUE>' as email_address_after from cwd_user where LOWER(email_address) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update cwd_user set email_address = '<NEW_VALUE>' where LOWER(email_address) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select cwd_user.*,lower_email_address as lower_email_address_before,'<NEW_VALUE>' as lower_email_address_after from cwd_user where LOWER(lower_email_address) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update cwd_user set lower_email_address = '<NEW_VALUE>' where LOWER(lower_email_address) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment configuration
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_env_config.*,docker_pipeline_config as docker_pipeline_config_before,replace(docker_pipeline_config,'<OLD_VALUE>','<NEW_VALUE>') as docker_pipeline_config_after from deployment_env_config where LOWER(docker_pipeline_config) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_env_config set docker_pipeline_config = replace(docker_pipeline_config,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(docker_pipeline_config) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment configuration
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_env_config.*,plugin_config as plugin_config_before,replace(plugin_config,'<OLD_VALUE>','<NEW_VALUE>') as plugin_config_after from deployment_env_config where LOWER(plugin_config) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_env_config set plugin_config = replace(plugin_config,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(plugin_config) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_environment.*,description as description_before,replace(description,'<OLD_VALUE>','<NEW_VALUE>') as description_after from deployment_environment where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_environment set description = replace(description,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_environment.*,name as name_before,replace(name,'<OLD_VALUE>','<NEW_VALUE>') as name_after from deployment_environment where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_environment set name = replace(name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_environment.*,triggers_xml_data as triggers_xml_data_before,replace(triggers_xml_data,'<OLD_VALUE>','<NEW_VALUE>') as triggers_xml_data_after from deployment_environment where LOWER(triggers_xml_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_environment set triggers_xml_data = replace(triggers_xml_data,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(triggers_xml_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_environment.*,xml_definition_data as xml_definition_data_before,replace(xml_definition_data,'<OLD_VALUE>','<NEW_VALUE>') as xml_definition_data_after from deployment_environment where LOWER(xml_definition_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_environment set xml_definition_data = replace(xml_definition_data,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(xml_definition_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment project definition
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_project.*,description as description_before,replace(description,'<OLD_VALUE>','<NEW_VALUE>') as description_after from deployment_project where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_project set description = replace(description,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment project items definition
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_project_item.*,name as name_before,replace(name,'<OLD_VALUE>','<NEW_VALUE>') as name_after from deployment_project_item where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_project_item set name = replace(name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment project definition
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_project.*,name as name_before,replace(name,'<OLD_VALUE>','<NEW_VALUE>') as name_after from deployment_project where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_project set name = replace(name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment result additional data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_result_customdata.*,custom_info_value as custom_info_value_before,replace(custom_info_value,'<OLD_VALUE>','<NEW_VALUE>') as custom_info_value_after from deployment_result_customdata where LOWER(custom_info_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_result_customdata set custom_info_value = replace(custom_info_value,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(custom_info_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment result
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_result.*,version_name as version_name_before,replace(version_name,'<OLD_VALUE>','<NEW_VALUE>') as version_name_after from deployment_result where LOWER(version_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_result set version_name = replace(version_name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(version_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment variable substitution
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_variable_substitution.*,variable_key as variable_key_before,replace(variable_key,'<OLD_VALUE>','<NEW_VALUE>') as variable_key_after from deployment_variable_substitution where LOWER(variable_key) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_variable_substitution set variable_key = replace(variable_key,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(variable_key) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment variable substitution
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_variable_substitution.*,variable_value as variable_value_before,replace(variable_value,'<OLD_VALUE>','<NEW_VALUE>') as variable_value_after from deployment_variable_substitution where LOWER(variable_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_variable_substitution set variable_value = replace(variable_value,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(variable_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version commit
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_version_commit.*,commit_comment_clob as commit_comment_clob_before,replace(commit_comment_clob,'<OLD_VALUE>','<NEW_VALUE>') as commit_comment_clob_after from deployment_version_commit where LOWER(commit_comment_clob) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_version_commit set commit_comment_clob = replace(commit_comment_clob,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(commit_comment_clob) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_version_item_ba.*,copy_pattern as copy_pattern_before,replace(copy_pattern,'<OLD_VALUE>','<NEW_VALUE>') as copy_pattern_after from deployment_version_item_ba where LOWER(copy_pattern) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_version_item_ba set copy_pattern = replace(copy_pattern,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(copy_pattern) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_version_item_ba.*,label as label_before,replace(label,'<OLD_VALUE>','<NEW_VALUE>') as label_after from deployment_version_item_ba where LOWER(label) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_version_item_ba set label = replace(label,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(label) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_version_item_ba.*,location as location_before,replace(location,'<OLD_VALUE>','<NEW_VALUE>') as location_after from deployment_version_item_ba where LOWER(location) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_version_item_ba set location = replace(location,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(location) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version item
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_version_item.*,name as name_before,replace(name,'<OLD_VALUE>','<NEW_VALUE>') as name_after from deployment_version_item where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_version_item set name = replace(name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_version.*,name as name_before,replace(name,'<OLD_VALUE>','<NEW_VALUE>') as name_after from deployment_version where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_version set name = replace(name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version naming
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_version_naming.*,next_version_name as next_version_name_before,replace(next_version_name,'<OLD_VALUE>','<NEW_VALUE>') as next_version_name_after from deployment_version_naming where LOWER(next_version_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_version_naming set next_version_name = replace(next_version_name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(next_version_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_version.*,plan_branch_name as plan_branch_name_before,replace(plan_branch_name,'<OLD_VALUE>','<NEW_VALUE>') as plan_branch_name_after from deployment_version where LOWER(plan_branch_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_version set plan_branch_name = replace(plan_branch_name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(plan_branch_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Elastic image data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select elastic_image.*,description as description_before,replace(description,'<OLD_VALUE>','<NEW_VALUE>') as description_after from elastic_image where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update elastic_image set description = replace(description,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Elastic image data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select elastic_image.*,name as name_before,replace(name,'<OLD_VALUE>','<NEW_VALUE>') as name_after from elastic_image where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update elastic_image set name = replace(name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select groups.*,groupname as groupname_before,replace(groupname,'<OLD_VALUE>','<NEW_VALUE>') as groupname_after from groups where LOWER(groupname) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update groups set groupname = replace(groupname,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(groupname) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select imserver.*,host as host_before,replace(host,'<OLD_VALUE>','<NEW_VALUE>') as host_after from imserver where LOWER(host) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update imserver set host = replace(host,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(host) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select imserver.*,resource_name as resource_name_before,replace(resource_name,'<OLD_VALUE>','<NEW_VALUE>') as resource_name_after from imserver where LOWER(resource_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update imserver set resource_name = replace(resource_name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(resource_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select imserver.*,title as title_before,replace(title,'<OLD_VALUE>','<NEW_VALUE>') as title_after from imserver where LOWER(title) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update imserver set title = replace(title,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(title) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select label.*,name as name_before,replace(name,'<OLD_VALUE>','<NEW_VALUE>') as name_after from label where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update label set name = replace(name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Merge result
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select merge_result.*,failure_reason as failure_reason_before,replace(failure_reason,'<OLD_VALUE>','<NEW_VALUE>') as failure_reason_after from merge_result where LOWER(failure_reason) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update merge_result set failure_reason = replace(failure_reason,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(failure_reason) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo notifications send to user or other recipients
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select notifications.*,recipient as recipient_before,replace(recipient,'<OLD_VALUE>','<NEW_VALUE>') as recipient_after from notifications where (LOWER(recipient) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)') AND recipient_type  <> 'com.atlassian.bamboo.plugin.system.notifications:recipient.user';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update notifications set recipient = replace(recipient,'<OLD_VALUE>','<NEW_VALUE>') where (LOWER(recipient) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)') AND recipient_type  <> 'com.atlassian.bamboo.plugin.system.notifications:recipient.user';

-- Type        : update
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select os_propertyentry.*,entity_key as entity_key_before,replace(entity_key,'<OLD_VALUE>','<NEW_VALUE>') as entity_key_after from os_propertyentry where LOWER(entity_key) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update os_propertyentry set entity_key = replace(entity_key,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(entity_key) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select os_propertyentry.*,entity_name as entity_name_before,replace(entity_name,'<OLD_VALUE>','<NEW_VALUE>') as entity_name_after from os_propertyentry where LOWER(entity_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update os_propertyentry set entity_name = replace(entity_name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(entity_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select os_propertyentry.*,string_val as string_val_before,replace(string_val,'<OLD_VALUE>','<NEW_VALUE>') as string_val_after from os_propertyentry where LOWER(string_val) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update os_propertyentry set string_val = replace(string_val,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(string_val) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Merge result
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select plan_vcs_history.*,xml_custom_data as xml_custom_data_before,replace(xml_custom_data,'<OLD_VALUE>','<NEW_VALUE>') as xml_custom_data_after from plan_vcs_history where LOWER(xml_custom_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update plan_vcs_history set xml_custom_data = replace(xml_custom_data,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(xml_custom_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo project
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select project.*,description as description_before,replace(description,'<OLD_VALUE>','<NEW_VALUE>') as description_after from project where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update project set description = replace(description,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo project
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select project.*,title as title_before,replace(title,'<OLD_VALUE>','<NEW_VALUE>') as title_after from project where LOWER(title) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update project set title = replace(title,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(title) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select queue.*,agent_description as agent_description_before,replace(agent_description,'<OLD_VALUE>','<NEW_VALUE>') as agent_description_after from queue where LOWER(agent_description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update queue set agent_description = replace(agent_description,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(agent_description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select queue.*,title as title_before,replace(title,'<OLD_VALUE>','<NEW_VALUE>') as title_after from queue where LOWER(title) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update queue set title = replace(title,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(title) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select quick_filter_rules.*,configuration as configuration_before,replace(configuration,'<OLD_VALUE>','<NEW_VALUE>') as configuration_after from quick_filter_rules where LOWER(configuration) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update quick_filter_rules set configuration = replace(configuration,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(configuration) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select quick_filter_rules.*,name as name_before,replace(name,'<OLD_VALUE>','<NEW_VALUE>') as name_after from quick_filter_rules where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update quick_filter_rules set name = replace(name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select quick_filters.*,name as name_before,replace(name,'<OLD_VALUE>','<NEW_VALUE>') as name_after from quick_filters where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update quick_filters set name = replace(name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Job requirements
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select requirement.*,key_identifier as key_identifier_before,replace(key_identifier,'<OLD_VALUE>','<NEW_VALUE>') as key_identifier_after from requirement where LOWER(key_identifier) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update requirement set key_identifier = replace(key_identifier,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(key_identifier) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Job requirements
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select requirement.*,match_value as match_value_before,replace(match_value,'<OLD_VALUE>','<NEW_VALUE>') as match_value_after from requirement where LOWER(match_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update requirement set match_value = replace(match_value,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(match_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Scripts data, used eg. for elastic images startup
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select script.*,body as body_before,replace(body,'<OLD_VALUE>','<NEW_VALUE>') as body_after from script where LOWER(body) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update script set body = replace(body,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(body) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Value of variables in result stage
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select stage_variable_context.*,variable_key as variable_key_before,replace(variable_key,'<OLD_VALUE>','<NEW_VALUE>') as variable_key_after from stage_variable_context where LOWER(variable_key) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update stage_variable_context set variable_key = replace(variable_key,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(variable_key) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Value of variables in result stage
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select stage_variable_context.*,variable_value as variable_value_before,replace(variable_value,'<OLD_VALUE>','<NEW_VALUE>') as variable_value_after from stage_variable_context where LOWER(variable_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update stage_variable_context set variable_value = replace(variable_value,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(variable_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Test error content
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select test_error.*,error_content as error_content_before,replace(error_content,'<OLD_VALUE>','<NEW_VALUE>') as error_content_after from test_error where LOWER(error_content) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update test_error set error_content = replace(error_content,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(error_content) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Name of the trusted app
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select trusted_apps.*,app_name as app_name_before,replace(app_name,'<OLD_VALUE>','<NEW_VALUE>') as app_name_after from trusted_apps where LOWER(app_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update trusted_apps set app_name = replace(app_name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(app_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : IP address of the trusted app
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select trusted_apps_ips.*,ip_pattern as ip_pattern_before,'<NEW_VALUE>' as ip_pattern_after from trusted_apps_ips where LOWER(ip_pattern) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update trusted_apps_ips set ip_pattern = '<NEW_VALUE>' where LOWER(ip_pattern) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : bamboo
-- Description : URL of the trusted app
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select trusted_apps_urls.*,url_pattern as url_pattern_before,'<NEW_VALUE>' as url_pattern_after from trusted_apps_urls where LOWER(url_pattern) like LOWER('%<OLD_VALUE>%') ;

-- + UPDATE (be careful)
update trusted_apps_urls set url_pattern = '<NEW_VALUE>' where LOWER(url_pattern) like LOWER('%<OLD_VALUE>%') ;

-- Type        : update
-- Origin      : bamboo
-- Description : author of user comments
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select user_comment.*,content as content_before,replace(content,'<OLD_VALUE>','<NEW_VALUE>') as content_after from user_comment where LOWER(content) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update user_comment set content = replace(content,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(content) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Repository commits comments - imported from vcs repository
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select user_commit.*,commit_comment_clob as commit_comment_clob_before,replace(commit_comment_clob,'<OLD_VALUE>','<NEW_VALUE>') as commit_comment_clob_after from user_commit where LOWER(commit_comment_clob) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update user_commit set commit_comment_clob = replace(commit_comment_clob,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(commit_comment_clob) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo internal users directory
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select users.*,email as email_before,'<NEW_VALUE>' as email_after from users where LOWER(email) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update users set email = '<NEW_VALUE>' where LOWER(email) = LOWER('<OLD_VALUE>') ;

-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo internal users directory
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select users.*,fullname as fullname_before,replace(fullname,'<OLD_VALUE>','<NEW_VALUE>') as fullname_after from users where LOWER(fullname) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update users set fullname = replace(fullname,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(fullname) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select variable_context.*,variable_key as variable_key_before,replace(variable_key,'<OLD_VALUE>','<NEW_VALUE>') as variable_key_after from variable_context where LOWER(variable_key) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update variable_context set variable_key = replace(variable_key,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(variable_key) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select variable_context.*,variable_value as variable_value_before,replace(variable_value,'<OLD_VALUE>','<NEW_VALUE>') as variable_value_after from variable_context where LOWER(variable_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update variable_context set variable_value = replace(variable_value,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(variable_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select variable_substitution.*,variable_key as variable_key_before,replace(variable_key,'<OLD_VALUE>','<NEW_VALUE>') as variable_key_after from variable_substitution where LOWER(variable_key) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update variable_substitution set variable_key = replace(variable_key,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(variable_key) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select variable_substitution.*,variable_value as variable_value_before,replace(variable_value,'<OLD_VALUE>','<NEW_VALUE>') as variable_value_after from variable_substitution where LOWER(variable_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update variable_substitution set variable_value = replace(variable_value,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(variable_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Configuration of vcs repository
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select vcs_location.*,name as name_before,replace(name,'<OLD_VALUE>','<NEW_VALUE>') as name_after from vcs_location where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update vcs_location set name = replace(name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : update
-- Origin      : bamboo
-- Description : Configuration of vcs repository
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select vcs_location.*,xml_definition_data as xml_definition_data_before,replace(xml_definition_data,'<OLD_VALUE>','<NEW_VALUE>') as xml_definition_data_after from vcs_location where LOWER(xml_definition_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update vcs_location set xml_definition_data = replace(xml_definition_data,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(xml_definition_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

