-- Type        : select
-- Origin      : support healthcheck plugin
-- Description : record of dismissed messages
-- Database    : postgresql

select "AO_2F1435_READ_NOTIFICATIONS".* from "AO_2F1435_READ_NOTIFICATIONS" where LOWER("USER_KEY") = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bundled Bamboo plugin
-- Description : no description
-- Database    : postgresql

select "AO_7A45FB_AOTRACKING_USER".* from "AO_7A45FB_AOTRACKING_USER" where LOWER("USERNAME") = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bundled Bamboo plugin
-- Description : no description
-- Database    : postgresql

select "AO_7A45FB_AOTRACKING_USER".* from "AO_7A45FB_AOTRACKING_USER" where LOWER("USER_WHO_UPDATED") = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo plugin
-- Description : ???
-- Database    : postgresql

select "AO_C7F71E_OAUTH_SVC_PROV_TKNS".* from "AO_C7F71E_OAUTH_SVC_PROV_TKNS" where LOWER("USER_NAME") = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo plugin
-- Description : ???
-- Database    : postgresql

select "AO_CABD61_AODASHBOARD_SELECT".* from "AO_CABD61_AODASHBOARD_SELECT" where LOWER("USER_NAME") = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo plugin
-- Description : ???
-- Database    : postgresql

select "AO_F1B80E_BRANCH_SELECTION".* from "AO_F1B80E_BRANCH_SELECTION" where LOWER("USER") = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : user permissions
-- Database    : postgresql

select "acl_entry".* from acl_entry where (LOWER(sid) = LOWER('<OLD_VALUE>') ) AND type  = 'PRINCIPAL';

-- Type        : select
-- Origin      : bamboo
-- Description : user permissions - ownership
-- Database    : postgresql

select "acl_object_identity".* from acl_object_identity where (LOWER(owner_sid) = LOWER('<OLD_VALUE>') ) AND owner_type  = 'PRINCIPAL';

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : postgresql

select "audit_log".* from audit_log where LOWER(user_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : last user login, number of failed logins per user
-- Database    : postgresql

select "auth_attempt_info".* from auth_attempt_info where LOWER(user_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Repository author
-- Database    : postgresql

select "author".* from author where LOWER(linked_user_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : creator of result label
-- Database    : postgresql

select "buildresultsummary_label".* from buildresultsummary_label where LOWER(user_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Default group name
-- Database    : postgresql

select "cwd_app_dir_default_groups".* from cwd_app_dir_default_groups where LOWER(group_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : User groups mapping
-- Database    : postgresql

select "cwd_app_dir_group_mapping".* from cwd_app_dir_group_mapping where LOWER(group_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : postgresql

select "cwd_application_alias".* from cwd_application_alias where LOWER(lower_user_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : postgresql

select "cwd_application_alias".* from cwd_application_alias where LOWER(user_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd user tokens
-- Database    : postgresql

select "cwd_expirable_user_token".* from cwd_expirable_user_token where LOWER(user_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd granted permissions
-- Database    : postgresql

select "cwd_granted_perm".* from cwd_granted_perm where LOWER(group_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd groups
-- Database    : postgresql

select "cwd_group".* from cwd_group where LOWER(group_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd groups
-- Database    : postgresql

select "cwd_group".* from cwd_group where LOWER(lower_group_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd user groups membership
-- Database    : postgresql

select "cwd_membership".* from cwd_membership where LOWER(child_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd user groups membership
-- Database    : postgresql

select "cwd_membership".* from cwd_membership where LOWER(lower_child_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd user groups membership
-- Database    : postgresql

select "cwd_membership".* from cwd_membership where LOWER(lower_parent_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd user groups membership
-- Database    : postgresql

select "cwd_membership".* from cwd_membership where LOWER(parent_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd synchronisation tokens
-- Database    : postgresql

select "cwd_token".* from cwd_token where LOWER(entity_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd tombstone
-- Database    : postgresql

select "cwd_tombstone".* from cwd_tombstone where LOWER(entity_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd tombstone
-- Database    : postgresql

select "cwd_tombstone".* from cwd_tombstone where LOWER(parent) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : postgresql

select "cwd_user".* from cwd_user where LOWER(display_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : postgresql

select "cwd_user".* from cwd_user where LOWER(first_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : postgresql

select "cwd_user".* from cwd_user where LOWER(last_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : postgresql

select "cwd_user".* from cwd_user where LOWER(lower_display_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : postgresql

select "cwd_user".* from cwd_user where LOWER(lower_first_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : postgresql

select "cwd_user".* from cwd_user where LOWER(lower_last_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : postgresql

select "cwd_user".* from cwd_user where LOWER(lower_user_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : postgresql

select "cwd_user".* from cwd_user where LOWER(user_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : postgresql

select "deployment_version".* from deployment_version where LOWER(creator_username) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : deployment version approver
-- Database    : postgresql

select "deployment_version_status".* from deployment_version_status where LOWER(user_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : External users name
-- Database    : postgresql

select "external_entities".* from external_entities where LOWER(name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : postgresql

select "imserver".* from imserver where LOWER(username) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : postgresql

select "label".* from label where LOWER(namespace) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo notifications send to user or other recipients
-- Database    : postgresql

select "notifications".* from notifications where (LOWER(recipient) = LOWER('<OLD_VALUE>') ) AND recipient_type  = 'com.atlassian.bamboo.plugin.system.notifications:recipient.user';

-- Type        : select
-- Origin      : bamboo
-- Description : reset password tokens requested by user
-- Database    : postgresql

select "password_reset_token".* from password_reset_token where LOWER(username) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : used to auto-login user based on cookie
-- Database    : postgresql

select "rememberme_token".* from rememberme_token where LOWER(username) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : user that has quarantined test case
-- Database    : postgresql

select "test_case".* from test_case where LOWER(quarantining_username) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : author of user comments
-- Database    : postgresql

select "user_comment".* from user_comment where LOWER(user_name) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo internal users directory
-- Database    : postgresql

select "users".* from users where LOWER(name) = LOWER('<OLD_VALUE>') ;

