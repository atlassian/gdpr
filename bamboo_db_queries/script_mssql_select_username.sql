-- Type        : select
-- Origin      : support healthcheck plugin
-- Description : record of dismissed messages
-- Database    : mssql

select AO_2F1435_READ_NOTIFICATIONS.* from AO_2F1435_READ_NOTIFICATIONS where USER_KEY = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bundled Bamboo plugin
-- Description : no description
-- Database    : mssql

select AO_7A45FB_AOTRACKING_USER.* from AO_7A45FB_AOTRACKING_USER where USERNAME = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bundled Bamboo plugin
-- Description : no description
-- Database    : mssql

select AO_7A45FB_AOTRACKING_USER.* from AO_7A45FB_AOTRACKING_USER where USER_WHO_UPDATED = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo plugin
-- Description : ???
-- Database    : mssql

select AO_C7F71E_OAUTH_SVC_PROV_TKNS.* from AO_C7F71E_OAUTH_SVC_PROV_TKNS where USER_NAME = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo plugin
-- Description : ???
-- Database    : mssql

select AO_CABD61_AODASHBOARD_SELECT.* from AO_CABD61_AODASHBOARD_SELECT where USER_NAME = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo plugin
-- Description : ???
-- Database    : mssql

select AO_F1B80E_BRANCH_SELECTION.* from AO_F1B80E_BRANCH_SELECTION where "USER" = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : user permissions
-- Database    : mssql

select acl_entry.* from acl_entry where (sid = '<OLD_VALUE>' ) AND type  = 'PRINCIPAL';

-- Type        : select
-- Origin      : bamboo
-- Description : user permissions - ownership
-- Database    : mssql

select acl_object_identity.* from acl_object_identity where (owner_sid = '<OLD_VALUE>' ) AND owner_type  = 'PRINCIPAL';

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mssql

select audit_log.* from audit_log where user_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : last user login, number of failed logins per user
-- Database    : mssql

select auth_attempt_info.* from auth_attempt_info where user_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Repository author
-- Database    : mssql

select author.* from author where linked_user_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : creator of result label
-- Database    : mssql

select buildresultsummary_label.* from buildresultsummary_label where user_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Default group name
-- Database    : mssql

select cwd_app_dir_default_groups.* from cwd_app_dir_default_groups where group_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : User groups mapping
-- Database    : mssql

select cwd_app_dir_group_mapping.* from cwd_app_dir_group_mapping where group_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : mssql

select cwd_application_alias.* from cwd_application_alias where lower_user_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : mssql

select cwd_application_alias.* from cwd_application_alias where user_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd user tokens
-- Database    : mssql

select cwd_expirable_user_token.* from cwd_expirable_user_token where user_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd granted permissions
-- Database    : mssql

select cwd_granted_perm.* from cwd_granted_perm where group_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd groups
-- Database    : mssql

select cwd_group.* from cwd_group where group_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd groups
-- Database    : mssql

select cwd_group.* from cwd_group where lower_group_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd user groups membership
-- Database    : mssql

select cwd_membership.* from cwd_membership where child_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd user groups membership
-- Database    : mssql

select cwd_membership.* from cwd_membership where lower_child_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd user groups membership
-- Database    : mssql

select cwd_membership.* from cwd_membership where lower_parent_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd user groups membership
-- Database    : mssql

select cwd_membership.* from cwd_membership where parent_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd synchronisation tokens
-- Database    : mssql

select cwd_token.* from cwd_token where entity_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd tombstone
-- Database    : mssql

select cwd_tombstone.* from cwd_tombstone where entity_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd tombstone
-- Database    : mssql

select cwd_tombstone.* from cwd_tombstone where parent = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mssql

select cwd_user.* from cwd_user where display_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mssql

select cwd_user.* from cwd_user where first_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mssql

select cwd_user.* from cwd_user where last_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mssql

select cwd_user.* from cwd_user where lower_display_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mssql

select cwd_user.* from cwd_user where lower_first_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mssql

select cwd_user.* from cwd_user where lower_last_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mssql

select cwd_user.* from cwd_user where lower_user_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mssql

select cwd_user.* from cwd_user where user_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : mssql

select deployment_version.* from deployment_version where creator_username = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : deployment version approver
-- Database    : mssql

select deployment_version_status.* from deployment_version_status where user_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : External users name
-- Database    : mssql

select external_entities.* from external_entities where name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : mssql

select imserver.* from imserver where username = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : mssql

select label.* from label where namespace = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo notifications send to user or other recipients
-- Database    : mssql

select notifications.* from notifications where (recipient = '<OLD_VALUE>' ) AND recipient_type  = 'com.atlassian.bamboo.plugin.system.notifications:recipient.user';

-- Type        : select
-- Origin      : bamboo
-- Description : reset password tokens requested by user
-- Database    : mssql

select password_reset_token.* from password_reset_token where username = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : used to auto-login user based on cookie
-- Database    : mssql

select rememberme_token.* from rememberme_token where username = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : user that has quarantined test case
-- Database    : mssql

select test_case.* from test_case where quarantining_username = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : author of user comments
-- Database    : mssql

select user_comment.* from user_comment where user_name = '<OLD_VALUE>' ;

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo internal users directory
-- Database    : mssql

select users.* from users where name = '<OLD_VALUE>' ;

