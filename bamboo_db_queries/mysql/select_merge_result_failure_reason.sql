-- Type        : select
-- Origin      : bamboo
-- Description : Merge result
-- Database    : mysql

select merge_result.* from merge_result where LOWER(failure_reason) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

