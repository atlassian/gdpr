-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in result stage
-- Database    : mysql

select stage_variable_context.* from stage_variable_context where LOWER(variable_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

