-- Type        : select
-- Origin      : bamboo
-- Description : Merge result
-- Database    : mysql

select plan_vcs_history.* from plan_vcs_history where LOWER(xml_custom_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

