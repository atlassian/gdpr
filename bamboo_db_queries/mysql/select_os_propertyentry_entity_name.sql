-- Type        : select
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : mysql

select os_propertyentry.* from os_propertyentry where LOWER(entity_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

