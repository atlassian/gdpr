-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_version_item_ba.*,label as label_before,replace(label,'<OLD_VALUE>','<NEW_VALUE>') as label_after from deployment_version_item_ba where LOWER(label) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_version_item_ba set label = replace(label,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(label) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

