-- Type        : update
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_version.*,name as name_before,replace(name,'<OLD_VALUE>','<NEW_VALUE>') as name_after from deployment_version where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_version set name = replace(name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

