-- Type        : select
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : mysql

select artifact_definition.* from artifact_definition where LOWER(copy_pattern) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

