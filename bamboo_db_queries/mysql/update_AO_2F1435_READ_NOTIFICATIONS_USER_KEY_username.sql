-- Type        : update
-- Origin      : support healthcheck plugin
-- Description : record of dismissed messages
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select AO_2F1435_READ_NOTIFICATIONS.*,USER_KEY as USER_KEY_before,'<NEW_VALUE>' as USER_KEY_after from AO_2F1435_READ_NOTIFICATIONS where LOWER(USER_KEY) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_2F1435_READ_NOTIFICATIONS set USER_KEY = '<NEW_VALUE>' where LOWER(USER_KEY) = LOWER('<OLD_VALUE>') ;

