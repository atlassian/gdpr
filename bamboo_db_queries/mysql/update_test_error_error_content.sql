-- Type        : update
-- Origin      : bamboo
-- Description : Test error content
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select test_error.*,error_content as error_content_before,replace(error_content,'<OLD_VALUE>','<NEW_VALUE>') as error_content_after from test_error where LOWER(error_content) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update test_error set error_content = replace(error_content,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(error_content) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

