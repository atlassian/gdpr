-- Type        : select
-- Origin      : bamboo
-- Description : Deployment result additional data
-- Database    : mysql

select deployment_result_customdata.* from deployment_result_customdata where LOWER(custom_info_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

