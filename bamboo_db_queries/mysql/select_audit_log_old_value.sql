-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : mysql

select audit_log.* from audit_log where LOWER(old_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

