-- Type        : update
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select variable_substitution.*,variable_key as variable_key_before,replace(variable_key,'<OLD_VALUE>','<NEW_VALUE>') as variable_key_after from variable_substitution where LOWER(variable_key) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update variable_substitution set variable_key = replace(variable_key,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(variable_key) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

