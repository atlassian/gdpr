-- Type        : update
-- Origin      : bamboo
-- Description : Merge result
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select merge_result.*,failure_reason as failure_reason_before,replace(failure_reason,'<OLD_VALUE>','<NEW_VALUE>') as failure_reason_after from merge_result where LOWER(failure_reason) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update merge_result set failure_reason = replace(failure_reason,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(failure_reason) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

