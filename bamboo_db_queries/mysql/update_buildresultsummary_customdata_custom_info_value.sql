-- Type        : update
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select buildresultsummary_customdata.*,custom_info_value as custom_info_value_before,replace(custom_info_value,'<OLD_VALUE>','<NEW_VALUE>') as custom_info_value_after from buildresultsummary_customdata where LOWER(custom_info_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update buildresultsummary_customdata set custom_info_value = replace(custom_info_value,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(custom_info_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

