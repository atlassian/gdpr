-- Type        : select
-- Origin      : bamboo
-- Description : Elastic image data
-- Database    : mysql

select elastic_image.* from elastic_image where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

