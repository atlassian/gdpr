-- Type        : select
-- Origin      : bamboo
-- Description : Deployment result
-- Database    : mysql

select deployment_result.* from deployment_result where LOWER(version_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

