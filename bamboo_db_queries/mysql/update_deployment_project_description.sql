-- Type        : update
-- Origin      : bamboo
-- Description : Deployment project definition
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_project.*,description as description_before,replace(description,'<OLD_VALUE>','<NEW_VALUE>') as description_after from deployment_project where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_project set description = replace(description,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

