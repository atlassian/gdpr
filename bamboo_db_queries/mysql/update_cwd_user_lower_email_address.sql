-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select cwd_user.*,lower_email_address as lower_email_address_before,'<NEW_VALUE>' as lower_email_address_after from cwd_user where LOWER(lower_email_address) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update cwd_user set lower_email_address = '<NEW_VALUE>' where LOWER(lower_email_address) = LOWER('<OLD_VALUE>') ;

