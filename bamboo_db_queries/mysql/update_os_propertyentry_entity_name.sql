-- Type        : update
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select os_propertyentry.*,entity_name as entity_name_before,replace(entity_name,'<OLD_VALUE>','<NEW_VALUE>') as entity_name_after from os_propertyentry where LOWER(entity_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update os_propertyentry set entity_name = replace(entity_name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(entity_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

