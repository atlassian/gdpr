-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd groups
-- Database    : mysql

select cwd_group.* from cwd_group where LOWER(description) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

