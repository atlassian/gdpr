-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : mysql

select deployment_environment.* from deployment_environment where LOWER(xml_definition_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

