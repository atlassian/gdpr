-- Type        : update
-- Origin      : bamboo
-- Description : IP address of the trusted app
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select trusted_apps_ips.*,ip_pattern as ip_pattern_before,'<NEW_VALUE>' as ip_pattern_after from trusted_apps_ips where LOWER(ip_pattern) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update trusted_apps_ips set ip_pattern = '<NEW_VALUE>' where LOWER(ip_pattern) = LOWER('<OLD_VALUE>') ;

