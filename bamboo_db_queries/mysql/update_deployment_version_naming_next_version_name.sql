-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version naming
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_version_naming.*,next_version_name as next_version_name_before,replace(next_version_name,'<OLD_VALUE>','<NEW_VALUE>') as next_version_name_after from deployment_version_naming where LOWER(next_version_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_version_naming set next_version_name = replace(next_version_name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(next_version_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

