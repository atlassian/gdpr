-- Type        : update
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select AO_A0B856_WEB_HOOK_LISTENER_AO.*,URL as URL_before,'<NEW_VALUE>' as URL_after from AO_A0B856_WEB_HOOK_LISTENER_AO where LOWER(URL) like LOWER('%<OLD_VALUE>%') ;

-- + UPDATE (be careful)
update AO_A0B856_WEB_HOOK_LISTENER_AO set URL = '<NEW_VALUE>' where LOWER(URL) like LOWER('%<OLD_VALUE>%') ;

