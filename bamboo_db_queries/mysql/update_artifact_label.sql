-- Type        : update
-- Origin      : bamboo
-- Description : Artefact data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select artifact.*,label as label_before,replace(label,'<OLD_VALUE>','<NEW_VALUE>') as label_after from artifact where LOWER(label) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update artifact set label = replace(label,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(label) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

