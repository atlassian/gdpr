-- Type        : select
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : mysql

select AO_A0B856_WEB_HOOK_LISTENER_AO.* from AO_A0B856_WEB_HOOK_LISTENER_AO where LOWER(DESCRIPTION) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

