-- Type        : select
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : mysql

select groups.* from groups where LOWER(groupname) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

