-- Type        : update
-- Origin      : bamboo
-- Description : Merge result
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select plan_vcs_history.*,xml_custom_data as xml_custom_data_before,replace(xml_custom_data,'<OLD_VALUE>','<NEW_VALUE>') as xml_custom_data_after from plan_vcs_history where LOWER(xml_custom_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update plan_vcs_history set xml_custom_data = replace(xml_custom_data,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(xml_custom_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

