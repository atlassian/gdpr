-- Type        : select
-- Origin      : bamboo
-- Description : Artefact data
-- Database    : mysql

select artifact.* from artifact where LOWER(label) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

