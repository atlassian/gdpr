-- Type        : select
-- Origin      : bamboo
-- Description : author of user comments
-- Database    : mysql

select user_comment.* from user_comment where LOWER(content) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

