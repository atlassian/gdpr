-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd user groups membership
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select cwd_membership.*,lower_child_name as lower_child_name_before,'<NEW_VALUE>' as lower_child_name_after from cwd_membership where LOWER(lower_child_name) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update cwd_membership set lower_child_name = '<NEW_VALUE>' where LOWER(lower_child_name) = LOWER('<OLD_VALUE>') ;

