-- Type        : update
-- Origin      : bamboo
-- Description : last user login, number of failed logins per user
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select auth_attempt_info.*,user_name as user_name_before,'<NEW_VALUE>' as user_name_after from auth_attempt_info where LOWER(user_name) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update auth_attempt_info set user_name = '<NEW_VALUE>' where LOWER(user_name) = LOWER('<OLD_VALUE>') ;

