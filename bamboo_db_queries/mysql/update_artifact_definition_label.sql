-- Type        : update
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select artifact_definition.*,label as label_before,replace(label,'<OLD_VALUE>','<NEW_VALUE>') as label_after from artifact_definition where LOWER(label) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update artifact_definition set label = replace(label,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(label) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

