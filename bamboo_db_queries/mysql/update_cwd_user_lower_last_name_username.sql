-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select cwd_user.*,lower_last_name as lower_last_name_before,'<NEW_VALUE>' as lower_last_name_after from cwd_user where LOWER(lower_last_name) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update cwd_user set lower_last_name = '<NEW_VALUE>' where LOWER(lower_last_name) = LOWER('<OLD_VALUE>') ;

