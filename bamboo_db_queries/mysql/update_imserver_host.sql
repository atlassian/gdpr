-- Type        : update
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select imserver.*,host as host_before,replace(host,'<OLD_VALUE>','<NEW_VALUE>') as host_after from imserver where LOWER(host) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update imserver set host = replace(host,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(host) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

