-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select cwd_user.*,first_name as first_name_before,'<NEW_VALUE>' as first_name_after from cwd_user where LOWER(first_name) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update cwd_user set first_name = '<NEW_VALUE>' where LOWER(first_name) = LOWER('<OLD_VALUE>') ;

