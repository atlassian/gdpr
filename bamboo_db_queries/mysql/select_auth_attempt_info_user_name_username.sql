-- Type        : select
-- Origin      : bamboo
-- Description : last user login, number of failed logins per user
-- Database    : mysql

select auth_attempt_info.* from auth_attempt_info where LOWER(user_name) = LOWER('<OLD_VALUE>') ;

