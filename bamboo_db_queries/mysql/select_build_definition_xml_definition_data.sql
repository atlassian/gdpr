-- Type        : select
-- Origin      : bamboo
-- Description : Job configuration
-- Database    : mysql

select build_definition.* from build_definition where LOWER(xml_definition_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

