-- Type        : select
-- Origin      : bamboo
-- Description : set of various values used by system; may contain username
-- Database    : mysql

select bandana.* from bandana where LOWER(serialized_data) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

