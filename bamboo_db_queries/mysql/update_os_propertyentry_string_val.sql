-- Type        : update
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select os_propertyentry.*,string_val as string_val_before,replace(string_val,'<OLD_VALUE>','<NEW_VALUE>') as string_val_after from os_propertyentry where LOWER(string_val) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update os_propertyentry set string_val = replace(string_val,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(string_val) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

