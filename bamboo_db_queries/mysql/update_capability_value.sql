-- Type        : update
-- Origin      : bamboo
-- Description : Agent capabilities
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select capability.*,value as value_before,replace(value,'<OLD_VALUE>','<NEW_VALUE>') as value_after from capability where LOWER(value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update capability set value = replace(value,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

