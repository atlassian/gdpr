-- Type        : select
-- Origin      : bamboo
-- Description : Directory pattern of result artifacts subscription
-- Database    : mysql

select brs_consumed_subscription.* from brs_consumed_subscription where LOWER(dst_directory) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

