-- Type        : update
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select artifact_definition.*,copy_pattern as copy_pattern_before,replace(copy_pattern,'<OLD_VALUE>','<NEW_VALUE>') as copy_pattern_after from artifact_definition where LOWER(copy_pattern) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update artifact_definition set copy_pattern = replace(copy_pattern,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(copy_pattern) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

