-- Type        : update
-- Origin      : bamboo
-- Description : User groups mapping
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select cwd_app_dir_group_mapping.*,group_name as group_name_before,'<NEW_VALUE>' as group_name_after from cwd_app_dir_group_mapping where LOWER(group_name) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update cwd_app_dir_group_mapping set group_name = '<NEW_VALUE>' where LOWER(group_name) = LOWER('<OLD_VALUE>') ;

