-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo internal users directory
-- Database    : mysql

select users.* from users where LOWER(fullname) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

