-- Type        : update
-- Origin      : bamboo
-- Description : Repository commits comments - imported from vcs repository
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select user_commit.*,commit_comment_clob as commit_comment_clob_before,replace(commit_comment_clob,'<OLD_VALUE>','<NEW_VALUE>') as commit_comment_clob_after from user_commit where LOWER(commit_comment_clob) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update user_commit set commit_comment_clob = replace(commit_comment_clob,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(commit_comment_clob) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

