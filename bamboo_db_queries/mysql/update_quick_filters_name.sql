-- Type        : update
-- Origin      : bamboo
-- Description : Build queue
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select quick_filters.*,name as name_before,replace(name,'<OLD_VALUE>','<NEW_VALUE>') as name_after from quick_filters where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update quick_filters set name = replace(name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

