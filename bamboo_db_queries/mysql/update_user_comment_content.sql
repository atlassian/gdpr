-- Type        : update
-- Origin      : bamboo
-- Description : author of user comments
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select user_comment.*,content as content_before,replace(content,'<OLD_VALUE>','<NEW_VALUE>') as content_after from user_comment where LOWER(content) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update user_comment set content = replace(content,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(content) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

