-- Type        : select
-- Origin      : bamboo
-- Description : Deployment variable substitution
-- Database    : mysql

select deployment_variable_substitution.* from deployment_variable_substitution where LOWER(variable_key) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

