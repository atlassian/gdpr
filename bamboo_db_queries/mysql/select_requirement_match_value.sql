-- Type        : select
-- Origin      : bamboo
-- Description : Job requirements
-- Database    : mysql

select requirement.* from requirement where LOWER(match_value) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

