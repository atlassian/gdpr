-- Type        : update
-- Origin      : bamboo
-- Description : Configuration of vcs repository
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select vcs_location.*,name as name_before,replace(name,'<OLD_VALUE>','<NEW_VALUE>') as name_after from vcs_location where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update vcs_location set name = replace(name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

