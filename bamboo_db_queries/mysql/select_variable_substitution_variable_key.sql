-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : mysql

select variable_substitution.* from variable_substitution where LOWER(variable_key) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

