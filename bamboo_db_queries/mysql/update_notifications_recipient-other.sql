-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo notifications send to user or other recipients
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select notifications.*,recipient as recipient_before,replace(recipient,'<OLD_VALUE>','<NEW_VALUE>') as recipient_after from notifications where (LOWER(recipient) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)') AND recipient_type  <> 'com.atlassian.bamboo.plugin.system.notifications:recipient.user';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update notifications set recipient = replace(recipient,'<OLD_VALUE>','<NEW_VALUE>') where (LOWER(recipient) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)') AND recipient_type  <> 'com.atlassian.bamboo.plugin.system.notifications:recipient.user';

