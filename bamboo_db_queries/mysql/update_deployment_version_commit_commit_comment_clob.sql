-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version commit
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select deployment_version_commit.*,commit_comment_clob as commit_comment_clob_before,replace(commit_comment_clob,'<OLD_VALUE>','<NEW_VALUE>') as commit_comment_clob_after from deployment_version_commit where LOWER(commit_comment_clob) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update deployment_version_commit set commit_comment_clob = replace(commit_comment_clob,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(commit_comment_clob) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

