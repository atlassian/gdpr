-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo shared credentials
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select credentials.*,xml as xml_before,replace(xml,'<OLD_VALUE>','<NEW_VALUE>') as xml_after from credentials where LOWER(xml) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update credentials set xml = replace(xml,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(xml) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

