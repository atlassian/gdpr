-- Type        : update
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select imserver.*,resource_name as resource_name_before,replace(resource_name,'<OLD_VALUE>','<NEW_VALUE>') as resource_name_after from imserver where LOWER(resource_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update imserver set resource_name = replace(resource_name,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(resource_name) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

