-- Type        : update
-- Origin      : bundled Bamboo plugin
-- Description : no description
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select AO_7A45FB_AOTRACKING_USER.*,USERNAME as USERNAME_before,'<NEW_VALUE>' as USERNAME_after from AO_7A45FB_AOTRACKING_USER where LOWER(USERNAME) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_7A45FB_AOTRACKING_USER set USERNAME = '<NEW_VALUE>' where LOWER(USERNAME) = LOWER('<OLD_VALUE>') ;

