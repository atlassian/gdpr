-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd user tokens
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select cwd_expirable_user_token.*,user_name as user_name_before,'<NEW_VALUE>' as user_name_after from cwd_expirable_user_token where LOWER(user_name) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update cwd_expirable_user_token set user_name = '<NEW_VALUE>' where LOWER(user_name) = LOWER('<OLD_VALUE>') ;

