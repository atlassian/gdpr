-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version commit
-- Database    : mysql

select deployment_version_commit.* from deployment_version_commit where LOWER(commit_comment_clob) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

