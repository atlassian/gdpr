-- Type        : update
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select AO_A0B856_WEB_HOOK_LISTENER_AO.*,DESCRIPTION as DESCRIPTION_before,replace(DESCRIPTION,'<OLD_VALUE>','<NEW_VALUE>') as DESCRIPTION_after from AO_A0B856_WEB_HOOK_LISTENER_AO where LOWER(DESCRIPTION) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update AO_A0B856_WEB_HOOK_LISTENER_AO set DESCRIPTION = replace(DESCRIPTION,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(DESCRIPTION) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

