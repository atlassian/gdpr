-- Type        : update
-- Origin      : bamboo
-- Description : used to auto-login user based on cookie
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select rememberme_token.*,username as username_before,'<NEW_VALUE>' as username_after from rememberme_token where LOWER(username) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update rememberme_token set username = '<NEW_VALUE>' where LOWER(username) = LOWER('<OLD_VALUE>') ;

