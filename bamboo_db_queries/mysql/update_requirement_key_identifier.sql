-- Type        : update
-- Origin      : bamboo
-- Description : Job requirements
-- Database    : mysql

 
-- + SELECT (please review changes BEFORE)
select requirement.*,key_identifier as key_identifier_before,replace(key_identifier,'<OLD_VALUE>','<NEW_VALUE>') as key_identifier_after from requirement where LOWER(key_identifier) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
-- this statement will probably not work as MySQL has no REGEXP_REPLACE function and will not update column in case-insensitive manner. Manual inspection/processing of data is advised.
update requirement set key_identifier = replace(key_identifier,'<OLD_VALUE>','<NEW_VALUE>') where LOWER(key_identifier) regexp '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

