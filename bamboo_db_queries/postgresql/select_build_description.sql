-- Type        : select
-- Origin      : bamboo
-- Description : Plan / Job data
-- Database    : postgresql

select "build".* from build where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

