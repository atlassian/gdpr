-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo internal users directory
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "users".*,fullname as fullname_before,REGEXP_REPLACE(fullname,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as fullname_after from users where LOWER(fullname) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update users set fullname = REGEXP_REPLACE(fullname,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(fullname) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

