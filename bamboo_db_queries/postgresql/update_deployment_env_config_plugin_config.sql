-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment configuration
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "deployment_env_config".*,plugin_config as plugin_config_before,REGEXP_REPLACE(plugin_config,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as plugin_config_after from deployment_env_config where LOWER(plugin_config) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update deployment_env_config set plugin_config = REGEXP_REPLACE(plugin_config,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(plugin_config) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

