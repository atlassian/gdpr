-- Type        : update
-- Origin      : bamboo
-- Description : Deployment version commit
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "deployment_version_commit".*,commit_comment_clob as commit_comment_clob_before,REGEXP_REPLACE(commit_comment_clob,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as commit_comment_clob_after from deployment_version_commit where LOWER(commit_comment_clob) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update deployment_version_commit set commit_comment_clob = REGEXP_REPLACE(commit_comment_clob,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(commit_comment_clob) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

