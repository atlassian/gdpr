-- Type        : update
-- Origin      : bamboo
-- Description : user permissions
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "acl_entry".*,sid as sid_before,'<NEW_VALUE>' as sid_after from acl_entry where (LOWER(sid) = LOWER('<OLD_VALUE>') ) AND type  = 'PRINCIPAL';

-- + UPDATE (be careful)
update acl_entry set sid = '<NEW_VALUE>' where (LOWER(sid) = LOWER('<OLD_VALUE>') ) AND type  = 'PRINCIPAL';

