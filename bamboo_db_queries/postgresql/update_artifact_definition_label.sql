-- Type        : update
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "artifact_definition".*,label as label_before,REGEXP_REPLACE(label,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as label_after from artifact_definition where LOWER(label) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update artifact_definition set label = REGEXP_REPLACE(label,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(label) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

