-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "cwd_application_alias".*,user_name as user_name_before,'<NEW_VALUE>' as user_name_after from cwd_application_alias where LOWER(user_name) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update cwd_application_alias set user_name = '<NEW_VALUE>' where LOWER(user_name) = LOWER('<OLD_VALUE>') ;

