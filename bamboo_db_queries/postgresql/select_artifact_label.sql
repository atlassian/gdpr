-- Type        : select
-- Origin      : bamboo
-- Description : Artefact data
-- Database    : postgresql

select "artifact".* from artifact where LOWER(label) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

