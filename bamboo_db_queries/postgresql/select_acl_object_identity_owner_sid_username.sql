-- Type        : select
-- Origin      : bamboo
-- Description : user permissions - ownership
-- Database    : postgresql

select "acl_object_identity".* from acl_object_identity where (LOWER(owner_sid) = LOWER('<OLD_VALUE>') ) AND owner_type  = 'PRINCIPAL';

