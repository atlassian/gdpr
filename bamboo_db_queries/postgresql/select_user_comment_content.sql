-- Type        : select
-- Origin      : bamboo
-- Description : author of user comments
-- Database    : postgresql

select "user_comment".* from user_comment where LOWER(content) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

