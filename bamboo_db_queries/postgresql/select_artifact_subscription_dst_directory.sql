-- Type        : select
-- Origin      : bamboo
-- Description : Artifact subscription
-- Database    : postgresql

select "artifact_subscription".* from artifact_subscription where LOWER(dst_directory) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

