-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd user groups membership
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "cwd_membership".*,child_name as child_name_before,'<NEW_VALUE>' as child_name_after from cwd_membership where LOWER(child_name) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update cwd_membership set child_name = '<NEW_VALUE>' where LOWER(child_name) = LOWER('<OLD_VALUE>') ;

