-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "audit_log".*,old_value as old_value_before,REGEXP_REPLACE(old_value,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as old_value_after from audit_log where LOWER(old_value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update audit_log set old_value = REGEXP_REPLACE(old_value,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(old_value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

