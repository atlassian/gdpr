-- Type        : update
-- Origin      : bamboo
-- Description : Deployment variable substitution
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "deployment_variable_substitution".*,variable_key as variable_key_before,REGEXP_REPLACE(variable_key,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as variable_key_after from deployment_variable_substitution where LOWER(variable_key) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update deployment_variable_substitution set variable_key = REGEXP_REPLACE(variable_key,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(variable_key) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

