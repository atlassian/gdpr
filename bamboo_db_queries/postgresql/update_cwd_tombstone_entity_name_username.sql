-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd tombstone
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "cwd_tombstone".*,entity_name as entity_name_before,'<NEW_VALUE>' as entity_name_after from cwd_tombstone where LOWER(entity_name) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update cwd_tombstone set entity_name = '<NEW_VALUE>' where LOWER(entity_name) = LOWER('<OLD_VALUE>') ;

