-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo shared credentials
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "credentials".*,xml as xml_before,REGEXP_REPLACE(xml,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as xml_after from credentials where LOWER(xml) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update credentials set xml = REGEXP_REPLACE(xml,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(xml) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

