-- Type        : select
-- Origin      : bamboo
-- Description : set of various values used by system; may contain username
-- Database    : postgresql

select "bandana".* from bandana where LOWER(serialized_data) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

