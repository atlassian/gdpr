-- Type        : update
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "deployment_environment".*,xml_definition_data as xml_definition_data_before,REGEXP_REPLACE(xml_definition_data,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as xml_definition_data_after from deployment_environment where LOWER(xml_definition_data) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update deployment_environment set xml_definition_data = REGEXP_REPLACE(xml_definition_data,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(xml_definition_data) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

