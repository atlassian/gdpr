-- Type        : update
-- Origin      : bamboo
-- Description : Artifact subscription
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "artifact_subscription".*,dst_directory as dst_directory_before,REGEXP_REPLACE(dst_directory,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as dst_directory_after from artifact_subscription where LOWER(dst_directory) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update artifact_subscription set dst_directory = REGEXP_REPLACE(dst_directory,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(dst_directory) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

