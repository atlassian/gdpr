-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : postgresql

select "deployment_environment".* from deployment_environment where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

