-- Type        : update
-- Origin      : bamboo
-- Description : Job configuration
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "build_definition".*,xml_definition_data as xml_definition_data_before,REGEXP_REPLACE(xml_definition_data,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as xml_definition_data_after from build_definition where LOWER(xml_definition_data) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update build_definition set xml_definition_data = REGEXP_REPLACE(xml_definition_data,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(xml_definition_data) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

