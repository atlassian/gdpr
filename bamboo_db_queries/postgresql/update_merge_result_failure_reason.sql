-- Type        : update
-- Origin      : bamboo
-- Description : Merge result
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "merge_result".*,failure_reason as failure_reason_before,REGEXP_REPLACE(failure_reason,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as failure_reason_after from merge_result where LOWER(failure_reason) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update merge_result set failure_reason = REGEXP_REPLACE(failure_reason,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(failure_reason) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

