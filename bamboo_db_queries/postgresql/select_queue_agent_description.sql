-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : postgresql

select "queue".* from queue where LOWER(agent_description) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

