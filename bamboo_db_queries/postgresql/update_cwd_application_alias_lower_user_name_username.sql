-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "cwd_application_alias".*,lower_user_name as lower_user_name_before,'<NEW_VALUE>' as lower_user_name_after from cwd_application_alias where LOWER(lower_user_name) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update cwd_application_alias set lower_user_name = '<NEW_VALUE>' where LOWER(lower_user_name) = LOWER('<OLD_VALUE>') ;

