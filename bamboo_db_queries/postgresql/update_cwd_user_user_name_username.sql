-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "cwd_user".*,user_name as user_name_before,'<NEW_VALUE>' as user_name_after from cwd_user where LOWER(user_name) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update cwd_user set user_name = '<NEW_VALUE>' where LOWER(user_name) = LOWER('<OLD_VALUE>') ;

