-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd user groups membership
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "cwd_membership".*,lower_parent_name as lower_parent_name_before,'<NEW_VALUE>' as lower_parent_name_after from cwd_membership where LOWER(lower_parent_name) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update cwd_membership set lower_parent_name = '<NEW_VALUE>' where LOWER(lower_parent_name) = LOWER('<OLD_VALUE>') ;

