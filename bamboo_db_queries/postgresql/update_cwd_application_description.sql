-- Type        : update
-- Origin      : bamboo
-- Description : User groups mapping
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "cwd_application".*,description as description_before,REGEXP_REPLACE(description,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as description_after from cwd_application where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update cwd_application set description = REGEXP_REPLACE(description,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

