-- Type        : update
-- Origin      : bamboo
-- Description : Build queue
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "queue".*,agent_description as agent_description_before,REGEXP_REPLACE(agent_description,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as agent_description_after from queue where LOWER(agent_description) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update queue set agent_description = REGEXP_REPLACE(agent_description,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(agent_description) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

