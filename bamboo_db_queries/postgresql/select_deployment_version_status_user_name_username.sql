-- Type        : select
-- Origin      : bamboo
-- Description : deployment version approver
-- Database    : postgresql

select "deployment_version_status".* from deployment_version_status where LOWER(user_name) = LOWER('<OLD_VALUE>') ;

