-- Type        : select
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : postgresql

select "artifact_definition".* from artifact_definition where LOWER(copy_pattern) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

