-- Type        : select
-- Origin      : bamboo
-- Description : Configuration of vcs repository
-- Database    : postgresql

select "vcs_location".* from vcs_location where LOWER(xml_definition_data) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

