-- Type        : update
-- Origin      : bamboo
-- Description : Value of variables in result stage
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "stage_variable_context".*,variable_value as variable_value_before,REGEXP_REPLACE(variable_value,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as variable_value_after from stage_variable_context where LOWER(variable_value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update stage_variable_context set variable_value = REGEXP_REPLACE(variable_value,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(variable_value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

