-- Type        : update
-- Origin      : bamboo
-- Description : user that has quarantined test case
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "test_case".*,quarantining_username as quarantining_username_before,'<NEW_VALUE>' as quarantining_username_after from test_case where LOWER(quarantining_username) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update test_case set quarantining_username = '<NEW_VALUE>' where LOWER(quarantining_username) = LOWER('<OLD_VALUE>') ;

