-- Type        : update
-- Origin      : bamboo
-- Description : Repository author
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "author".*,author_name as author_name_before,REGEXP_REPLACE(author_name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as author_name_after from author where LOWER(author_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update author set author_name = REGEXP_REPLACE(author_name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(author_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

