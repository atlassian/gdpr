-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : postgresql

select "deployment_environment".* from deployment_environment where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

