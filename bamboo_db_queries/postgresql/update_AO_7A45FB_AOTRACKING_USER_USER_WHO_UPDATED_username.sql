-- Type        : update
-- Origin      : bundled Bamboo plugin
-- Description : no description
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "AO_7A45FB_AOTRACKING_USER".*,"USER_WHO_UPDATED" as USER_WHO_UPDATED_before,'<NEW_VALUE>' as USER_WHO_UPDATED_after from "AO_7A45FB_AOTRACKING_USER" where LOWER("USER_WHO_UPDATED") = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update "AO_7A45FB_AOTRACKING_USER" set "USER_WHO_UPDATED" = '<NEW_VALUE>' where LOWER("USER_WHO_UPDATED") = LOWER('<OLD_VALUE>') ;

