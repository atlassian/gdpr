-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd tombstone
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "cwd_tombstone".*,parent as parent_before,'<NEW_VALUE>' as parent_after from cwd_tombstone where LOWER(parent) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update cwd_tombstone set parent = '<NEW_VALUE>' where LOWER(parent) = LOWER('<OLD_VALUE>') ;

