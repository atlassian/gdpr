-- Type        : select
-- Origin      : bamboo
-- Description : Plan stage definition
-- Database    : postgresql

select "chain_stage".* from chain_stage where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

