-- Type        : update
-- Origin      : bamboo
-- Description : author of user comments
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "user_comment".*,user_name as user_name_before,'<NEW_VALUE>' as user_name_after from user_comment where LOWER(user_name) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update user_comment set user_name = '<NEW_VALUE>' where LOWER(user_name) = LOWER('<OLD_VALUE>') ;

