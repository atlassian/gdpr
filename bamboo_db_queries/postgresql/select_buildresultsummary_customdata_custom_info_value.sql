-- Type        : select
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : postgresql

select "buildresultsummary_customdata".* from buildresultsummary_customdata where LOWER(custom_info_value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

