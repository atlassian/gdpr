-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : postgresql

select "variable_substitution".* from variable_substitution where LOWER(variable_value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

