-- Type        : update
-- Origin      : bamboo
-- Description : Repository commits comments - imported from vcs repository
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "user_commit".*,commit_comment_clob as commit_comment_clob_before,REGEXP_REPLACE(commit_comment_clob,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as commit_comment_clob_after from user_commit where LOWER(commit_comment_clob) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update user_commit set commit_comment_clob = REGEXP_REPLACE(commit_comment_clob,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(commit_comment_clob) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

