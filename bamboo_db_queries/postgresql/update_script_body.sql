-- Type        : update
-- Origin      : bamboo
-- Description : Scripts data, used eg. for elastic images startup
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "script".*,body as body_before,REGEXP_REPLACE(body,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as body_after from script where LOWER(body) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update script set body = REGEXP_REPLACE(body,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(body) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

