-- Type        : update
-- Origin      : bamboo
-- Description : Deployment variable substitution
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "deployment_variable_substitution".*,variable_value as variable_value_before,REGEXP_REPLACE(variable_value,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as variable_value_after from deployment_variable_substitution where LOWER(variable_value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update deployment_variable_substitution set variable_value = REGEXP_REPLACE(variable_value,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(variable_value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

