-- Type        : select
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : postgresql

select "chain_stage_result".* from chain_stage_result where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

