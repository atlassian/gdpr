-- Type        : select
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : postgresql

select "AO_A0B856_WEB_HOOK_LISTENER_AO".* from "AO_A0B856_WEB_HOOK_LISTENER_AO" where LOWER("NAME") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

