-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "audit_log".*,msg as msg_before,REGEXP_REPLACE(msg,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as msg_after from audit_log where LOWER(msg) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update audit_log set msg = REGEXP_REPLACE(msg,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(msg) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

