-- Type        : select
-- Origin      : bamboo
-- Description : Elastic image data
-- Database    : postgresql

select "elastic_image".* from elastic_image where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

