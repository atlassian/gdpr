-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd granted permissions
-- Database    : postgresql

select "cwd_granted_perm".* from cwd_granted_perm where LOWER(group_name) = LOWER('<OLD_VALUE>') ;

