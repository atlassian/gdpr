-- Type        : select
-- Origin      : bamboo
-- Description : Deployment result additional data
-- Database    : postgresql

select "deployment_result_customdata".* from deployment_result_customdata where LOWER(custom_info_value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

