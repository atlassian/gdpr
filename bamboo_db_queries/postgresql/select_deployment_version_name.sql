-- Type        : select
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : postgresql

select "deployment_version".* from deployment_version where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

