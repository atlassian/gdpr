-- Type        : select
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : postgresql

select "os_propertyentry".* from os_propertyentry where LOWER(string_val) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

