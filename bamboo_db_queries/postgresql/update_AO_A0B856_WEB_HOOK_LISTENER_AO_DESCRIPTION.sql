-- Type        : update
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "AO_A0B856_WEB_HOOK_LISTENER_AO".*,"DESCRIPTION" as DESCRIPTION_before,REGEXP_REPLACE("DESCRIPTION",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as DESCRIPTION_after from "AO_A0B856_WEB_HOOK_LISTENER_AO" where LOWER("DESCRIPTION") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update "AO_A0B856_WEB_HOOK_LISTENER_AO" set "DESCRIPTION" = REGEXP_REPLACE("DESCRIPTION",'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER("DESCRIPTION") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

