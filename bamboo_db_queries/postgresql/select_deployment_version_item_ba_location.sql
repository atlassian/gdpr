-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : postgresql

select "deployment_version_item_ba".* from deployment_version_item_ba where LOWER(location) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

