-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "audit_log".*,task_header as task_header_before,REGEXP_REPLACE(task_header,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as task_header_after from audit_log where LOWER(task_header) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update audit_log set task_header = REGEXP_REPLACE(task_header,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(task_header) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

