-- Type        : select
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : postgresql

select "deployment_version".* from deployment_version where LOWER(creator_username) = LOWER('<OLD_VALUE>') ;

