-- Type        : update
-- Origin      : bamboo
-- Description : Job requirements
-- Database    : postgresql

 
-- + SELECT (please review changes BEFORE)
select "requirement".*,match_value as match_value_before,REGEXP_REPLACE(match_value,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') as match_value_after from requirement where LOWER(match_value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- + UPDATE (be careful)
update requirement set match_value = REGEXP_REPLACE(match_value,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2','ig') where LOWER(match_value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

