-- Type        : select
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : oracle

select buildresultsummary_customdata.* from buildresultsummary_customdata where REGEXP_LIKE (LOWER(custom_info_value),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

