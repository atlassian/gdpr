-- Type        : update
-- Origin      : bamboo plugin
-- Description : ???
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select AO_C7F71E_OAUTH_SVC_PROV_TKNS.*,USER_NAME as USER_NAME_before,'<NEW_VALUE>' as USER_NAME_after from AO_C7F71E_OAUTH_SVC_PROV_TKNS where LOWER(USER_NAME) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update AO_C7F71E_OAUTH_SVC_PROV_TKNS set USER_NAME = '<NEW_VALUE>' where LOWER(USER_NAME) = LOWER('<OLD_VALUE>') ;

