-- Type        : select
-- Origin      : bamboo
-- Description : Deployment project items definition
-- Database    : oracle

select deployment_project_item.* from deployment_project_item where REGEXP_LIKE (LOWER(name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

