-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select cwd_user.*,last_name as last_name_before,'<NEW_VALUE>' as last_name_after from cwd_user where LOWER(last_name) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update cwd_user set last_name = '<NEW_VALUE>' where LOWER(last_name) = LOWER('<OLD_VALUE>') ;

