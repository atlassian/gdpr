-- Type        : select
-- Origin      : bamboo
-- Description : Deployment variable substitution
-- Database    : oracle

select deployment_variable_substitution.* from deployment_variable_substitution where REGEXP_LIKE (LOWER(variable_key),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

