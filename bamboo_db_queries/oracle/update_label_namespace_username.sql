-- Type        : update
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select label.*,namespace as namespace_before,'<NEW_VALUE>' as namespace_after from label where LOWER(namespace) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update label set namespace = '<NEW_VALUE>' where LOWER(namespace) = LOWER('<OLD_VALUE>') ;

