-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in result stage
-- Database    : oracle

select stage_variable_context.* from stage_variable_context where REGEXP_LIKE (LOWER(variable_key),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

