-- Type        : update
-- Origin      : bamboo
-- Description : External users name
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select external_entities.*,name as name_before,'<NEW_VALUE>' as name_after from external_entities where LOWER(name) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update external_entities set name = '<NEW_VALUE>' where LOWER(name) = LOWER('<OLD_VALUE>') ;

