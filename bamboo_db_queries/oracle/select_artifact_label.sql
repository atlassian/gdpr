-- Type        : select
-- Origin      : bamboo
-- Description : Artefact data
-- Database    : oracle

select artifact.* from artifact where REGEXP_LIKE (LOWER(label),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

