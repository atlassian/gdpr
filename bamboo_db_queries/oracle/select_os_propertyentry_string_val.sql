-- Type        : select
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : oracle

select os_propertyentry.* from os_propertyentry where REGEXP_LIKE (LOWER(string_val),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

