-- Type        : update
-- Origin      : bamboo
-- Description : Merge result
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select plan_vcs_history.*,xml_custom_data as xml_custom_data_before,REGEXP_REPLACE(xml_custom_data,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as xml_custom_data_after from plan_vcs_history where REGEXP_LIKE (LOWER(xml_custom_data),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update plan_vcs_history set xml_custom_data = REGEXP_REPLACE(xml_custom_data,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(xml_custom_data),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

