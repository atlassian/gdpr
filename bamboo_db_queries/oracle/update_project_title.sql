-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo project
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select project.*,title as title_before,REGEXP_REPLACE(title,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as title_after from project where REGEXP_LIKE (LOWER(title),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update project set title = REGEXP_REPLACE(title,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(title),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

