-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select cwd_user.*,lower_display_name as lower_display_name_before,'<NEW_VALUE>' as lower_display_name_after from cwd_user where LOWER(lower_display_name) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update cwd_user set lower_display_name = '<NEW_VALUE>' where LOWER(lower_display_name) = LOWER('<OLD_VALUE>') ;

