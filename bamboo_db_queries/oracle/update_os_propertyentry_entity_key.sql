-- Type        : update
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select os_propertyentry.*,entity_key as entity_key_before,REGEXP_REPLACE(entity_key,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as entity_key_after from os_propertyentry where REGEXP_LIKE (LOWER(entity_key),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update os_propertyentry set entity_key = REGEXP_REPLACE(entity_key,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(entity_key),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

