-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd groups
-- Database    : oracle

select cwd_group.* from cwd_group where REGEXP_LIKE (LOWER(description),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

