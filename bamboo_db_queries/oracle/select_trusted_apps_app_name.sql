-- Type        : select
-- Origin      : bamboo
-- Description : Name of the trusted app
-- Database    : oracle

select trusted_apps.* from trusted_apps where REGEXP_LIKE (LOWER(app_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

