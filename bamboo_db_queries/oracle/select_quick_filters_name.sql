-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : oracle

select quick_filters.* from quick_filters where REGEXP_LIKE (LOWER(name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

