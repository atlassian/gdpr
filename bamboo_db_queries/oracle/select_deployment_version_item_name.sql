-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version item
-- Database    : oracle

select deployment_version_item.* from deployment_version_item where REGEXP_LIKE (LOWER(name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

