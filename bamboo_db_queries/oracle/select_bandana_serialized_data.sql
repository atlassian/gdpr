-- Type        : select
-- Origin      : bamboo
-- Description : set of various values used by system; may contain username
-- Database    : oracle

select bandana.* from bandana where REGEXP_LIKE (LOWER(serialized_data),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

