-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select cwd_application_alias.*,alias_name as alias_name_before,REGEXP_REPLACE(alias_name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as alias_name_after from cwd_application_alias where REGEXP_LIKE (LOWER(alias_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update cwd_application_alias set alias_name = REGEXP_REPLACE(alias_name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(alias_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

