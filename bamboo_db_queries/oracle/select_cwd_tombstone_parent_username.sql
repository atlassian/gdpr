-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd tombstone
-- Database    : oracle

select cwd_tombstone.* from cwd_tombstone where LOWER(parent) = LOWER('<OLD_VALUE>') ;

