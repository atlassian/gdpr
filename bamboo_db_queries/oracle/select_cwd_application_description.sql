-- Type        : select
-- Origin      : bamboo
-- Description : User groups mapping
-- Database    : oracle

select cwd_application.* from cwd_application where REGEXP_LIKE (LOWER(description),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

