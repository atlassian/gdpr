-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo notifications send to user or other recipients
-- Database    : oracle

select notifications.* from notifications where (REGEXP_LIKE (LOWER(recipient),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)')) AND recipient_type  <> 'com.atlassian.bamboo.plugin.system.notifications:recipient.user';

