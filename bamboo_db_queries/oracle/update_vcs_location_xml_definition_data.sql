-- Type        : update
-- Origin      : bamboo
-- Description : Configuration of vcs repository
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select vcs_location.*,xml_definition_data as xml_definition_data_before,REGEXP_REPLACE(xml_definition_data,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as xml_definition_data_after from vcs_location where REGEXP_LIKE (LOWER(xml_definition_data),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update vcs_location set xml_definition_data = REGEXP_REPLACE(xml_definition_data,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(xml_definition_data),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

