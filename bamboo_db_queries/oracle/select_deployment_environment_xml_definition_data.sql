-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : oracle

select deployment_environment.* from deployment_environment where REGEXP_LIKE (LOWER(xml_definition_data),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

