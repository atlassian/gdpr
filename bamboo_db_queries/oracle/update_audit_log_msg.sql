-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select audit_log.*,msg as msg_before,REGEXP_REPLACE(msg,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as msg_after from audit_log where REGEXP_LIKE (LOWER(msg),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update audit_log set msg = REGEXP_REPLACE(msg,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(msg),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

