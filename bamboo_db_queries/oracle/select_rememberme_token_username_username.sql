-- Type        : select
-- Origin      : bamboo
-- Description : used to auto-login user based on cookie
-- Database    : oracle

select rememberme_token.* from rememberme_token where LOWER(username) = LOWER('<OLD_VALUE>') ;

