-- Type        : select
-- Origin      : bamboo
-- Description : Job configuration
-- Database    : oracle

select build_definition.* from build_definition where REGEXP_LIKE (LOWER(xml_definition_data),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

