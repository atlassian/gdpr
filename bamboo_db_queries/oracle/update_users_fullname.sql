-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo internal users directory
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select users.*,fullname as fullname_before,REGEXP_REPLACE(fullname,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as fullname_after from users where REGEXP_LIKE (LOWER(fullname),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update users set fullname = REGEXP_REPLACE(fullname,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(fullname),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

