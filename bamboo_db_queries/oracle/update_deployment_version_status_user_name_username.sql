-- Type        : update
-- Origin      : bamboo
-- Description : deployment version approver
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select deployment_version_status.*,user_name as user_name_before,'<NEW_VALUE>' as user_name_after from deployment_version_status where LOWER(user_name) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update deployment_version_status set user_name = '<NEW_VALUE>' where LOWER(user_name) = LOWER('<OLD_VALUE>') ;

