-- Type        : select
-- Origin      : bamboo
-- Description : Plan stage definition
-- Database    : oracle

select chain_stage.* from chain_stage where REGEXP_LIKE (LOWER(name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

