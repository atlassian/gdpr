-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version commit
-- Database    : oracle

select deployment_version_commit.* from deployment_version_commit where REGEXP_LIKE (LOWER(commit_comment_clob),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

