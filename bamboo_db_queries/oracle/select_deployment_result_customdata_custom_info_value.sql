-- Type        : select
-- Origin      : bamboo
-- Description : Deployment result additional data
-- Database    : oracle

select deployment_result_customdata.* from deployment_result_customdata where REGEXP_LIKE (LOWER(custom_info_value),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

