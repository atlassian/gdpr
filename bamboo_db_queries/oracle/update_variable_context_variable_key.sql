-- Type        : update
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select variable_context.*,variable_key as variable_key_before,REGEXP_REPLACE(variable_key,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as variable_key_after from variable_context where REGEXP_LIKE (LOWER(variable_key),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update variable_context set variable_key = REGEXP_REPLACE(variable_key,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(variable_key),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

