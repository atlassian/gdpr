-- Type        : update
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select variable_substitution.*,variable_value as variable_value_before,REGEXP_REPLACE(variable_value,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as variable_value_after from variable_substitution where REGEXP_LIKE (LOWER(variable_value),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update variable_substitution set variable_value = REGEXP_REPLACE(variable_value,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(variable_value),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

