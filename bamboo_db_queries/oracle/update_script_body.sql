-- Type        : update
-- Origin      : bamboo
-- Description : Scripts data, used eg. for elastic images startup
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select script.*,body as body_before,REGEXP_REPLACE(body,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as body_after from script where REGEXP_LIKE (LOWER(body),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update script set body = REGEXP_REPLACE(body,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(body),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

