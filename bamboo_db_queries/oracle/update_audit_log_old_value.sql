-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select audit_log.*,old_value as old_value_before,REGEXP_REPLACE(old_value,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as old_value_after from audit_log where REGEXP_LIKE (LOWER(old_value),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update audit_log set old_value = REGEXP_REPLACE(old_value,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(old_value),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

