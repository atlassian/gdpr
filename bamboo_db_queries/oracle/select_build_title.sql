-- Type        : select
-- Origin      : bamboo
-- Description : Plan / Job data
-- Database    : oracle

select build.* from build where REGEXP_LIKE (LOWER(title),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

