-- Type        : select
-- Origin      : bamboo
-- Description : Job requirements
-- Database    : oracle

select requirement.* from requirement where REGEXP_LIKE (LOWER(key_identifier),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

