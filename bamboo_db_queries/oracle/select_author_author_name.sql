-- Type        : select
-- Origin      : bamboo
-- Description : Repository author
-- Database    : oracle

select author.* from author where REGEXP_LIKE (LOWER(author_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

