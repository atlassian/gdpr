-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : oracle

select variable_context.* from variable_context where REGEXP_LIKE (LOWER(variable_value),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

