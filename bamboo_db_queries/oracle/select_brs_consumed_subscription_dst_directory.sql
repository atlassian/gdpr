-- Type        : select
-- Origin      : bamboo
-- Description : Directory pattern of result artifacts subscription
-- Database    : oracle

select brs_consumed_subscription.* from brs_consumed_subscription where REGEXP_LIKE (LOWER(dst_directory),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

