-- Type        : select
-- Origin      : bamboo
-- Description : Merge result
-- Database    : oracle

select plan_vcs_history.* from plan_vcs_history where REGEXP_LIKE (LOWER(xml_custom_data),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

