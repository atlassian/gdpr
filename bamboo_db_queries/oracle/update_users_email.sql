-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo internal users directory
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select users.*,email as email_before,'<NEW_VALUE>' as email_after from users where LOWER(email) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update users set email = '<NEW_VALUE>' where LOWER(email) = LOWER('<OLD_VALUE>') ;

