-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd granted permissions
-- Database    : oracle

select cwd_granted_perm.* from cwd_granted_perm where LOWER(group_name) = LOWER('<OLD_VALUE>') ;

