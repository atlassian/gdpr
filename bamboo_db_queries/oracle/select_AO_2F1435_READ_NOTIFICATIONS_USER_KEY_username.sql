-- Type        : select
-- Origin      : support healthcheck plugin
-- Description : record of dismissed messages
-- Database    : oracle

select AO_2F1435_READ_NOTIFICATIONS.* from AO_2F1435_READ_NOTIFICATIONS where LOWER(USER_KEY) = LOWER('<OLD_VALUE>') ;

