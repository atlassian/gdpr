-- Type        : select
-- Origin      : bamboo
-- Description : Job requirements
-- Database    : oracle

select requirement.* from requirement where REGEXP_LIKE (LOWER(match_value),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

