-- Type        : update
-- Origin      : bamboo
-- Description : Repository author
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select author.*,linked_user_name as linked_user_name_before,'<NEW_VALUE>' as linked_user_name_after from author where LOWER(linked_user_name) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update author set linked_user_name = '<NEW_VALUE>' where LOWER(linked_user_name) = LOWER('<OLD_VALUE>') ;

