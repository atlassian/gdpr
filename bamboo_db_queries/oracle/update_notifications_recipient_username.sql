-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo notifications send to user or other recipients
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select notifications.*,recipient as recipient_before,'<NEW_VALUE>' as recipient_after from notifications where (LOWER(recipient) = LOWER('<OLD_VALUE>') ) AND recipient_type  = 'com.atlassian.bamboo.plugin.system.notifications:recipient.user';

-- + UPDATE (be careful)
update notifications set recipient = '<NEW_VALUE>' where (LOWER(recipient) = LOWER('<OLD_VALUE>') ) AND recipient_type  = 'com.atlassian.bamboo.plugin.system.notifications:recipient.user';

