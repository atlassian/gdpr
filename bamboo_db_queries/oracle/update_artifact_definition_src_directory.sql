-- Type        : update
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select artifact_definition.*,src_directory as src_directory_before,REGEXP_REPLACE(src_directory,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as src_directory_after from artifact_definition where REGEXP_LIKE (LOWER(src_directory),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update artifact_definition set src_directory = REGEXP_REPLACE(src_directory,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(src_directory),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

