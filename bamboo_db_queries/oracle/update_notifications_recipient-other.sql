-- Type        : update
-- Origin      : bamboo
-- Description : Bamboo notifications send to user or other recipients
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select notifications.*,recipient as recipient_before,REGEXP_REPLACE(recipient,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as recipient_after from notifications where (REGEXP_LIKE (LOWER(recipient),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)')) AND recipient_type  <> 'com.atlassian.bamboo.plugin.system.notifications:recipient.user';

-- + UPDATE (be careful)
update notifications set recipient = REGEXP_REPLACE(recipient,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where (REGEXP_LIKE (LOWER(recipient),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)')) AND recipient_type  <> 'com.atlassian.bamboo.plugin.system.notifications:recipient.user';

