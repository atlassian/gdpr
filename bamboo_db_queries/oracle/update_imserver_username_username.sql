-- Type        : update
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select imserver.*,username as username_before,'<NEW_VALUE>' as username_after from imserver where LOWER(username) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update imserver set username = '<NEW_VALUE>' where LOWER(username) = LOWER('<OLD_VALUE>') ;

