-- Type        : select
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : oracle

select groups.* from groups where REGEXP_LIKE (LOWER(groupname),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

