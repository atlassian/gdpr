-- Type        : update
-- Origin      : bamboo
-- Description : Build queue
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select quick_filter_rules.*,name as name_before,REGEXP_REPLACE(name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as name_after from quick_filter_rules where REGEXP_LIKE (LOWER(name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update quick_filter_rules set name = REGEXP_REPLACE(name,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

