-- Type        : update
-- Origin      : bamboo
-- Description : Default group name
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select cwd_app_dir_default_groups.*,group_name as group_name_before,'<NEW_VALUE>' as group_name_after from cwd_app_dir_default_groups where LOWER(group_name) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update cwd_app_dir_default_groups set group_name = '<NEW_VALUE>' where LOWER(group_name) = LOWER('<OLD_VALUE>') ;

