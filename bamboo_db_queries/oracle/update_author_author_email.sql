-- Type        : update
-- Origin      : bamboo
-- Description : Repository author
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select author.*,author_email as author_email_before,'<NEW_VALUE>' as author_email_after from author where LOWER(author_email) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update author set author_email = '<NEW_VALUE>' where LOWER(author_email) = LOWER('<OLD_VALUE>') ;

