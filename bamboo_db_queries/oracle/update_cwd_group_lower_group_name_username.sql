-- Type        : update
-- Origin      : bamboo
-- Description : Embedded crowd groups
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select cwd_group.*,lower_group_name as lower_group_name_before,'<NEW_VALUE>' as lower_group_name_after from cwd_group where LOWER(lower_group_name) = LOWER('<OLD_VALUE>') ;

-- + UPDATE (be careful)
update cwd_group set lower_group_name = '<NEW_VALUE>' where LOWER(lower_group_name) = LOWER('<OLD_VALUE>') ;

