-- Type        : update
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : oracle

 
-- + SELECT (please review changes BEFORE)
select artifact_definition.*,copy_pattern as copy_pattern_before,REGEXP_REPLACE(copy_pattern,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') as copy_pattern_after from artifact_definition where REGEXP_LIKE (LOWER(copy_pattern),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- + UPDATE (be careful)
update artifact_definition set copy_pattern = REGEXP_REPLACE(copy_pattern,'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_VALUE>\2') where REGEXP_LIKE (LOWER(copy_pattern),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

