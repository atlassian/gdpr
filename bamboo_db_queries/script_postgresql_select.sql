-- Type        : select
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : postgresql

select "AO_A0B856_WEB_HOOK_LISTENER_AO".* from "AO_A0B856_WEB_HOOK_LISTENER_AO" where LOWER("DESCRIPTION") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : postgresql

select "AO_A0B856_WEB_HOOK_LISTENER_AO".* from "AO_A0B856_WEB_HOOK_LISTENER_AO" where LOWER("NAME") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : postgresql

select "AO_A0B856_WEB_HOOK_LISTENER_AO".* from "AO_A0B856_WEB_HOOK_LISTENER_AO" where LOWER("URL") like LOWER('%<OLD_VALUE>%') ;

-- Type        : select
-- Origin      : bamboo plugin
-- Description : ???
-- Database    : postgresql

select "AO_C7F71E_OAUTH_SVC_PROV_TKNS".* from "AO_C7F71E_OAUTH_SVC_PROV_TKNS" where LOWER("VALUE") ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : postgresql

select "artifact_definition".* from artifact_definition where LOWER(copy_pattern) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : postgresql

select "artifact_definition".* from artifact_definition where LOWER(label) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : postgresql

select "artifact_definition".* from artifact_definition where LOWER(src_directory) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Artefact data
-- Database    : postgresql

select "artifact".* from artifact where LOWER(label) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Artifact subscription
-- Database    : postgresql

select "artifact_subscription".* from artifact_subscription where LOWER(dst_directory) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : postgresql

select "audit_log".* from audit_log where LOWER(msg) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : postgresql

select "audit_log".* from audit_log where LOWER(new_value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : postgresql

select "audit_log".* from audit_log where LOWER(old_value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : postgresql

select "audit_log".* from audit_log where LOWER(task_header) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Repository author
-- Database    : postgresql

select "author".* from author where LOWER(author_email) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Repository author
-- Database    : postgresql

select "author".* from author where LOWER(author_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : set of various values used by system; may contain username
-- Database    : postgresql

select "bandana".* from bandana where LOWER(serialized_data) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Directory pattern of result artifacts subscription
-- Database    : postgresql

select "brs_consumed_subscription".* from brs_consumed_subscription where LOWER(dst_directory) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Job configuration
-- Database    : postgresql

select "build_definition".* from build_definition where LOWER(xml_definition_data) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Plan / Job data
-- Database    : postgresql

select "build".* from build where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Plan / Job data
-- Database    : postgresql

select "build".* from build where LOWER(title) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : postgresql

select "buildresultsummary_customdata".* from buildresultsummary_customdata where LOWER(custom_info_value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Agent capabilities
-- Database    : postgresql

select "capability".* from capability where LOWER(value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Plan stage definition
-- Database    : postgresql

select "chain_stage".* from chain_stage where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Plan stage definition
-- Database    : postgresql

select "chain_stage".* from chain_stage where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : postgresql

select "chain_stage_result".* from chain_stage_result where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : postgresql

select "chain_stage_result".* from chain_stage_result where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo shared credentials
-- Database    : postgresql

select "credentials".* from credentials where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo shared credentials
-- Database    : postgresql

select "credentials".* from credentials where LOWER(xml) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : postgresql

select "cwd_application_alias".* from cwd_application_alias where LOWER(alias_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : postgresql

select "cwd_application_alias".* from cwd_application_alias where LOWER(lower_alias_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : User groups mapping
-- Database    : postgresql

select "cwd_application".* from cwd_application where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd directories
-- Database    : postgresql

select "cwd_directory".* from cwd_directory where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd user tokens
-- Database    : postgresql

select "cwd_expirable_user_token".* from cwd_expirable_user_token where LOWER(email_address) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd groups
-- Database    : postgresql

select "cwd_group".* from cwd_group where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : postgresql

select "cwd_user".* from cwd_user where LOWER(email_address) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : postgresql

select "cwd_user".* from cwd_user where LOWER(lower_email_address) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment configuration
-- Database    : postgresql

select "deployment_env_config".* from deployment_env_config where LOWER(docker_pipeline_config) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment configuration
-- Database    : postgresql

select "deployment_env_config".* from deployment_env_config where LOWER(plugin_config) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : postgresql

select "deployment_environment".* from deployment_environment where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : postgresql

select "deployment_environment".* from deployment_environment where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : postgresql

select "deployment_environment".* from deployment_environment where LOWER(triggers_xml_data) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : postgresql

select "deployment_environment".* from deployment_environment where LOWER(xml_definition_data) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment project definition
-- Database    : postgresql

select "deployment_project".* from deployment_project where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment project items definition
-- Database    : postgresql

select "deployment_project_item".* from deployment_project_item where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment project definition
-- Database    : postgresql

select "deployment_project".* from deployment_project where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment result additional data
-- Database    : postgresql

select "deployment_result_customdata".* from deployment_result_customdata where LOWER(custom_info_value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment result
-- Database    : postgresql

select "deployment_result".* from deployment_result where LOWER(version_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment variable substitution
-- Database    : postgresql

select "deployment_variable_substitution".* from deployment_variable_substitution where LOWER(variable_key) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment variable substitution
-- Database    : postgresql

select "deployment_variable_substitution".* from deployment_variable_substitution where LOWER(variable_value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version commit
-- Database    : postgresql

select "deployment_version_commit".* from deployment_version_commit where LOWER(commit_comment_clob) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : postgresql

select "deployment_version_item_ba".* from deployment_version_item_ba where LOWER(copy_pattern) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : postgresql

select "deployment_version_item_ba".* from deployment_version_item_ba where LOWER(label) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : postgresql

select "deployment_version_item_ba".* from deployment_version_item_ba where LOWER(location) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version item
-- Database    : postgresql

select "deployment_version_item".* from deployment_version_item where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : postgresql

select "deployment_version".* from deployment_version where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version naming
-- Database    : postgresql

select "deployment_version_naming".* from deployment_version_naming where LOWER(next_version_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : postgresql

select "deployment_version".* from deployment_version where LOWER(plan_branch_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Elastic image data
-- Database    : postgresql

select "elastic_image".* from elastic_image where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Elastic image data
-- Database    : postgresql

select "elastic_image".* from elastic_image where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : postgresql

select "groups".* from groups where LOWER(groupname) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : postgresql

select "imserver".* from imserver where LOWER(host) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : postgresql

select "imserver".* from imserver where LOWER(resource_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : postgresql

select "imserver".* from imserver where LOWER(title) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : postgresql

select "label".* from label where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Merge result
-- Database    : postgresql

select "merge_result".* from merge_result where LOWER(failure_reason) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo notifications send to user or other recipients
-- Database    : postgresql

select "notifications".* from notifications where (LOWER(recipient) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)') AND recipient_type  <> 'com.atlassian.bamboo.plugin.system.notifications:recipient.user';

-- Type        : select
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : postgresql

select "os_propertyentry".* from os_propertyentry where LOWER(entity_key) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : postgresql

select "os_propertyentry".* from os_propertyentry where LOWER(entity_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : postgresql

select "os_propertyentry".* from os_propertyentry where LOWER(string_val) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Merge result
-- Database    : postgresql

select "plan_vcs_history".* from plan_vcs_history where LOWER(xml_custom_data) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo project
-- Database    : postgresql

select "project".* from project where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo project
-- Database    : postgresql

select "project".* from project where LOWER(title) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : postgresql

select "queue".* from queue where LOWER(agent_description) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : postgresql

select "queue".* from queue where LOWER(title) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : postgresql

select "quick_filter_rules".* from quick_filter_rules where LOWER(configuration) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : postgresql

select "quick_filter_rules".* from quick_filter_rules where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : postgresql

select "quick_filters".* from quick_filters where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Job requirements
-- Database    : postgresql

select "requirement".* from requirement where LOWER(key_identifier) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Job requirements
-- Database    : postgresql

select "requirement".* from requirement where LOWER(match_value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Scripts data, used eg. for elastic images startup
-- Database    : postgresql

select "script".* from script where LOWER(body) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in result stage
-- Database    : postgresql

select "stage_variable_context".* from stage_variable_context where LOWER(variable_key) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in result stage
-- Database    : postgresql

select "stage_variable_context".* from stage_variable_context where LOWER(variable_value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Test error content
-- Database    : postgresql

select "test_error".* from test_error where LOWER(error_content) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Name of the trusted app
-- Database    : postgresql

select "trusted_apps".* from trusted_apps where LOWER(app_name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : IP address of the trusted app
-- Database    : postgresql

select "trusted_apps_ips".* from trusted_apps_ips where LOWER(ip_pattern) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : URL of the trusted app
-- Database    : postgresql

select "trusted_apps_urls".* from trusted_apps_urls where LOWER(url_pattern) like LOWER('%<OLD_VALUE>%') ;

-- Type        : select
-- Origin      : bamboo
-- Description : author of user comments
-- Database    : postgresql

select "user_comment".* from user_comment where LOWER(content) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Repository commits comments - imported from vcs repository
-- Database    : postgresql

select "user_commit".* from user_commit where LOWER(commit_comment_clob) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo internal users directory
-- Database    : postgresql

select "users".* from users where LOWER(email) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo internal users directory
-- Database    : postgresql

select "users".* from users where LOWER(fullname) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : postgresql

select "variable_context".* from variable_context where LOWER(variable_key) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : postgresql

select "variable_context".* from variable_context where LOWER(variable_value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : postgresql

select "variable_substitution".* from variable_substitution where LOWER(variable_key) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : postgresql

select "variable_substitution".* from variable_substitution where LOWER(variable_value) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Configuration of vcs repository
-- Database    : postgresql

select "vcs_location".* from vcs_location where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : bamboo
-- Description : Configuration of vcs repository
-- Database    : postgresql

select "vcs_location".* from vcs_location where LOWER(xml_definition_data) ~* '(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)';

