#!/usr/bin/env bash

if [ $# != 1 ];
then
    printf "\nUsage: ${0} [mssql|mysql|mssql|postgresql]\n\n"
    exit 1
fi

database=${1}

pattern="username.sql"
select="select"
update="update"


cat ${database}/${select}*${pattern} > script_${database}_select_${pattern}
cat ${database}/${update}*${pattern} > script_${database}_update_${pattern}

GLOBIGNORE="*${pattern}"
cat ${database}/${select}* > script_${database}_select.sql
cat ${database}/${update}* > script_${database}_update.sql
GLOBIGNORE=""

if [ -d "${database}_manual" ];
then
    GLOBIGNORE="*${pattern}"
    cat "${database}_manual"/${select}* >> script_${database}_select.sql
    cat "${database}_manual"/${update}* >> script_${database}_update.sql
    GLOBIGNORE=""
fi
