-- Type        : select
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : oracle

select AO_A0B856_WEB_HOOK_LISTENER_AO.* from AO_A0B856_WEB_HOOK_LISTENER_AO where REGEXP_LIKE (LOWER(DESCRIPTION),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : oracle

select AO_A0B856_WEB_HOOK_LISTENER_AO.* from AO_A0B856_WEB_HOOK_LISTENER_AO where REGEXP_LIKE (LOWER(NAME),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : oracle

select AO_A0B856_WEB_HOOK_LISTENER_AO.* from AO_A0B856_WEB_HOOK_LISTENER_AO where LOWER(URL) like LOWER('%<OLD_VALUE>%') ;

-- Type        : select
-- Origin      : bamboo plugin
-- Description : ???
-- Database    : oracle

select AO_C7F71E_OAUTH_SVC_PROV_TKNS.* from AO_C7F71E_OAUTH_SVC_PROV_TKNS where REGEXP_LIKE (LOWER(VALUE),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : oracle

select artifact_definition.* from artifact_definition where REGEXP_LIKE (LOWER(copy_pattern),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : oracle

select artifact_definition.* from artifact_definition where REGEXP_LIKE (LOWER(label),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Artifact definition
-- Database    : oracle

select artifact_definition.* from artifact_definition where REGEXP_LIKE (LOWER(src_directory),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Artefact data
-- Database    : oracle

select artifact.* from artifact where REGEXP_LIKE (LOWER(label),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Artifact subscription
-- Database    : oracle

select artifact_subscription.* from artifact_subscription where REGEXP_LIKE (LOWER(dst_directory),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : oracle

select audit_log.* from audit_log where REGEXP_LIKE (LOWER(msg),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : oracle

select audit_log.* from audit_log where REGEXP_LIKE (LOWER(new_value),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : oracle

select audit_log.* from audit_log where REGEXP_LIKE (LOWER(old_value),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo audit log
-- Database    : oracle

select audit_log.* from audit_log where REGEXP_LIKE (LOWER(task_header),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Repository author
-- Database    : oracle

select author.* from author where LOWER(author_email) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Repository author
-- Database    : oracle

select author.* from author where REGEXP_LIKE (LOWER(author_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : set of various values used by system; may contain username
-- Database    : oracle

select bandana.* from bandana where REGEXP_LIKE (LOWER(serialized_data),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Directory pattern of result artifacts subscription
-- Database    : oracle

select brs_consumed_subscription.* from brs_consumed_subscription where REGEXP_LIKE (LOWER(dst_directory),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Job configuration
-- Database    : oracle

select build_definition.* from build_definition where REGEXP_LIKE (LOWER(xml_definition_data),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Plan / Job data
-- Database    : oracle

select build.* from build where REGEXP_LIKE (LOWER(description),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Plan / Job data
-- Database    : oracle

select build.* from build where REGEXP_LIKE (LOWER(title),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : oracle

select buildresultsummary_customdata.* from buildresultsummary_customdata where REGEXP_LIKE (LOWER(custom_info_value),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Agent capabilities
-- Database    : oracle

select capability.* from capability where REGEXP_LIKE (LOWER(value),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Plan stage definition
-- Database    : oracle

select chain_stage.* from chain_stage where REGEXP_LIKE (LOWER(description),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Plan stage definition
-- Database    : oracle

select chain_stage.* from chain_stage where REGEXP_LIKE (LOWER(name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : oracle

select chain_stage_result.* from chain_stage_result where REGEXP_LIKE (LOWER(description),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Result custom data
-- Database    : oracle

select chain_stage_result.* from chain_stage_result where REGEXP_LIKE (LOWER(name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo shared credentials
-- Database    : oracle

select credentials.* from credentials where REGEXP_LIKE (LOWER(name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo shared credentials
-- Database    : oracle

select credentials.* from credentials where REGEXP_LIKE (LOWER(xml),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : oracle

select cwd_application_alias.* from cwd_application_alias where REGEXP_LIKE (LOWER(alias_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd application aliases
-- Database    : oracle

select cwd_application_alias.* from cwd_application_alias where REGEXP_LIKE (LOWER(lower_alias_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : User groups mapping
-- Database    : oracle

select cwd_application.* from cwd_application where REGEXP_LIKE (LOWER(description),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd directories
-- Database    : oracle

select cwd_directory.* from cwd_directory where REGEXP_LIKE (LOWER(description),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd user tokens
-- Database    : oracle

select cwd_expirable_user_token.* from cwd_expirable_user_token where LOWER(email_address) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd groups
-- Database    : oracle

select cwd_group.* from cwd_group where REGEXP_LIKE (LOWER(description),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : oracle

select cwd_user.* from cwd_user where LOWER(email_address) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Embedded crowd users data
-- Database    : oracle

select cwd_user.* from cwd_user where LOWER(lower_email_address) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment configuration
-- Database    : oracle

select deployment_env_config.* from deployment_env_config where REGEXP_LIKE (LOWER(docker_pipeline_config),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment configuration
-- Database    : oracle

select deployment_env_config.* from deployment_env_config where REGEXP_LIKE (LOWER(plugin_config),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : oracle

select deployment_environment.* from deployment_environment where REGEXP_LIKE (LOWER(description),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : oracle

select deployment_environment.* from deployment_environment where REGEXP_LIKE (LOWER(name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : oracle

select deployment_environment.* from deployment_environment where REGEXP_LIKE (LOWER(triggers_xml_data),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment environment definition
-- Database    : oracle

select deployment_environment.* from deployment_environment where REGEXP_LIKE (LOWER(xml_definition_data),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment project definition
-- Database    : oracle

select deployment_project.* from deployment_project where REGEXP_LIKE (LOWER(description),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment project items definition
-- Database    : oracle

select deployment_project_item.* from deployment_project_item where REGEXP_LIKE (LOWER(name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment project definition
-- Database    : oracle

select deployment_project.* from deployment_project where REGEXP_LIKE (LOWER(name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment result additional data
-- Database    : oracle

select deployment_result_customdata.* from deployment_result_customdata where REGEXP_LIKE (LOWER(custom_info_value),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment result
-- Database    : oracle

select deployment_result.* from deployment_result where REGEXP_LIKE (LOWER(version_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment variable substitution
-- Database    : oracle

select deployment_variable_substitution.* from deployment_variable_substitution where REGEXP_LIKE (LOWER(variable_key),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment variable substitution
-- Database    : oracle

select deployment_variable_substitution.* from deployment_variable_substitution where REGEXP_LIKE (LOWER(variable_value),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version commit
-- Database    : oracle

select deployment_version_commit.* from deployment_version_commit where REGEXP_LIKE (LOWER(commit_comment_clob),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : oracle

select deployment_version_item_ba.* from deployment_version_item_ba where REGEXP_LIKE (LOWER(copy_pattern),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : oracle

select deployment_version_item_ba.* from deployment_version_item_ba where REGEXP_LIKE (LOWER(label),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version artifact data
-- Database    : oracle

select deployment_version_item_ba.* from deployment_version_item_ba where REGEXP_LIKE (LOWER(location),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version item
-- Database    : oracle

select deployment_version_item.* from deployment_version_item where REGEXP_LIKE (LOWER(name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : oracle

select deployment_version.* from deployment_version where REGEXP_LIKE (LOWER(name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Deployment version naming
-- Database    : oracle

select deployment_version_naming.* from deployment_version_naming where REGEXP_LIKE (LOWER(next_version_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : deployment version definition
-- Database    : oracle

select deployment_version.* from deployment_version where REGEXP_LIKE (LOWER(plan_branch_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Elastic image data
-- Database    : oracle

select elastic_image.* from elastic_image where REGEXP_LIKE (LOWER(description),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Elastic image data
-- Database    : oracle

select elastic_image.* from elastic_image where REGEXP_LIKE (LOWER(name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : oracle

select groups.* from groups where REGEXP_LIKE (LOWER(groupname),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : oracle

select imserver.* from imserver where REGEXP_LIKE (LOWER(host),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : oracle

select imserver.* from imserver where REGEXP_LIKE (LOWER(resource_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : IM servers data
-- Database    : oracle

select imserver.* from imserver where REGEXP_LIKE (LOWER(title),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Internal bamboo user groups
-- Database    : oracle

select label.* from label where REGEXP_LIKE (LOWER(name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Merge result
-- Database    : oracle

select merge_result.* from merge_result where REGEXP_LIKE (LOWER(failure_reason),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo notifications send to user or other recipients
-- Database    : oracle

select notifications.* from notifications where (REGEXP_LIKE (LOWER(recipient),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)')) AND recipient_type  <> 'com.atlassian.bamboo.plugin.system.notifications:recipient.user';

-- Type        : select
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : oracle

select os_propertyentry.* from os_propertyentry where REGEXP_LIKE (LOWER(entity_key),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : oracle

select os_propertyentry.* from os_propertyentry where REGEXP_LIKE (LOWER(entity_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : External user (IM)
-- Database    : oracle

select os_propertyentry.* from os_propertyentry where REGEXP_LIKE (LOWER(string_val),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Merge result
-- Database    : oracle

select plan_vcs_history.* from plan_vcs_history where REGEXP_LIKE (LOWER(xml_custom_data),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo project
-- Database    : oracle

select project.* from project where REGEXP_LIKE (LOWER(description),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo project
-- Database    : oracle

select project.* from project where REGEXP_LIKE (LOWER(title),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : oracle

select queue.* from queue where REGEXP_LIKE (LOWER(agent_description),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : oracle

select queue.* from queue where REGEXP_LIKE (LOWER(title),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : oracle

select quick_filter_rules.* from quick_filter_rules where REGEXP_LIKE (LOWER(configuration),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : oracle

select quick_filter_rules.* from quick_filter_rules where REGEXP_LIKE (LOWER(name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Build queue
-- Database    : oracle

select quick_filters.* from quick_filters where REGEXP_LIKE (LOWER(name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Job requirements
-- Database    : oracle

select requirement.* from requirement where REGEXP_LIKE (LOWER(key_identifier),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Job requirements
-- Database    : oracle

select requirement.* from requirement where REGEXP_LIKE (LOWER(match_value),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Scripts data, used eg. for elastic images startup
-- Database    : oracle

select script.* from script where REGEXP_LIKE (LOWER(body),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in result stage
-- Database    : oracle

select stage_variable_context.* from stage_variable_context where REGEXP_LIKE (LOWER(variable_key),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in result stage
-- Database    : oracle

select stage_variable_context.* from stage_variable_context where REGEXP_LIKE (LOWER(variable_value),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Test error content
-- Database    : oracle

select test_error.* from test_error where REGEXP_LIKE (LOWER(error_content),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Name of the trusted app
-- Database    : oracle

select trusted_apps.* from trusted_apps where REGEXP_LIKE (LOWER(app_name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : IP address of the trusted app
-- Database    : oracle

select trusted_apps_ips.* from trusted_apps_ips where LOWER(ip_pattern) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : URL of the trusted app
-- Database    : oracle

select trusted_apps_urls.* from trusted_apps_urls where LOWER(url_pattern) like LOWER('%<OLD_VALUE>%') ;

-- Type        : select
-- Origin      : bamboo
-- Description : author of user comments
-- Database    : oracle

select user_comment.* from user_comment where REGEXP_LIKE (LOWER(content),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Repository commits comments - imported from vcs repository
-- Database    : oracle

select user_commit.* from user_commit where REGEXP_LIKE (LOWER(commit_comment_clob),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo internal users directory
-- Database    : oracle

select users.* from users where LOWER(email) = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : bamboo
-- Description : Bamboo internal users directory
-- Database    : oracle

select users.* from users where REGEXP_LIKE (LOWER(fullname),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : oracle

select variable_context.* from variable_context where REGEXP_LIKE (LOWER(variable_key),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : oracle

select variable_context.* from variable_context where REGEXP_LIKE (LOWER(variable_value),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : oracle

select variable_substitution.* from variable_substitution where REGEXP_LIKE (LOWER(variable_key),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Value of variables in results
-- Database    : oracle

select variable_substitution.* from variable_substitution where REGEXP_LIKE (LOWER(variable_value),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Configuration of vcs repository
-- Database    : oracle

select vcs_location.* from vcs_location where REGEXP_LIKE (LOWER(name),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

-- Type        : select
-- Origin      : bamboo
-- Description : Configuration of vcs repository
-- Database    : oracle

select vcs_location.* from vcs_location where REGEXP_LIKE (LOWER(xml_definition_data),'(^|[[:blank:]]|[[:punct:]])<OLD_VALUE>([[:blank:]]|[[:punct:]]|$)');

