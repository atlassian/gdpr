-- Type        : select
-- Origin      : jira mail plugin
-- Description : mail loop detection when handling issues
-- Database    : mssql

select AO_3B1893_LOOP_DETECTION.* from AO_3B1893_LOOP_DETECTION where SENDER_EMAIL = '<CURRENT_PD_VALUE>' ;

-- Type        : select
-- Origin      : atlassian webhooks plugin
-- Description : configuration of webhook
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/plugins/servlet/webhooks
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the cog icon in the top right corner
--     3. Choose System
--     4. Advanced section (left sidebar)
--     5. Choose WebHooks
-- 
-- Update via REST API:
--     https://developer.atlassian.com/server/jira/platform/webhooks/
select AO_4AEACD_WEBHOOK_DAO.* from AO_4AEACD_WEBHOOK_DAO where FILTER like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : atlassian webhooks plugin
-- Description : configuration of webhook
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/plugins/servlet/webhooks
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the cog icon in the top right corner
--     3. Choose System
--     4. Advanced section (left sidebar)
--     5. Choose WebHooks
-- 
-- Update via REST API:
--     https://developer.atlassian.com/server/jira/platform/webhooks/
select AO_4AEACD_WEBHOOK_DAO.* from AO_4AEACD_WEBHOOK_DAO where PARAMETERS like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : project shortcuts
-- Description : project shortcut
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/projects/${PKEY}/summary, where PKEY: select pkey from project where id = PROJECT_ID;
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'Projects' top menu
--     3. Select specific project name
--     4. 'Project Shortcuts' section (left sidebar)
select AO_550953_SHORTCUT.* from AO_550953_SHORTCUT where NAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : project shortcuts
-- Description : project shortcut
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/projects/${PKEY}/summary, where PKEY: select pkey from project where id = PROJECT_ID;
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'Projects' top menu
--     3. Select specific project name
--     4. 'Project Shortcuts' section (left sidebar)
select AO_550953_SHORTCUT.* from AO_550953_SHORTCUT where SHORTCUT_URL like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : activity stream entry (third party)
-- Database    : mssql

select AO_563AEE_ACTIVITY_ENTITY.* from AO_563AEE_ACTIVITY_ENTITY where CONTENT like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : activity stream entry (third party)
-- Database    : mssql

select AO_563AEE_ACTIVITY_ENTITY.* from AO_563AEE_ACTIVITY_ENTITY where TITLE like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : activity stream entry (third party)
-- Database    : mssql

select AO_563AEE_ACTIVITY_ENTITY.* from AO_563AEE_ACTIVITY_ENTITY where URL like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : entry author
-- Database    : mssql

select AO_563AEE_ACTOR_ENTITY.* from AO_563AEE_ACTOR_ENTITY where FULL_NAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : entry author
-- Database    : mssql

select AO_563AEE_ACTOR_ENTITY.* from AO_563AEE_ACTOR_ENTITY where PROFILE_PAGE_URI like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : entry author
-- Database    : mssql

select AO_563AEE_ACTOR_ENTITY.* from AO_563AEE_ACTOR_ENTITY where PROFILE_PICTURE_URI like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : link to some media eg. image
-- Database    : mssql

select AO_563AEE_MEDIA_LINK_ENTITY.* from AO_563AEE_MEDIA_LINK_ENTITY where URL like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : activity description
-- Database    : mssql

select AO_563AEE_OBJECT_ENTITY.* from AO_563AEE_OBJECT_ENTITY where CONTENT like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : activity description
-- Database    : mssql

select AO_563AEE_OBJECT_ENTITY.* from AO_563AEE_OBJECT_ENTITY where DISPLAY_NAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : activity description
-- Database    : mssql

select AO_563AEE_OBJECT_ENTITY.* from AO_563AEE_OBJECT_ENTITY where SUMMARY like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : activity context
-- Database    : mssql

select AO_563AEE_TARGET_ENTITY.* from AO_563AEE_TARGET_ENTITY where CONTENT like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : activity context
-- Database    : mssql

select AO_563AEE_TARGET_ENTITY.* from AO_563AEE_TARGET_ENTITY where DISPLAY_NAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : activity context
-- Database    : mssql

select AO_563AEE_TARGET_ENTITY.* from AO_563AEE_TARGET_ENTITY where SUMMARY like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : hipchat integration plugin
-- Description : seems to be auth data for specific users - ability to use private rooms in hipchat
-- Database    : mssql

select AO_5FB9D7_AOHIP_CHAT_USER.* from AO_5FB9D7_AOHIP_CHAT_USER where HIP_CHAT_USER_NAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira software
-- Description : spring audit log (reopen/close sprint)
-- Table valid only for specific product : Jira Software
-- Database    : mssql

select AO_60DB71_AUDITENTRY.* from AO_60DB71_AUDITENTRY where DATA like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira software
-- Description : board column name
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=columns
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Columns'
select AO_60DB71_COLUMN.* from AO_60DB71_COLUMN where NAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira software
-- Description : quick filter
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=quickFilters
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Quick Filters'
select AO_60DB71_QUICKFILTER.* from AO_60DB71_QUICKFILTER where DESCRIPTION like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira software
-- Description : quick filter
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=quickFilters
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Quick Filters'
select AO_60DB71_QUICKFILTER.* from AO_60DB71_QUICKFILTER where LONG_QUERY like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira software
-- Description : quick filter
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=quickFilters
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Quick Filters'
select AO_60DB71_QUICKFILTER.* from AO_60DB71_QUICKFILTER where NAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira software
-- Description : quick filter
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=quickFilters
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Quick Filters'
select AO_60DB71_QUICKFILTER.* from AO_60DB71_QUICKFILTER where QUERY like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira software
-- Description : board
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${ID}&tab=filter
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'General'
select AO_60DB71_RAPIDVIEW.* from AO_60DB71_RAPIDVIEW where NAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira software
-- Description : sprint
-- Table valid only for specific product : Jira Software
-- Table column valid only for specific versions : Jira>=7.5, Jira ServiceDesk>=3.8.1
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidBoard.jspa?rapidView=${RAPID_VIEW_ID}&view=planning.nodetail
-- 
--   How to access: 
--     1. ? Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'General'
select AO_60DB71_SPRINT.* from AO_60DB71_SPRINT where GOAL like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira software
-- Description : sprint
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidBoard.jspa?rapidView=${RAPID_VIEW_ID}&view=planning.nodetail
-- 
--   How to access: 
--     1. ? Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'General'
select AO_60DB71_SPRINT.* from AO_60DB71_SPRINT where NAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira software
-- Description : kanban filter subquery
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=filter
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'General'
select AO_60DB71_SUBQUERY.* from AO_60DB71_SUBQUERY where LONG_QUERY like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira software
-- Description : kanban filter subquery
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=filter
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'General'
select AO_60DB71_SUBQUERY.* from AO_60DB71_SUBQUERY where QUERY like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira software
-- Description : board swimlane
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=swimlanes
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Swimlanes'
select AO_60DB71_SWIMLANE.* from AO_60DB71_SWIMLANE where DESCRIPTION like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira software
-- Description : board swimlane
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=swimlanes
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Swimlanes'
select AO_60DB71_SWIMLANE.* from AO_60DB71_SWIMLANE where LONG_QUERY like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira software
-- Description : board swimlane
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=swimlanes
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Swimlanes'
select AO_60DB71_SWIMLANE.* from AO_60DB71_SWIMLANE where NAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira software
-- Description : board swimlane
-- Table valid only for specific product : Jira Software
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=swimlanes
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Swimlanes'
select AO_60DB71_SWIMLANE.* from AO_60DB71_SWIMLANE where QUERY like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira inform plugin
-- Description : saved issue event data
-- Table valid only for specific versions : Jira>=8.0, Jira ServiceDesk>=4.0.0
-- Database    : mssql

select AO_733371_EVENT_PARAMETER.* from AO_733371_EVENT_PARAMETER where VALUE like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : invite users plugin
-- Description : invitation to jira
-- Database    : mssql

select AO_97EDAB_USERINVITATION.* from AO_97EDAB_USERINVITATION where EMAIL_ADDRESS = '<CURRENT_PD_VALUE>' ;

-- Type        : select
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : mssql

select AO_A0B856_WEB_HOOK_LISTENER_AO.* from AO_A0B856_WEB_HOOK_LISTENER_AO where DESCRIPTION like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : mssql

select AO_A0B856_WEB_HOOK_LISTENER_AO.* from AO_A0B856_WEB_HOOK_LISTENER_AO where NAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : mssql

select AO_A0B856_WEB_HOOK_LISTENER_AO.* from AO_A0B856_WEB_HOOK_LISTENER_AO where URL like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : Jira diagnostics plugin
-- Description : saved alerts
-- Table valid only for specific versions : Jira>=7.13, Jira ServiceDesk>=3.16.0
-- Database    : mssql

select AO_C16815_ALERT_AO.* from AO_C16815_ALERT_AO where DETAILS_JSON like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : dvcs plugin
-- Description : Commit history from linked repositories
-- Table valid only for specific product : Jira Software
-- Database    : mssql

select AO_E8B6CC_CHANGESET_MAPPING.* from AO_E8B6CC_CHANGESET_MAPPING where AUTHOR like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : dvcs plugin
-- Description : Commit history from linked repositories
-- Table valid only for specific product : Jira Software
-- Database    : mssql

select AO_E8B6CC_CHANGESET_MAPPING.* from AO_E8B6CC_CHANGESET_MAPPING where AUTHOR_EMAIL = '<CURRENT_PD_VALUE>' ;

-- Type        : select
-- Origin      : dvcs plugin
-- Description : Commit history from linked repositories
-- Table valid only for specific product : Jira Software
-- Database    : mssql

select AO_E8B6CC_CHANGESET_MAPPING.* from AO_E8B6CC_CHANGESET_MAPPING where BRANCH like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : dvcs plugin
-- Description : Commit history from linked repositories
-- Table valid only for specific product : Jira Software
-- Database    : mssql

select AO_E8B6CC_CHANGESET_MAPPING.* from AO_E8B6CC_CHANGESET_MAPPING where MESSAGE like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : dvcs plugin
-- Description : Commit history from linked repositories
-- Table valid only for specific product : Jira Software
-- Database    : mssql

select AO_E8B6CC_CHANGESET_MAPPING.* from AO_E8B6CC_CHANGESET_MAPPING where RAW_AUTHOR like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : dvcs plugin
-- Description : 
-- Table valid only for specific product : Jira Software
-- Database    : mssql

select AO_E8B6CC_COMMIT.* from AO_E8B6CC_COMMIT where AUTHOR like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : dvcs plugin
-- Description : 
-- Table valid only for specific product : Jira Software
-- Database    : mssql

select AO_E8B6CC_COMMIT.* from AO_E8B6CC_COMMIT where MESSAGE like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : dvcs plugin
-- Description : 
-- Table valid only for specific product : Jira Software
-- Database    : mssql

select AO_E8B6CC_COMMIT.* from AO_E8B6CC_COMMIT where RAW_AUTHOR like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : dvcs plugin
-- Description : PR participants
-- Table valid only for specific product : Jira Software
-- Database    : mssql

select AO_E8B6CC_PR_PARTICIPANT.* from AO_E8B6CC_PR_PARTICIPANT where USERNAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : dvcs plugin
-- Description : pull request
-- Table valid only for specific product : Jira Software
-- Database    : mssql

select AO_E8B6CC_PULL_REQUEST.* from AO_E8B6CC_PULL_REQUEST where AUTHOR like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : dvcs plugin
-- Description : pull request
-- Table valid only for specific product : Jira Software
-- Database    : mssql

select AO_E8B6CC_PULL_REQUEST.* from AO_E8B6CC_PULL_REQUEST where DESTINATION_BRANCH like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : dvcs plugin
-- Description : pull request
-- Table valid only for specific product : Jira Software
-- Database    : mssql

select AO_E8B6CC_PULL_REQUEST.* from AO_E8B6CC_PULL_REQUEST where EXECUTED_BY like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : dvcs plugin
-- Description : pull request
-- Table valid only for specific product : Jira Software
-- Database    : mssql

select AO_E8B6CC_PULL_REQUEST.* from AO_E8B6CC_PULL_REQUEST where NAME like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : dvcs plugin
-- Description : pull request
-- Table valid only for specific product : Jira Software
-- Database    : mssql

select AO_E8B6CC_PULL_REQUEST.* from AO_E8B6CC_PULL_REQUEST where SOURCE_BRANCH like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : dvcs plugin
-- Description : pull request
-- Table valid only for specific product : Jira Software
-- Database    : mssql

select AO_E8B6CC_PULL_REQUEST.* from AO_E8B6CC_PULL_REQUEST where SOURCE_REPO like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : dvcs plugin
-- Description : pull request
-- Table valid only for specific product : Jira Software
-- Database    : mssql

select AO_E8B6CC_PULL_REQUEST.* from AO_E8B6CC_PULL_REQUEST where URL like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : application user
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${lower_user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. Click specific name on 'Full name' column
select app_user.* from app_user where lower_user_name = '<CURRENT_PD_VALUE>' ;

-- Type        : select
-- Origin      : jira
-- Description : audit log changed value
-- Database    : mssql

--  selecting dependant data
select audit_log.*,search_field as search_field_before,cast(replace(cast(search_field as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as search_field_after from audit_log where id in (select log_id from audit_changed_value where delta_from like '%<CURRENT_PD_VALUE>%' );
select audit_changed_value.* from audit_changed_value where delta_from like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : audit log changed value
-- Database    : mssql

--  selecting dependant data
select audit_log.*,search_field as search_field_before,cast(replace(cast(search_field as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as search_field_after from audit_log where id in (select log_id from audit_changed_value where delta_to like '%<CURRENT_PD_VALUE>%' );
select audit_changed_value.* from audit_changed_value where delta_to like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : Audit log affected item
-- Database    : mssql

select audit_item.* from audit_item where object_id like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : Audit log affected item
-- Database    : mssql

--  selecting dependant data
select audit_log.*,search_field as search_field_before,cast(replace(cast(search_field as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as search_field_after from audit_log where id in (select log_id from audit_item where (object_name like '%<CURRENT_PD_VALUE>%' ) AND object_type  in ('GROUP', 'SCHEME', 'WORKFLOW', 'PROJECT', 'VERSION', 'PROJECT_COMPONENT', 'BOARD', 'SCREEN'));
select audit_item.* from audit_item where (object_name like '%<CURRENT_PD_VALUE>%' ) AND object_type  in ('GROUP', 'SCHEME', 'WORKFLOW', 'PROJECT', 'VERSION', 'PROJECT_COMPONENT', 'BOARD', 'SCREEN');

-- Type        : select
-- Origin      : jira
-- Description : Audit log affected item
-- Database    : mssql

--  selecting dependant data
select audit_log.*,search_field as search_field_before,cast(replace(cast(search_field as nvarchar(max)), '<CURRENT_PD_VALUE>', '<NEW_PD_VALUE>') as ntext) as search_field_after from audit_log where id in (select log_id from audit_item where (object_name = '<CURRENT_PD_VALUE>' ) AND object_type  = 'USER');
select audit_item.* from audit_item where (object_name = '<CURRENT_PD_VALUE>' ) AND object_type  = 'USER';

-- Type        : select
-- Origin      : jira
-- Description : audit log main entry
-- Database    : mssql

select audit_log.* from audit_log where (object_id like '%<CURRENT_PD_VALUE>%' ) AND object_type  in ('GROUP', 'SCHEME', 'WORKFLOW', 'PROJECT', 'VERSION', 'PROJECT_COMPONENT', 'BOARD', 'SCREEN');

-- Type        : select
-- Origin      : jira
-- Description : audit log main entry
-- Database    : mssql

select audit_log.* from audit_log where (object_name like '%<CURRENT_PD_VALUE>%' ) AND object_type  in ('GROUP', 'SCHEME', 'WORKFLOW', 'PROJECT', 'VERSION', 'PROJECT_COMPONENT', 'BOARD', 'SCREEN');

-- Type        : select
-- Origin      : jira
-- Description : audit log main entry
-- Database    : mssql

select audit_log.* from audit_log where (object_name = '<CURRENT_PD_VALUE>' ) AND object_type  = 'USER';

-- Type        : select
-- Origin      : jira
-- Description : audit log main entry
-- Database    : mssql

select audit_log.* from audit_log where (remote_address = '<CURRENT_PD_VALUE>' ) AND remote_address  IS NOT NULL;

-- Type        : select
-- Origin      : jira
-- Description : audit log main entry
-- Database    : mssql

select audit_log.* from audit_log where search_field like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : avatar
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/ViewProfile.jspa?name=${user_key}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. Click specific name on 'Full name' column
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
select avatar.* from avatar where filename like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue history additional data
-- Database    : mssql

select changeitem.* from changeitem where field like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue history additional data
-- Database    : mssql

select changeitem.* from changeitem where newstring like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue history additional data
-- Database    : mssql

select changeitem.* from changeitem where newvalue like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue history additional data
-- Database    : mssql

select changeitem.* from changeitem where oldstring like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue history additional data
-- Database    : mssql

select changeitem.* from changeitem where oldvalue like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : component
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/projects/${PKEY}?selectedItem=com.atlassian.jira.jira-projects-plugin:components-page, where PKEY: select pkey from project where id = project
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Select specific project name
--     4. 'Components' section (left sidebar)
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/component
select component.* from component where cname like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : component
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/projects/${PKEY}?selectedItem=com.atlassian.jira.jira-projects-plugin:components-page, where PKEY: select pkey from project where id = project
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Select specific project name
--     4. 'Components' section (left sidebar)
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/component
select component.* from component where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : custom field
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditCustomField!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Custom fields'
select customfield.* from customfield where cfname like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : custom field
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditCustomField!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Custom fields'
select customfield.* from customfield where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : customfield value
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issue}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Custom fields'
select customfieldvalue.* from customfieldvalue where stringvalue like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : customfield value
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issue}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Custom fields'
select customfieldvalue.* from customfieldvalue where textvalue like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : group membership
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/EditUserGroups!default.jspa?name=${child_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Groups'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/group
select cwd_membership.* from cwd_membership where child_name = '<CURRENT_PD_VALUE>' ;

-- Type        : select
-- Origin      : jira
-- Description : group membership
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/EditUserGroups!default.jspa?name=${child_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Groups'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/group
select cwd_membership.* from cwd_membership where lower_child_name = '<CURRENT_PD_VALUE>' ;

-- Type        : select
-- Origin      : jira
-- Description : user_data
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
select cwd_user.* from cwd_user where display_name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : user_data
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
select cwd_user.* from cwd_user where email_address = '<CURRENT_PD_VALUE>' ;

-- Type        : select
-- Origin      : jira
-- Description : user_data
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
select cwd_user.* from cwd_user where first_name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : user_data
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
select cwd_user.* from cwd_user where last_name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : user_data
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
select cwd_user.* from cwd_user where lower_display_name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : user_data
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
select cwd_user.* from cwd_user where lower_email_address = '<CURRENT_PD_VALUE>' ;

-- Type        : select
-- Origin      : jira
-- Description : user_data
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
select cwd_user.* from cwd_user where lower_first_name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : user_data
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
select cwd_user.* from cwd_user where lower_last_name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : user_data
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
select cwd_user.* from cwd_user where lower_user_name = '<CURRENT_PD_VALUE>' ;

-- Type        : select
-- Origin      : jira
-- Description : user_data
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
select cwd_user.* from cwd_user where user_name = '<CURRENT_PD_VALUE>' ;

-- Type        : select
-- Origin      : jira
-- Description : draft workflow scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditWorkflowScheme.jspa?schemeId=${workflow_scheme_id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' (left sidebar)
--     5. Choose 'Workflow schemes'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflowscheme
select draftworkflowscheme.* from draftworkflowscheme where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : draft workflow scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditWorkflowScheme.jspa?schemeId=${workflow_scheme_id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' (left sidebar)
--     5. Choose 'Workflow schemes'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflowscheme
select draftworkflowscheme.* from draftworkflowscheme where name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : additional entity properties
-- Database    : mssql

select entity_property.* from entity_property where json_value like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : field configuration context
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/ConfigureCustomField!default.jspa?customFieldId={CF_ID} where CF_ID: split fieldid on _
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration schemes'
select fieldconfigscheme.* from fieldconfigscheme where configname like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : field configuration context
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/ConfigureCustomField!default.jspa?customFieldId={CF_ID} where CF_ID: split fieldid on _
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration schemes'
select fieldconfigscheme.* from fieldconfigscheme where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : field configuration
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldLayout!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration'
--     6. Click on a specific field configuration name
select fieldlayout.* from fieldlayout where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : field configuration
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldLayout!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration'
--     6. Click on a specific field configuration name
select fieldlayout.* from fieldlayout where name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : single field configuration on specific field configuration
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/ConfigureFieldLayout!default.jspa?id=${fieldlayout} and search for fieldidentifier
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration'
--     6. Click on a specific field configuration name
select fieldlayoutitem.* from fieldlayoutitem where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : field config scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldLayoutScheme!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration schemes'
--     6. Click on a specific field configuration name
select fieldlayoutscheme.* from fieldlayoutscheme where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : field config scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldLayoutScheme!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration schemes'
--     6. Click on a specific field configuration name
select fieldlayoutscheme.* from fieldlayoutscheme where name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : screen
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldScreen!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration'
--     6. Click on a specific field configuration name
--     7. Click on a 'Screens' under specific field name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/screens
select fieldscreen.* from fieldscreen where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : screen
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldScreen!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration'
--     6. Click on a specific field configuration name
--     7. Click on a 'Screens' under specific field name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/screens
select fieldscreen.* from fieldscreen where name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : screen scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldScreenScheme!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Screens' (left sidebar)
--     5. Choose 'Screens schemes'
--     6. Click 'Edit' under specific screen scheme name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/screens
select fieldscreenscheme.* from fieldscreenscheme where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : screen scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldScreenScheme!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Screens' (left sidebar)
--     5. Choose 'Screens schemes'
--     6. Click 'Edit' under specific screen scheme name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/screens
select fieldscreenscheme.* from fieldscreenscheme where name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : screen tab
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldScreen!default.jspa?id=${fieldscreen}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Screens' (left sidebar)
--     5. Click 'Edit' under specific screen name
select fieldscreentab.* from fieldscreentab where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : screen tab
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldScreen!default.jspa?id=${fieldscreen}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Screens' (left sidebar)
--     5. Click 'Edit' under specific screen name
select fieldscreentab.* from fieldscreentab where name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : attachment
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Attachments' sections
select fileattachment.* from fileattachment where filename like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : custom field default value
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/ConfigureCustomField!default.jspa?customFieldId=${CUSTOM_FIELD_ID} where CUSTOM_FIELD_ID: select SUBSTRING(fieldid, 13) from fieldconfiguration where id = ${datakey};
-- 
select genericconfiguration.* from genericconfiguration where xmlvalue like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue security scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditIssueSecurityScheme!default.jspa?schemeId=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. Left sidebar > 'Issue Security Schemes'
--     5. Click 'Edit' under specific issue security scheme name
select issuesecurityscheme.* from issuesecurityscheme where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue security scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditIssueSecurityScheme!default.jspa?schemeId=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. Left sidebar > 'Issue Security Schemes'
--     5. Click 'Edit' under specific issue security scheme name
select issuesecurityscheme.* from issuesecurityscheme where name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue status
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditStatus!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Issue Attributes' (left sidebar)
--     5. Choose 'Status'
--     6. Click 'Edit' under specific issue status name
select issuestatus.* from issuestatus where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue status
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditStatus!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Issue Attributes' (left sidebar)
--     5. Choose 'Status'
--     6. Click 'Edit' under specific issue status name
select issuestatus.* from issuestatus where pname like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue type
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditIssueType!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Issue Types' (left sidebar)
--     5. Choose 'Issue Types'
--     6. Click 'Edit' under specific issue type name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issuetype
select issuetype.* from issuetype where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue type
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditIssueType!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Issue Types' (left sidebar)
--     5. Choose 'Issue Types'
--     6. Click 'Edit' under specific issue type name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issuetype
select issuetype.* from issuetype where pname like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue type screen scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditIssueTypeScreenScheme!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Screens' (left sidebar)
--     5. Choose 'Issue type screen schemes'
--     6. Click 'Edit' under specific issue type screen scheme name
select issuetypescreenscheme.* from issuetypescreenscheme where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue type screen scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditIssueTypeScreenScheme!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Screens' (left sidebar)
--     5. Choose 'Issue type screen schemes'
--     6. Click 'Edit' under specific issue type screen scheme name
select issuetypescreenscheme.* from issuetypescreenscheme where name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : comment
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Click 'Comments' tab
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issue
select jiraaction.* from jiraaction where actionbody like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : draft of workflow
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/workflows/EditWorkflowDispatcher.jspa?wfName=${parentname}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' section (left sidebar)
--     5. Choose 'Workflows'
--     6. Click 'Edit' under specific workflow name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflow
select jiradraftworkflows.* from jiradraftworkflows where descriptor like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : draft of workflow
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/workflows/EditWorkflowDispatcher.jspa?wfName=${parentname}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' section (left sidebar)
--     5. Choose 'Workflows'
--     6. Click 'Edit' under specific workflow name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflow
select jiradraftworkflows.* from jiradraftworkflows where parentname like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issue
select jiraissue.* from jiraissue where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issue
select jiraissue.* from jiraissue where environment like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issue
select jiraissue.* from jiraissue where summary like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : workflow
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/workflows/EditWorkflowDispatcher.jspa?wfName=${workflowname}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' section (left sidebar)
--     5. Choose 'Workflows'
--     6. Click 'Edit' under specific workflow name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflow
select jiraworkflows.* from jiraworkflows where descriptor like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : workflow
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/workflows/EditWorkflowDispatcher.jspa?wfName=${workflowname}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' section (left sidebar)
--     5. Choose 'Workflows'
--     6. Click 'Edit' under specific workflow name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflow
-- 
-- SQL update of workflow name is not supported because it could break JIRA
-- Please follow steps described below
-- If it contains personal data please copy it and give it a new name
-- Assign new workflow in all workflow schemes using old workflow
-- Delete old workflow
select jiraworkflows.* from jiraworkflows where workflowname like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue label
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issue}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Details' section
select label.* from label where label like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : seems to be related to mail handler eg. issue created from email, issue commented from email etc.
-- Database    : mssql

select notificationinstance.* from notificationinstance where emailaddress = '<CURRENT_PD_VALUE>' ;

-- Type        : select
-- Origin      : jira
-- Description : notification scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditNotificationScheme!default.jspa?schemeId=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. Left sidebar > Choose 'Notification schemes'
--     5. Click 'Edit' under specific notification scheme name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/notificationscheme
select notificationscheme.* from notificationscheme where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : notification scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditNotificationScheme!default.jspa?schemeId=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. Left sidebar > Choose 'Notification schemes'
--     5. Click 'Edit' under specific notification scheme name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/notificationscheme
select notificationscheme.* from notificationscheme where name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : permission scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditPermissionScheme!default.jspa?schemeId=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. Left sidebar > Choose 'Permission schemes'
--     5. Click 'Edit' under specific permission scheme name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/permissionscheme
select permissionscheme.* from permissionscheme where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : permission scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditPermissionScheme!default.jspa?schemeId=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. Left sidebar > Choose 'Permission schemes'
--     5. Click 'Edit' under specific permission scheme name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/permissionscheme
select permissionscheme.* from permissionscheme where name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : dashboard
-- Database    : mssql

select portalpage.* from portalpage where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : dashboard
-- Database    : mssql

select portalpage.* from portalpage where pagename like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : priority
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditPriority!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Priorities' section (left sidebar)
--     5. Choose 'Priorities'
--     6. Click 'Edit' under specific priority name
select priority.* from priority where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : priority
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditPriority!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Priorities' section (left sidebar)
--     5. Choose 'Priorities'
--     6. Click 'Edit' under specific priority name
select priority.* from priority where iconurl like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : priority
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditPriority!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Priorities' section (left sidebar)
--     5. Choose 'Priorities'
--     6. Click 'Edit' under specific priority name
select priority.* from priority where pname like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : project
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/project/EditProject!default.jspa?pid=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/project
select project.* from project where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : project
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/project/EditProject!default.jspa?pid=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/project
-- 
-- SQL update of original project key is not supported because it could break JIRA
-- In order to alter original project key:
-- 	create new project
-- 	move all issues to new project
-- 	remove old project
select project.* from project where originalkey like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : project
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/project/EditProject!default.jspa?pid=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/project
-- 
-- SQL update of project key is not supported because it could break JIRA
-- Please follow steps described above
select project.* from project where pkey like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : project
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/project/EditProject!default.jspa?pid=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/project
select project.* from project where pname like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : project
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/project/EditProject!default.jspa?pid=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/project
select project.* from project where url like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : project category
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/projectcategories/ViewProjectCategories!default.jspa
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. 'Project categories' sidebar
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/projectCategory
select projectcategory.* from projectcategory where cname like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : project category
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/projectcategories/ViewProjectCategories!default.jspa
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. 'Project categories' sidebar
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/projectCategory
select projectcategory.* from projectcategory where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : version
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/plugins/servlet/project-config/${PROJECT_KEY}/administer-versions  where PROJECT_KEY: select pkey from project where id = ${project}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
--     5. 'Project settings' sidebar
--     6. Choose 'Version'
select projectversion.* from projectversion where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : version
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/plugins/servlet/project-config/${PROJECT_KEY}/administer-versions  where PROJECT_KEY: select pkey from project where id = ${project}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
--     5. 'Project settings' sidebar
--     6. Choose 'Version'
select projectversion.* from projectversion where url like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : version
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/plugins/servlet/project-config/${PROJECT_KEY}/administer-versions  where PROJECT_KEY: select pkey from project where id = ${project}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
--     5. 'Project settings' sidebar
--     6. Choose 'Version'
select projectversion.* from projectversion where vname like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : entity property value
-- Database    : mssql

select propertystring.* from propertystring where propertyvalue like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : entity property value
-- Database    : mssql

select propertytext.* from propertytext where propertyvalue like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
select remotelink.* from remotelink where icontitle like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
select remotelink.* from remotelink where relationship like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
select remotelink.* from remotelink where statuscategorykey like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
select remotelink.* from remotelink where statusdescription like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
select remotelink.* from remotelink where statusiconlink like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
select remotelink.* from remotelink where statusicontitle like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
select remotelink.* from remotelink where statusiconurl like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
select remotelink.* from remotelink where statusname like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
select remotelink.* from remotelink where summary like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
select remotelink.* from remotelink where title like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
select remotelink.* from remotelink where url like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue resolution
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditResolution!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Issue Attributes' section (left sidebar)
--     5. Choose 'Resolutions'
select resolution.* from resolution where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue resolution
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditResolution!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Issue Attributes' section (left sidebar)
--     5. Choose 'Resolutions'
select resolution.* from resolution where iconurl like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue resolution
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditResolution!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Issue Attributes' section (left sidebar)
--     5. Choose 'Resolutions'
select resolution.* from resolution where pname like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue security level
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditIssueSecurities!default.jspa?atl_token=schemeId=${scheme}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. Left sidebar > 'Issue security schemes'
--     5. Click 'Security Levels' under a specific issue security scheme name
select schemeissuesecuritylevels.* from schemeissuesecuritylevels where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : issue security level
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditIssueSecurities!default.jspa?atl_token=schemeId=${scheme}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. Left sidebar > 'Issue security schemes'
--     5. Click 'Security Levels' under a specific issue security scheme name
select schemeissuesecuritylevels.* from schemeissuesecuritylevels where name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : saved jql filter
-- Database    : mssql

select searchrequest.* from searchrequest where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : saved jql filter
-- Database    : mssql

select searchrequest.* from searchrequest where filtername like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : saved jql filter
-- Database    : mssql

select searchrequest.* from searchrequest where filtername_lower like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : saved jql filter
-- Database    : mssql

select searchrequest.* from searchrequest where reqcontent like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : recorded last operations for recently used entities (users, projects, issues etc.)
-- Database    : mssql

select userhistoryitem.* from userhistoryitem where data like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : workflow scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditWorkflowScheme.jspa?schemeId=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' (left sidebar)
--     5. Choose 'Workflow schemes'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflowscheme
select workflowscheme.* from workflowscheme where description like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : workflow scheme
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditWorkflowScheme.jspa?schemeId=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' (left sidebar)
--     5. Choose 'Workflow schemes'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflowscheme
select workflowscheme.* from workflowscheme where name like '%<CURRENT_PD_VALUE>%' ;

-- Type        : select
-- Origin      : jira
-- Description : worklog
-- Database    : mssql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Click 'Work Log' tab
select worklog.* from worklog where worklogbody like '%<CURRENT_PD_VALUE>%' ;

