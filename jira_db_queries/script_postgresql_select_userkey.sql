-- Type        : select
-- Origin      : Jira mobile plugin
-- Description : mobile notification settings for user
-- Table valid only for specific versions : Jira>=8.3, Jira ServiceDesk>=4.3.0
-- Database    : postgresql

select "AO_0A5972_NOTIFICATION_SETTING".* from "AO_0A5972_NOTIFICATION_SETTING" where LOWER("USER_KEY") = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : Jira mobile plugin
-- Description : configuration for push messages
-- Table valid only for specific versions : Jira>=8.3, Jira ServiceDesk>=4.3.0
-- Database    : postgresql

select "AO_0A5972_PUSH_REGISTRATION".* from "AO_0A5972_PUSH_REGISTRATION" where LOWER("USER_KEY") = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : atlassian whisper plugin
-- Description : properties for messaging campaigns
-- Table valid only for specific versions : Jira>=7.4, Jira ServiceDesk>=3.6.0
-- Database    : postgresql

select "AO_21F425_USER_PROPERTY_AO".* from "AO_21F425_USER_PROPERTY_AO" where LOWER("USER") = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : support healthcheck plugin
-- Description : record of dismissed messages
-- Table valid only for specific versions : Jira>=7.6 (with Atlassian Troubleshooting and Support Tools up to version 1.10.5), Jira ServiceDesk>=3.9.0
-- Database    : postgresql

select "AO_2F1435_READ_NOTIFICATIONS".* from "AO_2F1435_READ_NOTIFICATIONS" where LOWER("USER_KEY") = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : support healthcheck plugin
-- Description : record of dismissed messages
-- Table valid only for specific versions : Jira>=7.6 (with Atlassian Troubleshooting and Support Tools from version 1.10.5), Jira ServiceDesk>=3.9.0
-- Database    : postgresql

select "AO_4789DD_READ_NOTIFICATIONS".* from "AO_4789DD_READ_NOTIFICATIONS" where LOWER("USER_KEY") = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : atlassian webhooks plugin
-- Description : configuration of webhook
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/plugins/servlet/webhooks
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the cog icon in the top right corner
--     3. Choose System
--     4. Advanced section (left sidebar)
--     5. Choose WebHooks
-- 
-- Update via REST API:
--     https://developer.atlassian.com/server/jira/platform/webhooks/
select "AO_4AEACD_WEBHOOK_DAO".* from "AO_4AEACD_WEBHOOK_DAO" where LOWER("LAST_UPDATED_USER") = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : activity stream entry (third party)
-- Database    : postgresql

select "AO_563AEE_ACTIVITY_ENTITY".* from "AO_563AEE_ACTIVITY_ENTITY" where LOWER("POSTER") = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : activity stream entry (third party)
-- Database    : postgresql

select "AO_563AEE_ACTIVITY_ENTITY".* from "AO_563AEE_ACTIVITY_ENTITY" where LOWER("USERNAME") = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : entry author
-- Database    : postgresql

select "AO_563AEE_ACTOR_ENTITY".* from "AO_563AEE_ACTOR_ENTITY" where LOWER("USERNAME") = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : hipchat integration plugin
-- Description : seems to be auth data for specific users - ability to use private rooms in hipchat
-- Database    : postgresql

select "AO_5FB9D7_AOHIP_CHAT_USER".* from "AO_5FB9D7_AOHIP_CHAT_USER" where LOWER("USER_KEY") = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira software
-- Description : spring audit log (reopen/close sprint)
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

select "AO_60DB71_AUDITENTRY".* from "AO_60DB71_AUDITENTRY" where LOWER("USER") = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira software
-- Description : board admins
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=filter
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'General'
select "AO_60DB71_BOARDADMINS".* from "AO_60DB71_BOARDADMINS" where (LOWER("KEY") = LOWER('<CURRENT_PD_VALUE>') ) AND "TYPE"  = 'USER';

-- Type        : select
-- Origin      : jira software
-- Description : board
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${ID}&tab=filter
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'General'
select "AO_60DB71_RAPIDVIEW".* from "AO_60DB71_RAPIDVIEW" where LOWER("OWNER_USER_NAME") = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : Jira inform plugin
-- Description : event recipient
-- Table valid only for specific versions : Jira>=8.0, Jira ServiceDesk>=4.0.0
-- Database    : postgresql

select "AO_733371_EVENT_RECIPIENT".* from "AO_733371_EVENT_RECIPIENT" where LOWER("USER_KEY") = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : Jira inform plugin
-- Description : saved issue events
-- Table valid only for specific versions : Jira>=8.0, Jira ServiceDesk>=4.0.0
-- Database    : postgresql

select "AO_733371_EVENT".* from "AO_733371_EVENT" where LOWER("USER_KEY") = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : invite users plugin
-- Description : invitation to jira
-- Database    : postgresql

select "AO_97EDAB_USERINVITATION".* from "AO_97EDAB_USERINVITATION" where LOWER("SENDER_USERNAME") = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : application user
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${lower_user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. Click specific name on 'Full name' column
select "app_user".* from app_user where LOWER(user_key) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : audit log main entry
-- Database    : postgresql

select "audit_log".* from audit_log where LOWER(author_key) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : audit log main entry
-- Database    : postgresql

select "audit_log".* from audit_log where (LOWER(object_id) = LOWER('<CURRENT_PD_VALUE>') ) AND object_type  = 'USER';

-- Type        : select
-- Origin      : jira
-- Description : avatar
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/ViewProfile.jspa?name=${user_key}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. Click specific name on 'Full name' column
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
select "avatar".* from avatar where LOWER(owner) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : issue history
-- Database    : postgresql

select "changegroup".* from changegroup where LOWER(author) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : columns in issue table view
-- Database    : postgresql

select "columnlayout".* from columnlayout where LOWER(username) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : component
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/projects/${PKEY}?selectedItem=com.atlassian.jira.jira-projects-plugin:components-page, where PKEY: select pkey from project where id = project
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Select specific project name
--     4. 'Components' section (left sidebar)
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/component
select "component".* from component where LOWER(lead) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : draft workflow scheme
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditWorkflowScheme.jspa?schemeId=${workflow_scheme_id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' (left sidebar)
--     5. Choose 'Workflow schemes'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflowscheme
select "draftworkflowscheme".* from draftworkflowscheme where LOWER(last_modified_user) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : favourite jql filter
-- Database    : postgresql

select "favouriteassociations".* from favouriteassociations where LOWER(username) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : attachment
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Attachments' sections
select "fileattachment".* from fileattachment where LOWER(author) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : send list of issues matching jql
-- Database    : postgresql

select "filtersubscription".* from filtersubscription where LOWER(username) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : comment
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Click 'Comments' tab
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issue
select "jiraaction".* from jiraaction where LOWER(author) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : comment
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Click 'Comments' tab
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issue
select "jiraaction".* from jiraaction where LOWER(updateauthor) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : issue
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issue
select "jiraissue".* from jiraissue where LOWER(archivedby) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : issue
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issue
select "jiraissue".* from jiraissue where LOWER(assignee) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : issue
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issue
select "jiraissue".* from jiraissue where LOWER(creator) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : issue
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issue
select "jiraissue".* from jiraissue where LOWER(reporter) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : dashboard
-- Database    : postgresql

select "portalpage".* from portalpage where LOWER(username) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : project
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/project/EditProject!default.jspa?pid=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/project
select "project".* from project where LOWER(lead) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : project-role mapping
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/plugins/servlet/project-config/${PROJECT_KEY}/roles where PROJECT_KEY: select pkey from project where id = ${pid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
--     5. 'Project settings' sidebar
--     6. Choose 'Users and roles'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/project/{projectIdOrKey}/role
select "projectroleactor".* from projectroleactor where (LOWER(roletypeparameter) = LOWER('<CURRENT_PD_VALUE>') ) AND roletype  = 'atlassian-user-role-actor';

-- Type        : select
-- Origin      : jira
-- Description : used to auto log in based on cookie
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/UserRememberMeCookies!default.jspa?name=${username}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. Click 'Full name' value under specific user name
--     5. Button 'Actions' (top right)
--     6. Choose 'Remember My Login' Tokens
select "remembermetoken".* from remembermetoken where LOWER(username) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : saved jql filter
-- Database    : postgresql

select "searchrequest".* from searchrequest where LOWER(authorname) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : saved jql filter
-- Database    : postgresql

select "searchrequest".* from searchrequest where LOWER(username) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : issue votes/watchers
-- Database    : postgresql

select "userassociation".* from userassociation where LOWER(source_name) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : recorded last operations for recently used entities (users, projects, issues etc.)
-- Database    : postgresql

select "userhistoryitem".* from userhistoryitem where (LOWER(entityid) = LOWER('<CURRENT_PD_VALUE>') ) AND entitytype  IN ('UsedUser','Assignee');

-- Type        : select
-- Origin      : jira
-- Description : recorded last operations for recently used entities (users, projects, issues etc.)
-- Database    : postgresql

select "userhistoryitem".* from userhistoryitem where LOWER(username) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : worklog
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Click 'Work Log' tab
select "worklog".* from worklog where LOWER(author) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : worklog
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Click 'Work Log' tab
select "worklog".* from worklog where LOWER(updateauthor) = LOWER('<CURRENT_PD_VALUE>') ;

