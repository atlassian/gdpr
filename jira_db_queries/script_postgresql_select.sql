-- Type        : select
-- Origin      : jira mail plugin
-- Description : mail loop detection when handling issues
-- Database    : postgresql

select "AO_3B1893_LOOP_DETECTION".* from "AO_3B1893_LOOP_DETECTION" where LOWER("SENDER_EMAIL") = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : atlassian webhooks plugin
-- Description : configuration of webhook
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/plugins/servlet/webhooks
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the cog icon in the top right corner
--     3. Choose System
--     4. Advanced section (left sidebar)
--     5. Choose WebHooks
-- 
-- Update via REST API:
--     https://developer.atlassian.com/server/jira/platform/webhooks/
select "AO_4AEACD_WEBHOOK_DAO".* from "AO_4AEACD_WEBHOOK_DAO" where LOWER("FILTER") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : atlassian webhooks plugin
-- Description : configuration of webhook
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/plugins/servlet/webhooks
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the cog icon in the top right corner
--     3. Choose System
--     4. Advanced section (left sidebar)
--     5. Choose WebHooks
-- 
-- Update via REST API:
--     https://developer.atlassian.com/server/jira/platform/webhooks/
select "AO_4AEACD_WEBHOOK_DAO".* from "AO_4AEACD_WEBHOOK_DAO" where LOWER("PARAMETERS") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : project shortcuts
-- Description : project shortcut
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/projects/${PKEY}/summary, where PKEY: select pkey from project where id = PROJECT_ID;
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'Projects' top menu
--     3. Select specific project name
--     4. 'Project Shortcuts' section (left sidebar)
select "AO_550953_SHORTCUT".* from "AO_550953_SHORTCUT" where LOWER("NAME") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : project shortcuts
-- Description : project shortcut
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/projects/${PKEY}/summary, where PKEY: select pkey from project where id = PROJECT_ID;
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'Projects' top menu
--     3. Select specific project name
--     4. 'Project Shortcuts' section (left sidebar)
select "AO_550953_SHORTCUT".* from "AO_550953_SHORTCUT" where LOWER("SHORTCUT_URL") like LOWER('%<CURRENT_PD_VALUE>%') ;

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : activity stream entry (third party)
-- Database    : postgresql

select "AO_563AEE_ACTIVITY_ENTITY".* from "AO_563AEE_ACTIVITY_ENTITY" where LOWER("CONTENT") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : activity stream entry (third party)
-- Database    : postgresql

select "AO_563AEE_ACTIVITY_ENTITY".* from "AO_563AEE_ACTIVITY_ENTITY" where LOWER("TITLE") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : activity stream entry (third party)
-- Database    : postgresql

select "AO_563AEE_ACTIVITY_ENTITY".* from "AO_563AEE_ACTIVITY_ENTITY" where LOWER("URL") like LOWER('%<CURRENT_PD_VALUE>%') ;

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : entry author
-- Database    : postgresql

select "AO_563AEE_ACTOR_ENTITY".* from "AO_563AEE_ACTOR_ENTITY" where LOWER("FULL_NAME") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : entry author
-- Database    : postgresql

select "AO_563AEE_ACTOR_ENTITY".* from "AO_563AEE_ACTOR_ENTITY" where LOWER("PROFILE_PAGE_URI") like LOWER('%<CURRENT_PD_VALUE>%') ;

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : entry author
-- Database    : postgresql

select "AO_563AEE_ACTOR_ENTITY".* from "AO_563AEE_ACTOR_ENTITY" where LOWER("PROFILE_PICTURE_URI") like LOWER('%<CURRENT_PD_VALUE>%') ;

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : link to some media eg. image
-- Database    : postgresql

select "AO_563AEE_MEDIA_LINK_ENTITY".* from "AO_563AEE_MEDIA_LINK_ENTITY" where LOWER("URL") like LOWER('%<CURRENT_PD_VALUE>%') ;

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : activity description
-- Database    : postgresql

select "AO_563AEE_OBJECT_ENTITY".* from "AO_563AEE_OBJECT_ENTITY" where LOWER("CONTENT") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : activity description
-- Database    : postgresql

select "AO_563AEE_OBJECT_ENTITY".* from "AO_563AEE_OBJECT_ENTITY" where LOWER("DISPLAY_NAME") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : activity description
-- Database    : postgresql

select "AO_563AEE_OBJECT_ENTITY".* from "AO_563AEE_OBJECT_ENTITY" where LOWER("SUMMARY") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : activity context
-- Database    : postgresql

select "AO_563AEE_TARGET_ENTITY".* from "AO_563AEE_TARGET_ENTITY" where LOWER("CONTENT") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : activity context
-- Database    : postgresql

select "AO_563AEE_TARGET_ENTITY".* from "AO_563AEE_TARGET_ENTITY" where LOWER("DISPLAY_NAME") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : atlassian streams plugin
-- Description : activity context
-- Database    : postgresql

select "AO_563AEE_TARGET_ENTITY".* from "AO_563AEE_TARGET_ENTITY" where LOWER("SUMMARY") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : hipchat integration plugin
-- Description : seems to be auth data for specific users - ability to use private rooms in hipchat
-- Database    : postgresql

select "AO_5FB9D7_AOHIP_CHAT_USER".* from "AO_5FB9D7_AOHIP_CHAT_USER" where LOWER("HIP_CHAT_USER_NAME") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira software
-- Description : spring audit log (reopen/close sprint)
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

select "AO_60DB71_AUDITENTRY".* from "AO_60DB71_AUDITENTRY" where LOWER("DATA") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira software
-- Description : board column name
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=columns
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Columns'
select "AO_60DB71_COLUMN".* from "AO_60DB71_COLUMN" where LOWER("NAME") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira software
-- Description : quick filter
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=quickFilters
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Quick Filters'
select "AO_60DB71_QUICKFILTER".* from "AO_60DB71_QUICKFILTER" where LOWER("DESCRIPTION") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira software
-- Description : quick filter
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=quickFilters
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Quick Filters'
select "AO_60DB71_QUICKFILTER".* from "AO_60DB71_QUICKFILTER" where LOWER("LONG_QUERY") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira software
-- Description : quick filter
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=quickFilters
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Quick Filters'
select "AO_60DB71_QUICKFILTER".* from "AO_60DB71_QUICKFILTER" where LOWER("NAME") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira software
-- Description : quick filter
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=quickFilters
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Quick Filters'
select "AO_60DB71_QUICKFILTER".* from "AO_60DB71_QUICKFILTER" where LOWER("QUERY") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira software
-- Description : board
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${ID}&tab=filter
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'General'
select "AO_60DB71_RAPIDVIEW".* from "AO_60DB71_RAPIDVIEW" where LOWER("NAME") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira software
-- Description : sprint
-- Table valid only for specific product : Jira Software
-- Table column valid only for specific versions : Jira>=7.5, Jira ServiceDesk>=3.8.1
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidBoard.jspa?rapidView=${RAPID_VIEW_ID}&view=planning.nodetail
-- 
--   How to access: 
--     1. ? Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'General'
select "AO_60DB71_SPRINT".* from "AO_60DB71_SPRINT" where LOWER("GOAL") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira software
-- Description : sprint
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidBoard.jspa?rapidView=${RAPID_VIEW_ID}&view=planning.nodetail
-- 
--   How to access: 
--     1. ? Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'General'
select "AO_60DB71_SPRINT".* from "AO_60DB71_SPRINT" where LOWER("NAME") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira software
-- Description : kanban filter subquery
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=filter
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'General'
select "AO_60DB71_SUBQUERY".* from "AO_60DB71_SUBQUERY" where LOWER("LONG_QUERY") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira software
-- Description : kanban filter subquery
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=filter
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'General'
select "AO_60DB71_SUBQUERY".* from "AO_60DB71_SUBQUERY" where LOWER("QUERY") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira software
-- Description : board swimlane
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=swimlanes
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Swimlanes'
select "AO_60DB71_SWIMLANE".* from "AO_60DB71_SWIMLANE" where LOWER("DESCRIPTION") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira software
-- Description : board swimlane
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=swimlanes
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Swimlanes'
select "AO_60DB71_SWIMLANE".* from "AO_60DB71_SWIMLANE" where LOWER("LONG_QUERY") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira software
-- Description : board swimlane
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=swimlanes
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Swimlanes'
select "AO_60DB71_SWIMLANE".* from "AO_60DB71_SWIMLANE" where LOWER("NAME") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira software
-- Description : board swimlane
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/RapidView.jspa?rapidView=${RAPID_VIEW_ID}&tab=swimlanes
-- 
--   How to access: 
--     1. Login to Jira as Jira user:
--     2. Click the 'profile' avatar in the top right corner
--     3. Click 'Board' button
--     4. Choose 'Configure'
--     5. 'Configuration' section (left sidebar)
--     6. Choose 'Swimlanes'
select "AO_60DB71_SWIMLANE".* from "AO_60DB71_SWIMLANE" where LOWER("QUERY") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : Jira inform plugin
-- Description : saved issue event data
-- Table valid only for specific versions : Jira>=8.0, Jira ServiceDesk>=4.0.0
-- Database    : postgresql

select "AO_733371_EVENT_PARAMETER".* from "AO_733371_EVENT_PARAMETER" where LOWER("VALUE") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : invite users plugin
-- Description : invitation to jira
-- Database    : postgresql

select "AO_97EDAB_USERINVITATION".* from "AO_97EDAB_USERINVITATION" where LOWER("EMAIL_ADDRESS") = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : postgresql

select "AO_A0B856_WEB_HOOK_LISTENER_AO".* from "AO_A0B856_WEB_HOOK_LISTENER_AO" where LOWER("DESCRIPTION") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : postgresql

select "AO_A0B856_WEB_HOOK_LISTENER_AO".* from "AO_A0B856_WEB_HOOK_LISTENER_AO" where LOWER("NAME") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : atlassian webhooks plugin
-- Description : webhook configuration - seems to be legacy table, not used anymore
-- Database    : postgresql

select "AO_A0B856_WEB_HOOK_LISTENER_AO".* from "AO_A0B856_WEB_HOOK_LISTENER_AO" where LOWER("URL") like LOWER('%<CURRENT_PD_VALUE>%') ;

-- Type        : select
-- Origin      : Jira diagnostics plugin
-- Description : saved alerts
-- Table valid only for specific versions : Jira>=7.13, Jira ServiceDesk>=3.16.0
-- Database    : postgresql

select "AO_C16815_ALERT_AO".* from "AO_C16815_ALERT_AO" where LOWER("DETAILS_JSON") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : dvcs plugin
-- Description : Commit history from linked repositories
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

select "AO_E8B6CC_CHANGESET_MAPPING".* from "AO_E8B6CC_CHANGESET_MAPPING" where LOWER("AUTHOR") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : dvcs plugin
-- Description : Commit history from linked repositories
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

select "AO_E8B6CC_CHANGESET_MAPPING".* from "AO_E8B6CC_CHANGESET_MAPPING" where LOWER("AUTHOR_EMAIL") = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : dvcs plugin
-- Description : Commit history from linked repositories
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

select "AO_E8B6CC_CHANGESET_MAPPING".* from "AO_E8B6CC_CHANGESET_MAPPING" where LOWER("BRANCH") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : dvcs plugin
-- Description : Commit history from linked repositories
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

select "AO_E8B6CC_CHANGESET_MAPPING".* from "AO_E8B6CC_CHANGESET_MAPPING" where LOWER("MESSAGE") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : dvcs plugin
-- Description : Commit history from linked repositories
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

select "AO_E8B6CC_CHANGESET_MAPPING".* from "AO_E8B6CC_CHANGESET_MAPPING" where LOWER("RAW_AUTHOR") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : dvcs plugin
-- Description : 
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

select "AO_E8B6CC_COMMIT".* from "AO_E8B6CC_COMMIT" where LOWER("AUTHOR") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : dvcs plugin
-- Description : 
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

select "AO_E8B6CC_COMMIT".* from "AO_E8B6CC_COMMIT" where LOWER("MESSAGE") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : dvcs plugin
-- Description : 
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

select "AO_E8B6CC_COMMIT".* from "AO_E8B6CC_COMMIT" where LOWER("RAW_AUTHOR") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : dvcs plugin
-- Description : PR participants
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

select "AO_E8B6CC_PR_PARTICIPANT".* from "AO_E8B6CC_PR_PARTICIPANT" where LOWER("USERNAME") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : dvcs plugin
-- Description : pull request
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

select "AO_E8B6CC_PULL_REQUEST".* from "AO_E8B6CC_PULL_REQUEST" where LOWER("AUTHOR") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : dvcs plugin
-- Description : pull request
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

select "AO_E8B6CC_PULL_REQUEST".* from "AO_E8B6CC_PULL_REQUEST" where LOWER("DESTINATION_BRANCH") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : dvcs plugin
-- Description : pull request
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

select "AO_E8B6CC_PULL_REQUEST".* from "AO_E8B6CC_PULL_REQUEST" where LOWER("EXECUTED_BY") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : dvcs plugin
-- Description : pull request
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

select "AO_E8B6CC_PULL_REQUEST".* from "AO_E8B6CC_PULL_REQUEST" where LOWER("NAME") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : dvcs plugin
-- Description : pull request
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

select "AO_E8B6CC_PULL_REQUEST".* from "AO_E8B6CC_PULL_REQUEST" where LOWER("SOURCE_BRANCH") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : dvcs plugin
-- Description : pull request
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

select "AO_E8B6CC_PULL_REQUEST".* from "AO_E8B6CC_PULL_REQUEST" where LOWER("SOURCE_REPO") ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : dvcs plugin
-- Description : pull request
-- Table valid only for specific product : Jira Software
-- Database    : postgresql

select "AO_E8B6CC_PULL_REQUEST".* from "AO_E8B6CC_PULL_REQUEST" where LOWER("URL") like LOWER('%<CURRENT_PD_VALUE>%') ;

-- Type        : select
-- Origin      : jira
-- Description : application user
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${lower_user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. Click specific name on 'Full name' column
select "app_user".* from app_user where LOWER(lower_user_name) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : audit log changed value
-- Database    : postgresql

--  selecting dependant data
select "audit_log".*,search_field as search_field_before,REGEXP_REPLACE(search_field,'(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_PD_VALUE>\2','ig') as search_field_after from audit_log where id in (select log_id from audit_changed_value where LOWER(delta_from) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)');
select "audit_changed_value".* from audit_changed_value where LOWER(delta_from) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : audit log changed value
-- Database    : postgresql

--  selecting dependant data
select "audit_log".*,search_field as search_field_before,REGEXP_REPLACE(search_field,'(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_PD_VALUE>\2','ig') as search_field_after from audit_log where id in (select log_id from audit_changed_value where LOWER(delta_to) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)');
select "audit_changed_value".* from audit_changed_value where LOWER(delta_to) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : Audit log affected item
-- Database    : postgresql

select "audit_item".* from audit_item where LOWER(object_id) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : Audit log affected item
-- Database    : postgresql

--  selecting dependant data
select "audit_log".*,search_field as search_field_before,REGEXP_REPLACE(search_field,'(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_PD_VALUE>\2','ig') as search_field_after from audit_log where id in (select log_id from audit_item where (LOWER(object_name) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)') AND object_type  in ('GROUP', 'SCHEME', 'WORKFLOW', 'PROJECT', 'VERSION', 'PROJECT_COMPONENT', 'BOARD', 'SCREEN'));
select "audit_item".* from audit_item where (LOWER(object_name) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)') AND object_type  in ('GROUP', 'SCHEME', 'WORKFLOW', 'PROJECT', 'VERSION', 'PROJECT_COMPONENT', 'BOARD', 'SCREEN');

-- Type        : select
-- Origin      : jira
-- Description : Audit log affected item
-- Database    : postgresql

--  selecting dependant data
select "audit_log".*,search_field as search_field_before,REGEXP_REPLACE(search_field,'(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)','\1<NEW_PD_VALUE>\2','ig') as search_field_after from audit_log where id in (select log_id from audit_item where (LOWER(object_name) = LOWER('<CURRENT_PD_VALUE>') ) AND object_type  = 'USER');
select "audit_item".* from audit_item where (LOWER(object_name) = LOWER('<CURRENT_PD_VALUE>') ) AND object_type  = 'USER';

-- Type        : select
-- Origin      : jira
-- Description : audit log main entry
-- Database    : postgresql

select "audit_log".* from audit_log where (LOWER(object_id) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)') AND object_type  in ('GROUP', 'SCHEME', 'WORKFLOW', 'PROJECT', 'VERSION', 'PROJECT_COMPONENT', 'BOARD', 'SCREEN');

-- Type        : select
-- Origin      : jira
-- Description : audit log main entry
-- Database    : postgresql

select "audit_log".* from audit_log where (LOWER(object_name) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)') AND object_type  in ('GROUP', 'SCHEME', 'WORKFLOW', 'PROJECT', 'VERSION', 'PROJECT_COMPONENT', 'BOARD', 'SCREEN');

-- Type        : select
-- Origin      : jira
-- Description : audit log main entry
-- Database    : postgresql

select "audit_log".* from audit_log where (LOWER(object_name) = LOWER('<CURRENT_PD_VALUE>') ) AND object_type  = 'USER';

-- Type        : select
-- Origin      : jira
-- Description : audit log main entry
-- Database    : postgresql

select "audit_log".* from audit_log where (LOWER(remote_address) = LOWER('<CURRENT_PD_VALUE>') ) AND remote_address  IS NOT NULL;

-- Type        : select
-- Origin      : jira
-- Description : audit log main entry
-- Database    : postgresql

select "audit_log".* from audit_log where LOWER(search_field) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : avatar
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/ViewProfile.jspa?name=${user_key}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. Click specific name on 'Full name' column
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
select "avatar".* from avatar where LOWER(filename) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue history additional data
-- Database    : postgresql

select "changeitem".* from changeitem where LOWER(field) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue history additional data
-- Database    : postgresql

select "changeitem".* from changeitem where LOWER(newstring) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue history additional data
-- Database    : postgresql

select "changeitem".* from changeitem where LOWER(newvalue) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue history additional data
-- Database    : postgresql

select "changeitem".* from changeitem where LOWER(oldstring) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue history additional data
-- Database    : postgresql

select "changeitem".* from changeitem where LOWER(oldvalue) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : component
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/projects/${PKEY}?selectedItem=com.atlassian.jira.jira-projects-plugin:components-page, where PKEY: select pkey from project where id = project
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Select specific project name
--     4. 'Components' section (left sidebar)
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/component
select "component".* from component where LOWER(cname) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : component
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/projects/${PKEY}?selectedItem=com.atlassian.jira.jira-projects-plugin:components-page, where PKEY: select pkey from project where id = project
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Select specific project name
--     4. 'Components' section (left sidebar)
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/component
select "component".* from component where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : custom field
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditCustomField!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Custom fields'
select "customfield".* from customfield where LOWER(cfname) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : custom field
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditCustomField!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Custom fields'
select "customfield".* from customfield where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : customfield value
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issue}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Custom fields'
select "customfieldvalue".* from customfieldvalue where LOWER(stringvalue) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : customfield value
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issue}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Custom fields'
select "customfieldvalue".* from customfieldvalue where LOWER(textvalue) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : group membership
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/EditUserGroups!default.jspa?name=${child_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Groups'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/group
select "cwd_membership".* from cwd_membership where LOWER(child_name) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : group membership
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/EditUserGroups!default.jspa?name=${child_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Groups'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/group
select "cwd_membership".* from cwd_membership where LOWER(lower_child_name) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : user_data
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
select "cwd_user".* from cwd_user where LOWER(display_name) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : user_data
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
select "cwd_user".* from cwd_user where LOWER(email_address) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : user_data
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
select "cwd_user".* from cwd_user where LOWER(first_name) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : user_data
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
select "cwd_user".* from cwd_user where LOWER(last_name) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : user_data
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
select "cwd_user".* from cwd_user where LOWER(lower_display_name) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : user_data
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
select "cwd_user".* from cwd_user where LOWER(lower_email_address) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : user_data
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
select "cwd_user".* from cwd_user where LOWER(lower_first_name) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : user_data
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
select "cwd_user".* from cwd_user where LOWER(lower_last_name) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : user_data
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
select "cwd_user".* from cwd_user where LOWER(lower_user_name) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : user_data
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/user/ViewUser.jspa?name=${user_name}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'User management'
--     4. 'User management' (left sidebar)
--     5. Choose 'Users'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/user
select "cwd_user".* from cwd_user where LOWER(user_name) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : draft workflow scheme
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditWorkflowScheme.jspa?schemeId=${workflow_scheme_id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' (left sidebar)
--     5. Choose 'Workflow schemes'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflowscheme
select "draftworkflowscheme".* from draftworkflowscheme where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : draft workflow scheme
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditWorkflowScheme.jspa?schemeId=${workflow_scheme_id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' (left sidebar)
--     5. Choose 'Workflow schemes'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflowscheme
select "draftworkflowscheme".* from draftworkflowscheme where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : additional entity properties
-- Database    : postgresql

select "entity_property".* from entity_property where LOWER(json_value) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : field configuration context
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/ConfigureCustomField!default.jspa?customFieldId={CF_ID} where CF_ID: split fieldid on _
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration schemes'
select "fieldconfigscheme".* from fieldconfigscheme where LOWER(configname) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : field configuration context
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/ConfigureCustomField!default.jspa?customFieldId={CF_ID} where CF_ID: split fieldid on _
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration schemes'
select "fieldconfigscheme".* from fieldconfigscheme where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : field configuration
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldLayout!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration'
--     6. Click on a specific field configuration name
select "fieldlayout".* from fieldlayout where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : field configuration
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldLayout!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration'
--     6. Click on a specific field configuration name
select "fieldlayout".* from fieldlayout where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : single field configuration on specific field configuration
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/ConfigureFieldLayout!default.jspa?id=${fieldlayout} and search for fieldidentifier
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration'
--     6. Click on a specific field configuration name
select "fieldlayoutitem".* from fieldlayoutitem where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : field config scheme
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldLayoutScheme!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration schemes'
--     6. Click on a specific field configuration name
select "fieldlayoutscheme".* from fieldlayoutscheme where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : field config scheme
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldLayoutScheme!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration schemes'
--     6. Click on a specific field configuration name
select "fieldlayoutscheme".* from fieldlayoutscheme where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : screen
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldScreen!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration'
--     6. Click on a specific field configuration name
--     7. Click on a 'Screens' under specific field name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/screens
select "fieldscreen".* from fieldscreen where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : screen
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldScreen!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Fields' (left sidebar)
--     5. Choose 'Field configuration'
--     6. Click on a specific field configuration name
--     7. Click on a 'Screens' under specific field name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/screens
select "fieldscreen".* from fieldscreen where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : screen scheme
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldScreenScheme!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Screens' (left sidebar)
--     5. Choose 'Screens schemes'
--     6. Click 'Edit' under specific screen scheme name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/screens
select "fieldscreenscheme".* from fieldscreenscheme where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : screen scheme
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldScreenScheme!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Screens' (left sidebar)
--     5. Choose 'Screens schemes'
--     6. Click 'Edit' under specific screen scheme name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/screens
select "fieldscreenscheme".* from fieldscreenscheme where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : screen tab
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldScreen!default.jspa?id=${fieldscreen}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Screens' (left sidebar)
--     5. Click 'Edit' under specific screen name
select "fieldscreentab".* from fieldscreentab where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : screen tab
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditFieldScreen!default.jspa?id=${fieldscreen}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Screens' (left sidebar)
--     5. Click 'Edit' under specific screen name
select "fieldscreentab".* from fieldscreentab where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : attachment
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Attachments' sections
select "fileattachment".* from fileattachment where LOWER(filename) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : custom field default value
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/ConfigureCustomField!default.jspa?customFieldId=${CUSTOM_FIELD_ID} where CUSTOM_FIELD_ID: select SUBSTRING(fieldid, 13) from fieldconfiguration where id = ${datakey};
-- 
select "genericconfiguration".* from genericconfiguration where LOWER(xmlvalue) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue security scheme
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditIssueSecurityScheme!default.jspa?schemeId=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. Left sidebar > 'Issue Security Schemes'
--     5. Click 'Edit' under specific issue security scheme name
select "issuesecurityscheme".* from issuesecurityscheme where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue security scheme
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditIssueSecurityScheme!default.jspa?schemeId=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. Left sidebar > 'Issue Security Schemes'
--     5. Click 'Edit' under specific issue security scheme name
select "issuesecurityscheme".* from issuesecurityscheme where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue status
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditStatus!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Issue Attributes' (left sidebar)
--     5. Choose 'Status'
--     6. Click 'Edit' under specific issue status name
select "issuestatus".* from issuestatus where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue status
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditStatus!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Issue Attributes' (left sidebar)
--     5. Choose 'Status'
--     6. Click 'Edit' under specific issue status name
select "issuestatus".* from issuestatus where LOWER(pname) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue type
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditIssueType!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Issue Types' (left sidebar)
--     5. Choose 'Issue Types'
--     6. Click 'Edit' under specific issue type name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issuetype
select "issuetype".* from issuetype where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue type
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditIssueType!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Issue Types' (left sidebar)
--     5. Choose 'Issue Types'
--     6. Click 'Edit' under specific issue type name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issuetype
select "issuetype".* from issuetype where LOWER(pname) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue type screen scheme
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditIssueTypeScreenScheme!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Screens' (left sidebar)
--     5. Choose 'Issue type screen schemes'
--     6. Click 'Edit' under specific issue type screen scheme name
select "issuetypescreenscheme".* from issuetypescreenscheme where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue type screen scheme
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditIssueTypeScreenScheme!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Screens' (left sidebar)
--     5. Choose 'Issue type screen schemes'
--     6. Click 'Edit' under specific issue type screen scheme name
select "issuetypescreenscheme".* from issuetypescreenscheme where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : comment
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Click 'Comments' tab
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issue
select "jiraaction".* from jiraaction where LOWER(actionbody) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : draft of workflow
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/workflows/EditWorkflowDispatcher.jspa?wfName=${parentname}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' section (left sidebar)
--     5. Choose 'Workflows'
--     6. Click 'Edit' under specific workflow name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflow
select "jiradraftworkflows".* from jiradraftworkflows where LOWER(descriptor) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : draft of workflow
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/workflows/EditWorkflowDispatcher.jspa?wfName=${parentname}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' section (left sidebar)
--     5. Choose 'Workflows'
--     6. Click 'Edit' under specific workflow name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflow
select "jiradraftworkflows".* from jiradraftworkflows where LOWER(parentname) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issue
select "jiraissue".* from jiraissue where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issue
select "jiraissue".* from jiraissue where LOWER(environment) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/issue
select "jiraissue".* from jiraissue where LOWER(summary) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : workflow
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/workflows/EditWorkflowDispatcher.jspa?wfName=${workflowname}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' section (left sidebar)
--     5. Choose 'Workflows'
--     6. Click 'Edit' under specific workflow name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflow
select "jiraworkflows".* from jiraworkflows where LOWER(descriptor) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : workflow
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/workflows/EditWorkflowDispatcher.jspa?wfName=${workflowname}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' section (left sidebar)
--     5. Choose 'Workflows'
--     6. Click 'Edit' under specific workflow name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflow
-- 
-- SQL update of workflow name is not supported because it could break JIRA
-- Please follow steps described below
-- If it contains personal data please copy it and give it a new name
-- Assign new workflow in all workflow schemes using old workflow
-- Delete old workflow
select "jiraworkflows".* from jiraworkflows where LOWER(workflowname) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue label
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issue}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Details' section
select "label".* from label where LOWER(label) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : seems to be related to mail handler eg. issue created from email, issue commented from email etc.
-- Database    : postgresql

select "notificationinstance".* from notificationinstance where LOWER(emailaddress) = LOWER('<CURRENT_PD_VALUE>') ;

-- Type        : select
-- Origin      : jira
-- Description : notification scheme
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditNotificationScheme!default.jspa?schemeId=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. Left sidebar > Choose 'Notification schemes'
--     5. Click 'Edit' under specific notification scheme name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/notificationscheme
select "notificationscheme".* from notificationscheme where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : notification scheme
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditNotificationScheme!default.jspa?schemeId=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. Left sidebar > Choose 'Notification schemes'
--     5. Click 'Edit' under specific notification scheme name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/notificationscheme
select "notificationscheme".* from notificationscheme where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : permission scheme
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditPermissionScheme!default.jspa?schemeId=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. Left sidebar > Choose 'Permission schemes'
--     5. Click 'Edit' under specific permission scheme name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/permissionscheme
select "permissionscheme".* from permissionscheme where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : permission scheme
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditPermissionScheme!default.jspa?schemeId=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. Left sidebar > Choose 'Permission schemes'
--     5. Click 'Edit' under specific permission scheme name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/permissionscheme
select "permissionscheme".* from permissionscheme where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : dashboard
-- Database    : postgresql

select "portalpage".* from portalpage where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : dashboard
-- Database    : postgresql

select "portalpage".* from portalpage where LOWER(pagename) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : priority
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditPriority!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Priorities' section (left sidebar)
--     5. Choose 'Priorities'
--     6. Click 'Edit' under specific priority name
select "priority".* from priority where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : priority
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditPriority!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Priorities' section (left sidebar)
--     5. Choose 'Priorities'
--     6. Click 'Edit' under specific priority name
select "priority".* from priority where LOWER(iconurl) like LOWER('%<CURRENT_PD_VALUE>%') ;

-- Type        : select
-- Origin      : jira
-- Description : priority
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditPriority!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Priorities' section (left sidebar)
--     5. Choose 'Priorities'
--     6. Click 'Edit' under specific priority name
select "priority".* from priority where LOWER(pname) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : project
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/project/EditProject!default.jspa?pid=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/project
select "project".* from project where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : project
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/project/EditProject!default.jspa?pid=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/project
-- 
-- SQL update of original project key is not supported because it could break JIRA
-- In order to alter original project key:
-- 	create new project
-- 	move all issues to new project
-- 	remove old project
select "project".* from project where LOWER(originalkey) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : project
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/project/EditProject!default.jspa?pid=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/project
-- 
-- SQL update of project key is not supported because it could break JIRA
-- Please follow steps described above
select "project".* from project where LOWER(pkey) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : project
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/project/EditProject!default.jspa?pid=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/project
select "project".* from project where LOWER(pname) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : project
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/project/EditProject!default.jspa?pid=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/project
select "project".* from project where LOWER(url) like LOWER('%<CURRENT_PD_VALUE>%') ;

-- Type        : select
-- Origin      : jira
-- Description : project category
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/projectcategories/ViewProjectCategories!default.jspa
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. 'Project categories' sidebar
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/projectCategory
select "projectcategory".* from projectcategory where LOWER(cname) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : project category
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/projectcategories/ViewProjectCategories!default.jspa
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. 'Project categories' sidebar
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/projectCategory
select "projectcategory".* from projectcategory where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : version
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/plugins/servlet/project-config/${PROJECT_KEY}/administer-versions  where PROJECT_KEY: select pkey from project where id = ${project}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
--     5. 'Project settings' sidebar
--     6. Choose 'Version'
select "projectversion".* from projectversion where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : version
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/plugins/servlet/project-config/${PROJECT_KEY}/administer-versions  where PROJECT_KEY: select pkey from project where id = ${project}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
--     5. 'Project settings' sidebar
--     6. Choose 'Version'
select "projectversion".* from projectversion where LOWER(url) like LOWER('%<CURRENT_PD_VALUE>%') ;

-- Type        : select
-- Origin      : jira
-- Description : version
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/plugins/servlet/project-config/${PROJECT_KEY}/administer-versions  where PROJECT_KEY: select pkey from project where id = ${project}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Projects'
--     4. Click 'Edit' under specific project name
--     5. 'Project settings' sidebar
--     6. Choose 'Version'
select "projectversion".* from projectversion where LOWER(vname) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : entity property value
-- Database    : postgresql

select "propertystring".* from propertystring where LOWER(propertyvalue) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : entity property value
-- Database    : postgresql

select "propertytext".* from propertytext where LOWER(propertyvalue) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
select "remotelink".* from remotelink where LOWER(icontitle) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
select "remotelink".* from remotelink where LOWER(relationship) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
select "remotelink".* from remotelink where LOWER(statuscategorykey) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
select "remotelink".* from remotelink where LOWER(statusdescription) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
select "remotelink".* from remotelink where LOWER(statusiconlink) like LOWER('%<CURRENT_PD_VALUE>%') ;

-- Type        : select
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
select "remotelink".* from remotelink where LOWER(statusicontitle) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
select "remotelink".* from remotelink where LOWER(statusiconurl) like LOWER('%<CURRENT_PD_VALUE>%') ;

-- Type        : select
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
select "remotelink".* from remotelink where LOWER(statusname) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
select "remotelink".* from remotelink where LOWER(summary) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
select "remotelink".* from remotelink where LOWER(title) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue link eg. remote link
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Go to 'Issue links' section
select "remotelink".* from remotelink where LOWER(url) like LOWER('%<CURRENT_PD_VALUE>%') ;

-- Type        : select
-- Origin      : jira
-- Description : issue resolution
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditResolution!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Issue Attributes' section (left sidebar)
--     5. Choose 'Resolutions'
select "resolution".* from resolution where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue resolution
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditResolution!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Issue Attributes' section (left sidebar)
--     5. Choose 'Resolutions'
select "resolution".* from resolution where LOWER(iconurl) like LOWER('%<CURRENT_PD_VALUE>%') ;

-- Type        : select
-- Origin      : jira
-- Description : issue resolution
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditResolution!default.jspa?id=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Issue Attributes' section (left sidebar)
--     5. Choose 'Resolutions'
select "resolution".* from resolution where LOWER(pname) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue security level
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditIssueSecurities!default.jspa?atl_token=schemeId=${scheme}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. Left sidebar > 'Issue security schemes'
--     5. Click 'Security Levels' under a specific issue security scheme name
select "schemeissuesecuritylevels".* from schemeissuesecuritylevels where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : issue security level
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditIssueSecurities!default.jspa?atl_token=schemeId=${scheme}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. Left sidebar > 'Issue security schemes'
--     5. Click 'Security Levels' under a specific issue security scheme name
select "schemeissuesecuritylevels".* from schemeissuesecuritylevels where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : saved jql filter
-- Database    : postgresql

select "searchrequest".* from searchrequest where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : saved jql filter
-- Database    : postgresql

select "searchrequest".* from searchrequest where LOWER(filtername) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : saved jql filter
-- Database    : postgresql

select "searchrequest".* from searchrequest where LOWER(filtername_lower) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : saved jql filter
-- Database    : postgresql

select "searchrequest".* from searchrequest where LOWER(reqcontent) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : recorded last operations for recently used entities (users, projects, issues etc.)
-- Database    : postgresql

select "userhistoryitem".* from userhistoryitem where LOWER(data) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : workflow scheme
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditWorkflowScheme.jspa?schemeId=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' (left sidebar)
--     5. Choose 'Workflow schemes'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflowscheme
select "workflowscheme".* from workflowscheme where LOWER(description) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : workflow scheme
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/secure/admin/EditWorkflowScheme.jspa?schemeId=${id}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Click the 'cog' icon in the top right corner
--     3. Choose 'Issues'
--     4. 'Workflows' (left sidebar)
--     5. Choose 'Workflow schemes'
-- 
-- Update via REST API:
--     https://docs.atlassian.com/software/jira/docs/api/REST/latest/#api/2/workflowscheme
select "workflowscheme".* from workflowscheme where LOWER(name) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

-- Type        : select
-- Origin      : jira
-- Description : worklog
-- Database    : postgresql

-- Update via Jira URL (recommended):
--     http://<JIRA_URL>/browse/{ISSUE_KEY} where ISSUE_KEY: ISSUE_KEY: select (select pkey from project where id = project) || '-' || issuenum, * from jiraissue where id = ${issueid}
-- 
--   How to access: 
--     1. Login to Jira as Jira administrator:
--     2. Open 'http://<JIRA_URL>/browse/{ISSUE_KEY}' URL
--     3. Click 'Work Log' tab
select "worklog".* from worklog where LOWER(worklogbody) ~* '(^|[[:blank:]]|[[:punct:]])<CURRENT_PD_VALUE>([[:blank:]]|[[:punct:]]|$)';

