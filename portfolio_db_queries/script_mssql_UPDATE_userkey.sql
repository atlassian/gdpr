-- Type        : update
-- Origin      : Jira Portfolio, Team Management
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_82B313_PERSON.*,JIRA_USER_ID as JIRA_USER_ID_before,'<NEW_VALUE>' as JIRA_USER_ID_after from AO_82B313_PERSON where JIRA_USER_ID = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_82B313_PERSON set JIRA_USER_ID = '<NEW_VALUE>' where JIRA_USER_ID = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_D9132D_PERMISSIONS.*,HOLDER_KEY as HOLDER_KEY_before,'<NEW_VALUE>' as HOLDER_KEY_after from AO_D9132D_PERMISSIONS where HOLDER_KEY = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_D9132D_PERMISSIONS set HOLDER_KEY = '<NEW_VALUE>' where HOLDER_KEY = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_D9132D_PLAN.*,CREATOR_ID as CREATOR_ID_before,'<NEW_VALUE>' as CREATOR_ID_after from AO_D9132D_PLAN where CREATOR_ID = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_D9132D_PLAN set CREATOR_ID = '<NEW_VALUE>' where CREATOR_ID = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_D9132D_PROGRAM.*,OWNER as OWNER_before,'<NEW_VALUE>' as OWNER_after from AO_D9132D_PROGRAM where OWNER = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_D9132D_PROGRAM set OWNER = '<NEW_VALUE>' where OWNER = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_D9132D_SCENARIO_ABILITY.*,LAST_CHANGE_USER as LAST_CHANGE_USER_before,'<NEW_VALUE>' as LAST_CHANGE_USER_after from AO_D9132D_SCENARIO_ABILITY where LAST_CHANGE_USER = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_D9132D_SCENARIO_ABILITY set LAST_CHANGE_USER = '<NEW_VALUE>' where LAST_CHANGE_USER = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_D9132D_SCENARIO_ISSUES.*,LAST_CHANGE_USER as LAST_CHANGE_USER_before,'<NEW_VALUE>' as LAST_CHANGE_USER_after from AO_D9132D_SCENARIO_ISSUES where LAST_CHANGE_USER = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_D9132D_SCENARIO_ISSUES set LAST_CHANGE_USER = '<NEW_VALUE>' where LAST_CHANGE_USER = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_D9132D_SCENARIO_ISSUE_LINKS.*,LAST_CHANGE_USER as LAST_CHANGE_USER_before,'<NEW_VALUE>' as LAST_CHANGE_USER_after from AO_D9132D_SCENARIO_ISSUE_LINKS where LAST_CHANGE_USER = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_D9132D_SCENARIO_ISSUE_LINKS set LAST_CHANGE_USER = '<NEW_VALUE>' where LAST_CHANGE_USER = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_D9132D_SCENARIO_PERSON.*,LAST_CHANGE_USER as LAST_CHANGE_USER_before,'<NEW_VALUE>' as LAST_CHANGE_USER_after from AO_D9132D_SCENARIO_PERSON where LAST_CHANGE_USER = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_D9132D_SCENARIO_PERSON set LAST_CHANGE_USER = '<NEW_VALUE>' where LAST_CHANGE_USER = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_D9132D_SCENARIO_RESOURCE.*,LAST_CHANGE_USER as LAST_CHANGE_USER_before,'<NEW_VALUE>' as LAST_CHANGE_USER_after from AO_D9132D_SCENARIO_RESOURCE where LAST_CHANGE_USER = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_D9132D_SCENARIO_RESOURCE set LAST_CHANGE_USER = '<NEW_VALUE>' where LAST_CHANGE_USER = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_D9132D_SCENARIO_SKILL.*,LAST_CHANGE_USER as LAST_CHANGE_USER_before,'<NEW_VALUE>' as LAST_CHANGE_USER_after from AO_D9132D_SCENARIO_SKILL where LAST_CHANGE_USER = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_D9132D_SCENARIO_SKILL set LAST_CHANGE_USER = '<NEW_VALUE>' where LAST_CHANGE_USER = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_D9132D_SCENARIO_STAGE.*,LAST_CHANGE_USER as LAST_CHANGE_USER_before,'<NEW_VALUE>' as LAST_CHANGE_USER_after from AO_D9132D_SCENARIO_STAGE where LAST_CHANGE_USER = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_D9132D_SCENARIO_STAGE set LAST_CHANGE_USER = '<NEW_VALUE>' where LAST_CHANGE_USER = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_D9132D_SCENARIO_TEAM.*,LAST_CHANGE_USER as LAST_CHANGE_USER_before,'<NEW_VALUE>' as LAST_CHANGE_USER_after from AO_D9132D_SCENARIO_TEAM where LAST_CHANGE_USER = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_D9132D_SCENARIO_TEAM set LAST_CHANGE_USER = '<NEW_VALUE>' where LAST_CHANGE_USER = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_D9132D_SCENARIO_THEME.*,LAST_CHANGE_USER as LAST_CHANGE_USER_before,'<NEW_VALUE>' as LAST_CHANGE_USER_after from AO_D9132D_SCENARIO_THEME where LAST_CHANGE_USER = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_D9132D_SCENARIO_THEME set LAST_CHANGE_USER = '<NEW_VALUE>' where LAST_CHANGE_USER = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_D9132D_SCENARIO_VERSION.*,LAST_CHANGE_USER as LAST_CHANGE_USER_before,'<NEW_VALUE>' as LAST_CHANGE_USER_after from AO_D9132D_SCENARIO_VERSION where LAST_CHANGE_USER = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_D9132D_SCENARIO_VERSION set LAST_CHANGE_USER = '<NEW_VALUE>' where LAST_CHANGE_USER = '<OLD_VALUE>' ;

-- Type        : update
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : mssql

-- + SELECT (please review changes BEFORE)
select AO_D9132D_SCENARIO_XPVERSION.*,LAST_CHANGE_USER as LAST_CHANGE_USER_before,'<NEW_VALUE>' as LAST_CHANGE_USER_after from AO_D9132D_SCENARIO_XPVERSION where LAST_CHANGE_USER = '<OLD_VALUE>' ;

-- + UPDATE (be careful)
update AO_D9132D_SCENARIO_XPVERSION set LAST_CHANGE_USER = '<NEW_VALUE>' where LAST_CHANGE_USER = '<OLD_VALUE>' ;

