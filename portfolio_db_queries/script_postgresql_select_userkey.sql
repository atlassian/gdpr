-- Type        : select
-- Origin      : Jira Portfolio, Team Management
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : postgresql

select "AO_82B313_PERSON".* from "AO_82B313_PERSON" where LOWER("JIRA_USER_ID") = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : postgresql

select "AO_D9132D_PERMISSIONS".* from "AO_D9132D_PERMISSIONS" where LOWER("HOLDER_KEY") = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : postgresql

select "AO_D9132D_PLAN".* from "AO_D9132D_PLAN" where LOWER("CREATOR_ID") = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : postgresql

select "AO_D9132D_PROGRAM".* from "AO_D9132D_PROGRAM" where LOWER("OWNER") = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : postgresql

select "AO_D9132D_SCENARIO_ABILITY".* from "AO_D9132D_SCENARIO_ABILITY" where LOWER("LAST_CHANGE_USER") = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : postgresql

select "AO_D9132D_SCENARIO_ISSUES".* from "AO_D9132D_SCENARIO_ISSUES" where LOWER("LAST_CHANGE_USER") = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : postgresql

select "AO_D9132D_SCENARIO_ISSUE_LINKS".* from "AO_D9132D_SCENARIO_ISSUE_LINKS" where LOWER("LAST_CHANGE_USER") = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : postgresql

select "AO_D9132D_SCENARIO_PERSON".* from "AO_D9132D_SCENARIO_PERSON" where LOWER("LAST_CHANGE_USER") = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : postgresql

select "AO_D9132D_SCENARIO_RESOURCE".* from "AO_D9132D_SCENARIO_RESOURCE" where LOWER("LAST_CHANGE_USER") = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : postgresql

select "AO_D9132D_SCENARIO_SKILL".* from "AO_D9132D_SCENARIO_SKILL" where LOWER("LAST_CHANGE_USER") = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : postgresql

select "AO_D9132D_SCENARIO_STAGE".* from "AO_D9132D_SCENARIO_STAGE" where LOWER("LAST_CHANGE_USER") = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : postgresql

select "AO_D9132D_SCENARIO_TEAM".* from "AO_D9132D_SCENARIO_TEAM" where LOWER("LAST_CHANGE_USER") = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : postgresql

select "AO_D9132D_SCENARIO_THEME".* from "AO_D9132D_SCENARIO_THEME" where LOWER("LAST_CHANGE_USER") = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : postgresql

select "AO_D9132D_SCENARIO_VERSION".* from "AO_D9132D_SCENARIO_VERSION" where LOWER("LAST_CHANGE_USER") = LOWER('<OLD_VALUE>') ;

-- Type        : select
-- Origin      : Jira Portfolio, Live Plan
-- Description : 
-- Table valid only for specific product : Jira Portfolio, Live Plan
-- Database    : postgresql

select "AO_D9132D_SCENARIO_XPVERSION".* from "AO_D9132D_SCENARIO_XPVERSION" where LOWER("LAST_CHANGE_USER") = LOWER('<OLD_VALUE>') ;

