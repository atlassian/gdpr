#!/usr/bin/env bash

if [ $# != 1 ]
then
    echo "\nUsage: ${0} [jira|servicedesk|portfolio]\n"
    exit 1
fi

for db in mssql mysql oracle postgresql
do
    python parser4jira.py -o '<CURRENT_PD_VALUE>' -n '<NEW_PD_VALUE>' -f metadata/${1}_db.json -d ${db}
done
